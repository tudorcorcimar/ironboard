<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\Mailer\MailerBroadcastController;

class BroadcastCompany extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'broadcastCompany:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send company advertiseing mail by cron jobs!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $broadcast = new MailerBroadcastController;
        $broadcast->broadcastCompany();
    }
    
}
