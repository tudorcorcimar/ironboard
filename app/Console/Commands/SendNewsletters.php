<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\Newsletters\AdminNewslettersController;

class SendNewsletters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsletters:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send diferrent newsletters by cron jobs!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $newsletters = new AdminNewslettersController;
        $newsletters->sendAllNewsletters();
    }
    
}
