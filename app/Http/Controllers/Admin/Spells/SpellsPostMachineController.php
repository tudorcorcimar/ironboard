<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsPostMachineController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show machine texts
     * 
     * @return view
     */
    public function showPostMachineSpells(){
        $this->viewBag['subcategory'] = 'post_machine';
        
        $homeSpellsNames = [
            'post.machine.title',
            'post.machine.breadcrumbs.machine',
            'post.machine.form.title',
            'post.machine.form.machine.name',
            'post.machine.form.condition',
            'post.machine.form.type',
            'post.machine.form.description',
            'post.machine.form.upload.image',
            'post.machine.form.click.to.add.new.image',
            'post.machine.form.image.uploaded',
            'post.machine.form.more.details',
            'post.machine.form.currency',
            'post.machine.form.url',
            'post.machine.form.price',
            'post.machine.form.button.post.machine',
            'post.machine.condition.new',
            'post.machine.condition.used'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
