<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsFooterController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show footer texts
     * 
     * @return view
     */
    public function showFooterSpells(){
        $this->viewBag['subcategory'] = 'footer';
        
        $homeSpellsNames = [
            'footer.block.suport.title',
            'footer.block.suport.contact.us',
            'footer.block.suport.faq',
            'footer.block.aboutus.title',
            'footer.block.aboutus.aboutus',
            'footer.block.aboutus.services',
            'footer.block.aboutus.terms',
            'footer.block.aboutus.privacy',
            'footer.block.promo.title',
            'footer.block.promo.banner',
            'footer.block.promo.adds',
            'footer.block.promo.social'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
