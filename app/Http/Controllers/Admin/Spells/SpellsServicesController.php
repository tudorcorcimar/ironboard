<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsServicesController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show machine texts
     * 
     * @return view
     */
    public function showServicesSpells(){
        $this->viewBag['subcategory'] = 'services';
        
        $homeSpellsNames = [
            'services.title',
            'services.info',
            'services.table.title',
            'services.table.service.1',
            'services.table.service.2',
            'services.table.service.3',
            'services.table.service.4',
            'services.table.service.5',
            'services.table.service.6',
            'services.table.service.7',
            'services.table.service.8',
            'services.table.service.9',
            'services.table.service.10',
            'services.table.service.11',
            'services.table.service.12',
            'services.table.service.13',
            'services.table.service.14',
            'services.table.service.15',
            'services.table.service.16',
            'services.table.pakage.title.public',
            'services.table.pakage.title.partner',
            'services.table.pakage.title.marketing',
            'services.table.price',
            'services.table.price.public',
            'services.table.price.marketing',
            'services.table.price.partner',
            'services.table.special.offer',
            'services.table.button.buynow'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
