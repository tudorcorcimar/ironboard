<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsAlertsController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show machine texts
     * 
     * @return view
     */
    public function showAlertsSpells(){
        $this->viewBag['subcategory'] = 'alerts';
        
        $homeSpellsNames = [
            'alert.confirm.action',
            'alert.confirm.activate.machine',
            'alert.confirm.deactivate.machine',
            'alert.confirm.button.deactivate',
            'alert.confirm.button.activate',
            'alert.confirm.button.remove',
            'alert.confirm.remove.machine',
            'alert.confirm.button.cancel',
            'alert.notification.no.posted.machines'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
