<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsContactsController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show machine texts
     * 
     * @return view
     */
    public function showContactsSpells(){
        $this->viewBag['subcategory'] = 'contacts';
        
        $homeSpellsNames = [
            'contacts.title',
            'contacts.form.title',
            'contacts.form.name',
            'contacts.form.email',
            'contacts.form.subject',
            'contacts.form.message',
            'contacts.form.button.send',
            'contacts.info.title'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
