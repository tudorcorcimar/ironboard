<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsEditMachineController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show machine texts
     * 
     * @return view
     */
    public function showEditMachineSpells(){
        $this->viewBag['subcategory'] = 'post_machine';
        
        $homeSpellsNames = [
            'edit.machine.title',
            'edit.machine.breadcrumbs.machine',
            'edit.machine.form.title',
            'edit.machine.form.machine.name',
            'edit.machine.form.condition',
            'edit.machine.form.type',
            'edit.machine.form.description',
            'edit.machine.form.upload.image',
            'edit.machine.form.click.to.add.new.image',
            'edit.machine.form.image.uploaded',
            'edit.machine.form.more.details',
            'edit.machine.form.currency',
            'edit.machine.form.url',
            'edit.machine.form.price',
            'edit.machine.form.button.post.machine',
            'edit.machine.condition.new',
            'edit.machine.condition.used'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
