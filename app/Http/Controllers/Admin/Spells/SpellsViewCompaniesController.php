<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsViewCompaniesController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show view companies text
     * 
     * @return view
     */
    public function showViewCompaniesSpells(){
        $this->viewBag['subcategory'] = 'view_companies';
        
        $homeSpellsNames = [
            'view.company.breadcrumbs.companies',
            'view.company.title',
            'view.company.name',
            'view.company.person',
            'view.company.email',
            'view.company.phone',
            'view.company.website',
            'view.company.description',
            'view.company.machine.title',
            'view.company.last.update'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
