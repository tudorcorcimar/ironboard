<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\Services\Admin\AdminPermissionsManager;
use App\Models\Persistent\Cms\CmsDe;
use App\Models\Persistent\Cms\CmsEn;
use App\Models\Persistent\Cms\CmsFr;
use App\Models\Persistent\Cms\CmsIt;
use App\Models\Persistent\Cms\CmsRu;
use App\Models\Persistent\App\AppLanguages;
use App\Services\ValidatorService;
use Illuminate\Support\Facades\Redirect;

class SpellAddController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
        $this->viewBag['subcategory'] = 'add spell';
        $this->viewBag['languages'] = $this->getLanguages();
    }
    
    public function getLanguages() {
        $appLaguages = new AppLanguages;
        return $appLaguages->getAllLanguages();
    }
    
    public function showAddSpellForm() {
        return View::make("admin.spells.add", $this->viewBag);
    }
    
    private function getLastCmsId() {
        $languages = $this->getLanguages();
        $languageBiggestIds = [];
        foreach( $languages as $lang ){
            $viewDataEntityClassName = "App\Models\Persistent\Cms\Cms" . $lang;
            $selectedCmsIds = $viewDataEntityClassName::select('cms_id')->get()->toArray();
            $cmsIdsArray = [];
            foreach($selectedCmsIds as $arrayId){
                $cmsIdsArray[] = $arrayId['cms_id'];
            }
            sort($cmsIdsArray, SORT_NUMERIC);
            $languageBiggestIds[] = array_pop($cmsIdsArray);
        }
        sort($languageBiggestIds, SORT_NUMERIC);
        return array_pop($languageBiggestIds);
    }
    
    public function addSpell() {
        if($this->viewBag['userHasPermissions']){
            
            $cmsData = Input::all();
            
            $rules =[
                'cms_name'     => 'required|unique:cms_eng'
            ];

            $validator = ValidatorService::validate($rules);
            if($validator->fails()){
                $this->viewBag['form']['errors'] = $validator->errors();
                return View::make("admin.spells.add", $this->viewBag);
            }
            $lastCmsId = $this->getLastCmsId();
            $newCmsId = ++$lastCmsId;
            
            foreach( $cmsData['cms_content'] as $lang => $content ){
                $viewDataEntityClassName = "App\Models\Persistent\Cms\Cms" . $lang;
                $langEntity = new $viewDataEntityClassName;
                $langEntity->cms_id = $newCmsId;
                $langEntity->cms_name = $cmsData['cms_name'];
                $langEntity->cms_content = $content;
                $langEntity->save();
            }
            
            return Redirect::back()->with([
                'successMessage' => 'Successfuly!'
            ]);
            
        }
    }
    
}
