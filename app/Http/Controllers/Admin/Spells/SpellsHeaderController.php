<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsHeaderController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show header texts
     * 
     * @return view
     */
    public function showHeaderSpells(){
        $this->viewBag['subcategory'] = 'header';
        
        $homeSpellsNames = [
            'header.menu.button.home',
            'header.menu.button.machines',
            'header.menu.button.companies',
            'header.menu.button.register',
            'header.menu.button.sign.in',
            'header.menu.button.post.machine',
            'header.menu.button.profile',
            'header.menu.dropdown.general',
            'header.menu.dropdown.postings',
            'header.menu.dropdown.uploads',
            'header.menu.dropdown.sign.out'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
