<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsProfileGeneralController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show machine texts
     * 
     * @return view
     */
    public function showProfileGeneralSpells(){
        $this->viewBag['subcategory'] = 'profile_general';
        
        $homeSpellsNames = [
            'profile.general.info.title',
            'profile.general.info',
            'profile.general.public.page.title',
            'profile.general.form.title',
            'profile.general.form.name',
            'profile.general.form.contact.person',
            'profile.general.form.phone',
            'profile.general.form.countries',
            'profile.general.form.address',
            'profile.general.form.public.email',
            'profile.general.form.website',
            'profile.general.form.button.update',
            'profile.general.form.description'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
