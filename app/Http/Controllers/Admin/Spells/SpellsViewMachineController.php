<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsViewMachineController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show machine texts
     * 
     * @return view
     */
    public function showViewMachineSpells(){
        $this->viewBag['subcategory'] = 'view_machine';
        
        $homeSpellsNames = [
            'view.machine.breadcrumbs.machine',
            'view.machine.title',
            'view.machine.name',
            'view.machine.status',
            'view.machine.type',
            'view.machine.url',
            'view.machine.price',
            'view.machine.description',
            'view.machine.condition.new',
            'view.machine.condition.used',
            'view.machine.last.update',
            'view.machine.image.total',
            'view.machine.image.images',
            'view.machine.company.title',
            'view.machine.company.name',
            'view.machine.company.person',
            'view.machine.company.email',
            'view.machine.company.phone',
            'view.machine.company.website',
            'view.machine.company.button.open'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
