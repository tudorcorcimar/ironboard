<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsRegisterController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show machine texts
     * 
     * @return view
     */
    public function showRegisterSpells(){
        $this->viewBag['subcategory'] = 'register';
        
        $homeSpellsNames = [
            'register.title',
            'register.breadcrumbs.register',
            'register.general.info.title',
            'register.general.info',
            'register.form.title',
            'register.form.company.name',
            'register.form.email',
            'register.form.password',
            'register.form.repeat.password',
            'register.form.button.register',
            'register.successfuly.registered.title',
            'register.successfuly.registered.info'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
