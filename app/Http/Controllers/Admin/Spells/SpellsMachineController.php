<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsMachineController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show machine texts
     * 
     * @return view
     */
    public function showMachineSpells(){
        $this->viewBag['subcategory'] = 'machine';
        
        $homeSpellsNames = [
            'machine.search.form.title',
            'machine.search.form.keywords',
            'machine.search.form.type',
            'machine.search.form.condition',
            'machine.search.form.button.search',
            'machine.board.sort.name',
            'machine.board.sort.condition',
            'machine.board.sort.type',
            'machine.board.sort.last.update',
            'machine.under.board.text',
            'machine.condition.new',
            'machine.condition.used',
            'machine.title'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
