<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Services\Admin\AdminPermissionsManager;

class SpellEditController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
    }
    
    public function updateSpell() {
        if($this->viewBag['userHasPermissions']){
            $spellData = Input::all();
            $viewDataEntityClassName = "App\Models\Persistent\Cms\Cms" . $spellData['language'];
            $cmsEntity = $viewDataEntityClassName::where('cms_id', '=', $spellData['cms_id'])->get()->first();
            $cmsEntity->cms_content = $spellData['cms_content'];
            $cmsEntity->save();
            return Redirect::back()->with([
                'successMessage' => 'Id:'.$spellData['cms_id'] . ' [' . $spellData['language'] . ']' . ' [' . $spellData['cms_name'] . '] successfuly updated!'
            ]);
        }
    }
    
}
