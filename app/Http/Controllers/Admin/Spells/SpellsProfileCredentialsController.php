<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Runtime\Cms\CmsManager;
use App\Services\Admin\AdminPermissionsManager;
use View;

class SpellsProfileCredentialsController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
    }
    
    private function getSpells($spellsNames) {
        $cmsManager = new CmsManager;
        return $cmsManager->getSpellsFromArrayOfNames($spellsNames);
    }
    
    /*
     * Show machine texts
     * 
     * @return view
     */
    public function showProfileCredentialsSpells(){
        $this->viewBag['subcategory'] = 'profile_credentials';
        
        $homeSpellsNames = [
            'profile.credentials.info.title',
            'profile.credentials.info',
            'profile.credentials.change.email.title',
            'profile.credentials.change.password.title',
            'profile.credentials.change.email.current.password',
            'profile.credentials.change.email.new.email',
            'profile.credentials.change.password.new.password',
            'profile.credentials.change.password.current.password',
            'profile.credentials.button.save'
        ];
        $this->viewBag['spells'] = $this->getSpells($homeSpellsNames);
        
        return View::make("admin.spells.edit", $this->viewBag);
    }
    
}
