<?php

namespace App\Http\Controllers\Admin\Spells;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\Input;
use App\Models\Persistent\App\AppInfo;
use Illuminate\Support\Facades\View;
use App\Services\Admin\AdminPermissionsManager;
use Illuminate\Support\Facades\Redirect;

class SpellApplicationController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_SPELLS, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'spells';
        $this->viewBag['subcategory'] = 'application';
    }
    
    public function showApplicationSpellForm() {
        $appInfo = AppInfo::where('id', '=', '1')
                          ->get()
                          ->first();
        $this->viewBag['form'] = $this->serializeAppInfo($appInfo);
        return View::make("admin.spells.app", $this->viewBag);
    }
    
    private function serializeAppInfo($appInfo) {
        $app = [];
        
        $app['meta_description'] = $appInfo->meta_description;
        $app['meta_keywords'] = $appInfo->meta_keywords;
        $app['url'] = $appInfo->url;
        $app['email'] = $appInfo->email;
        $app['name'] = $appInfo->name;
        $app['address'] = $appInfo->address;
        $app['phone'] = $appInfo->phone;
        $app['fax'] = $appInfo->fax;
        
        return $app;
    }
    
    public function updateApplicationInfo() {
        if($this->viewBag['userHasPermissions']){
            $inputs = Input::all();
            
            $appInfo = AppInfo::where('id', '=', '1')
                              ->get()
                              ->first();
            if($inputs['fax']){
                $appInfo->fax = $inputs['fax'];
            }
            if($inputs['phone']){
                $appInfo->phone = $inputs['phone'];
            }
            if($inputs['address']){
                $appInfo->address = $inputs['address'];
            }
            if($inputs['meta_description']){
                $appInfo->meta_description = $inputs['meta_description'];
            }
            if($inputs['meta_keywords']){
                $appInfo->meta_keywords = $inputs['meta_keywords'];
            }
            if($inputs['url']){
                $appInfo->url = $inputs['url'];
            }
            if($inputs['email']){
                $appInfo->email = $inputs['email'];
            }
            if($inputs['name']){
                $appInfo->name = $inputs['name'];
            }
            $appInfo->save();

        }
        return Redirect::to('admin/spells/application');
    }
    
}
