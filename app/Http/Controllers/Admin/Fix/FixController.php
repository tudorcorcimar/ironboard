<?php

namespace App\Http\Controllers\Admin\Fix;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class FixController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function executeFix(){
        
        $pathToMachine = public_path().'/images/machine';
        $pathToThumbnail = public_path().'/images/thumbnail';
        $companies = array_diff(scandir($pathToMachine), array('..', '.'));
        foreach ($companies as $companyId) {
            $companyMachinePath = $pathToMachine.'/'.$companyId;
            $machines = array_diff(scandir($companyMachinePath), array('..', '.'));
            if(count($machines)>0){
                foreach ($machines as $machineId){
                    $machineCompanyPath = $companyMachinePath.'/'.$machineId;
                    $machineImagePaths = File::files($machineCompanyPath);
                    if(count($machineImagePaths) > 0){

                        $thumbnailPath = $pathToThumbnail."/".$companyId."/".$machineId;

                        if(!File::exists($thumbnailPath)){
                            File::makeDirectory($thumbnailPath, 0777, true);
                        }
                        $image = Image::make($machineImagePaths[0]);
                        $resizedImage = $image->heighten('150');

                        if(File::exists($thumbnailPath) && !File::exists($thumbnailPath."/thumbnail.jpg")){
                            $image = Image::make($machineImagePaths[0]);
                            $resizedImage = $image->heighten('150');
                            $resizedImage->save($thumbnailPath."/thumbnail.jpg");
                        }
                    }
                }
            }
        }
        echo 'done!';
    }
    
}
