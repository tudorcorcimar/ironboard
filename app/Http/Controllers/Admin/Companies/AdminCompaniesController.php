<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Persistent\Users\Users;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Services\CompaniesManager;
use Illuminate\Support\Facades\Redirect;
use App\Services\Admin\AdminPermissionsManager;
use Illuminate\Support\Facades\Response;
use View;

class AdminCompaniesController extends BaseController
{
    private $filters = [];
    private $request;
    
    public function __construct( Request $request ) {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_COMPANIES, AdminPermissionsManager::PERMISSION_ACTION_SEARCH);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->request = $request;
        $this->viewBag['category'] = 'company';
    }
    
    private function findCompanies(){
        $filters = $this->getSearchFilters();
        $this->viewBag['form'] = $filters;
        
        $companies = Users::where('type', 2);
        
        if(isset($filters['keywords'])){
            $companies->where(function ($query) use ($filters) {
                $query->where('company_name', 'LIKE', '%'.$filters['keywords'].'%');
                $query->orWhere('description', 'LIKE', '%'.$filters['keywords'].'%');
                $query->orWhere('email', 'LIKE', '%'.$filters['keywords'].'%');
            });
        }
        
        if(isset($filters['locations']) && count($filters['locations']) > 0){
            $companies->join('users_locations', 'users_locations.users_id', '=', 'users.id')
                      ->whereIn('users_locations.location_id', $filters['locations'])
                      ->select('*');
        }
        
        $companies->orderBy($filters['sortBy'], $filters['sortOrder']);
        $pagination = $companies->paginate($filters['pageSize']);
        
        return [
            'pagination' => $pagination,
            'sort' => [
                'by' => $filters['sortOrder'],
                'order' => $filters['sortBy']
            ],
            'items' => CompaniesManager::companySerialization($pagination),
        ];
        
    }
    
    public function filterSearch() {
        $this->resetSearch();
        $this->viewBag['companies'] = $this->findCompanies();
        return View::make('admin.companies.search', $this->viewBag);
    }
    
    /*
     * Show copanies view
     * 
     * @return view
     */
    public function showCompanies(){
        $this->viewBag['subcategory'] = 'search';
        $this->viewBag['companies'] = $this->findCompanies();
        return View::make("admin.companies.search", $this->viewBag);
    }
    
    private function getSearchFilters() {
        $this->filters['sortBy'] = 'last_comment';
        $this->filters['sortOrder'] = self::MACHINE_DEFAULT_SORT_ORDER;
        $this->filters['pageSize'] = self::MACHINE_DEFAULT_PAGE_SIZE;
        
        $this->addToFilters('sortBy', 'companiesSortBy');
        $this->addToFilters('sortOrder', 'companiesSortOrder');
        $this->addToFilters('pageSize', 'companiesPageSize');
        
        $this->addToFilters('keywords', 'companiesKeywords');
        $this->addToFilters('locations', 'companiesLocations');
        
        return $this->filters;
        
    }
    
    private function addToFilters($inputKey, $sessionKey) {
        $inputValue = Input::get($inputKey);
        if($inputValue && $inputValue !== ''){
            $this->filters[$inputKey] = $inputValue;
            $this->request->session()->put($sessionKey, $inputValue);
        }elseif($this->request->session()->has($sessionKey)){
            $this->filters[$inputKey] =  $this->request->session()->get($sessionKey);
        }
    }
    
    private function resetSearch() {
        $this->request->session()->forget('companiesSortBy');
        $this->request->session()->forget('companiesSortOrder');
        $this->request->session()->forget('companiesPageSize');
        $this->request->session()->forget('companiesKeywords');
        $this->request->session()->forget('companiesLocations');
    }
    
    public function clearFilters() {
        $this->resetSearch();
        return Redirect::to('/admin/companies');
    }
    
    public function simpleSearch( Request $request ) {
        $companyName = $request->company_name;
        $companies = Users::where('type', 2);
        if( $companyName ){
            $companies->where('company_name', 'LIKE', '%'.$companyName.'%')->take(20)->get();
        }
        $result = $companies->get()->toArray();
        
        return Response::json($result);
        
    }
    
}
