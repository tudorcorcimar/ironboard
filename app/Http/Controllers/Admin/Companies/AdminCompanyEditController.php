<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Persistent\Users\Users;
use App\Models\Persistent\Machine\Machine;
use Illuminate\Support\Facades\Input;
use App\Services\ValidatorService;
use Illuminate\Support\Facades\Redirect;
use App\Services\ImageService;
use App\Services\MachineManager;
use App\Models\Persistent\Users\UsersLocations;
use App\Models\Persistent\Users\UsersAdminComments;
use App\Http\Controllers\Admin\Machines\AdminMachinesEditController;
use App\Models\Persistent\Users\UsersSettings;
use App\Services\Admin\AdminPermissionsManager;
use View;

class AdminCompanyEditController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_COMPANIES, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'company';
    }
    
    /*
     * Validation rules
     * 
     * @type array
     */
    private $validationRules = [
        'company_name' => "required|max:150",
        'email'        => 'required|email',
        'password'     => 'required|max:150',
        'website'      => "url|max:70",
        'created_at'   => 'date',
        'updated_at'   => 'date'
    ];
    
    /*
     * Create new user in database
     * 
     * @param array $data
     * @return void
     */
    private function updateUser($data){
        $user = Users::find($data['id']);
        $currentDate = date('Y-m-d H:i:s');
        if(isset($data['company_name'])){
            $user->company_name = $data['company_name'];
        }
        if(isset($data['email'])){
            $user->email = $data['email'];
        }
        if(isset($data['name'])){
            $user->name = $data['name'];
        }
        if(isset($data['password'])){
            $user->password = $data['password'];
        }
        if(isset($data['updated_at'])){
            $user->updated_at = $currentDate;
        }
        if(isset($data['active'])){
            $user->active = $data['active'];
        }
        if(isset($data['address'])){
            $user->address = $data['address'];
        }
        if(isset($data['phone'])){
            $user->phone = $data['phone'];
        }
        if(isset($data['description'])){
            $user->description = $data['description'];
        }
        if(isset($data['website'])){
            $user->website = $data['website'];
        }
        if(isset($data['public_email'])){
            $user->public_email = $data['public_email'];
        }
        
        $user->updated_at = $currentDate;
        
        if(isset($data['logo'])){
            $this->uploadLogo($user->id);
        }
        
        $userLocations = UsersLocations::where('users_id', $user->id);
        $userLocations->delete();
        if(isset($this->viewBag['form']['location']) && count($this->viewBag['form']['location']) > 0){
            foreach ($this->viewBag['form']['location'] as $location){
                $userLocations = new UsersLocations;
                $userLocations->users_id = $user->id;
                $userLocations->location_id = $location;
                $userLocations->save();
            }
        }
        
        $user->save();
    }
    
    public function showEditCompany($id) {
        $this->viewBag['subcategory'] = 'company_edit';
        $this->viewBag['form'] = $this->getCompanyDetails($id);
        
        $machines = $this->getCompanyMachines($id);
        $this->viewBag['company']['machines'] = MachineManager::machineSerialization($machines);
        
        return View::make("admin.companies.edit", $this->viewBag);
    }
    
    public function editCompany() {
        $this->viewBag['subcategory'] = 'company_edit';
        $this->viewBag['form'] = [];
        $this->viewBag['form'] = Input::all();
        
        $user = Users::find($this->viewBag['form']['id']);
        if(trim($this->viewBag['form']['company_name']) !== trim($user->company_name)){
            $this->validationRules['company_name'] .= "|unique:users";
        }
        
        if(trim($this->viewBag['form']['email']) !== trim($user->email)){
            $this->validationRules['email'] .= "|unique:users";
        }
        
        $validator = ValidatorService::validate($this->validationRules);
        if($validator->fails()){
            $this->viewBag['form']['errors'] = $validator->errors();
            return View::make("admin.companies.edit", $this->viewBag);
        }else{
            $this->updateUser($this->viewBag['form']);
            return Redirect::to('/admin/company/edit/'.$this->viewBag['form']['id'])->with([
                'successMessage' => 'Company successfuly updated!'
            ]);
        }
    }
    
    private function getCompanyDetails($companyId){
        $user = Users::where('id', '=', $companyId)->get()->first();
        if($user){
            $company = $this->detailsToArray($user);
        }
        $logo = $this->getLogo($companyId);
        if($logo){
            $company['logo'] = $logo;
        }
        return $company;
    }
    
    private function detailsToArray($company){
        $userDetails = [];
        $userDetails['id'] = $company->id;
        $userDetails['company_name'] = $company->company_name;
        $userDetails['name'] = $company->name;
        $userDetails['email'] = $company->email;
        $userDetails['password'] = $company->password;
        $userDetails['active'] = $company->active;
        $userDetails['address'] = $company->address;
        $userDetails['phone'] = $company->phone;
        $userDetails['description'] = $company->description;
        $userDetails['website'] = $company->website;
        $userDetails['public_email'] = $company->public_email;
        $userDetails['highlighted'] = $company->highlighted;
        $userDetails['usersLocations'] = $company->usersLocations;
        $userDetails['comments'] = $company->usersAdminComments;
        
        return $userDetails;
    }
    
    private function getLogo($companyId) {
        $imageService = new ImageService;
        $images = $imageService->getImages(false, $companyId, true);
        if(count($images) > 0){
            return $imageService->getLogoImageInBase64($companyId, $images);
        }
        return false;
    }
    
    private function uploadLogo($userId) {
        $form = Input::all();
        $imageService = new ImageService;
        $imageService->saveImages($form['logo'], 'logo', $userId, true);
        return true;
    }
    
    private function getCompanyMachines($companyId){
        return Machine::where('user_id', $companyId)
            ->orderBy('updated_at', 'desc')->get();
    }
    
    public function activateHighlightCompany($companyId) {
        $company = Users::where('id', $companyId)
                            ->get()->first();
        if( $company !== null ){
            $company->highlighted = true;
            $company->save();
        }
        
        return Redirect::to('/admin/companies')->with('successMessage', 'Company was succesuful highlighted!');
    }
    
    public function deactivateHighlightCompany($companyId) {
        $company = Users::where('id', $companyId)
                            ->get()->first();
        if( $company !== null ){
            $company->highlighted = false;
            $company->save();
        }
        
        return Redirect::to('/admin/companies')->with('successMessage', 'Company was succesuful highlighted!');
    }
    
    public function addCompanyAdminComment() {
        $form = Input::all();
        $currentDate = date('Y-m-d');
        if($form['comment'] != ''){
            $comment = new UsersAdminComments;
            $comment->updated_at = $currentDate;
            $comment->created_at = $currentDate;
            $comment->comment = $form['comment'];
            $comment->users_id = $form['id'];
            $comment->save();
        }
        $user = Users::find($form['id']);
        $user->last_comment = $currentDate;
        $user->save();
        return Redirect::to('/admin/company/edit/'.$form['id'])->with('successMessage', 'Comment was succesufuly add!');
    }
    
    public function removeCompanyAdminComment($commentId) {
        $comment = UsersAdminComments::where('id', $commentId)
                            ->get()->first();
        $companyId = $comment->users_id;
        $comment->delete();
        
        return Redirect::to('/admin/company/edit/'.$companyId)->with('successMessage', 'Comment was succesufuly removed!');
    }
    
    public function removeCompany($id) {
        $companyMachines = Machine::where('user_id', '=', $id);
        $machineEditControler = new AdminMachinesEditController;
        foreach ($companyMachines->get() as $machine){
            $machineEditControler->removeMachine($machine->id);
        }
        $companyMachines->delete();
        $company = Users::where('id', '=', $id);
        
        $userSettings = UsersSettings::where('user_id', '=', $id);
        $userSettings->delete();
        
        $companyName = $company->first()->company_name;
        $company->delete();
        
        return Redirect::to('/admin/companies')->with('successMessage', 'Company ' . $companyName . ' successfuly removed!');
    }
    
    public function deactivateCompany($companyId) {
        $company = Users::where('id', '=', $companyId)->first();
        $company->active = false;
        $company->save();
        
        return Redirect::to('/admin/companies')->with('successMessage', 'Company successfuly deactivated!');
    }
    
    public function activateCompany($companyId) {
        $company = Users::where('id', '=', $companyId)->first();
        $company->active = true;
        $company->save();
        
        return Redirect::to('/admin/companies')->with('successMessage', 'Company successfuly activated!');
    }
    
    public function subscribeCompany($companyId) {
        $company = Users::where('id', '=', $companyId)->first();
        $company->subscribed = true;
        $company->save();
        
        return Redirect::to('/admin/companies')->with('successMessage', 'Company successfuly subscribed!');
    }
    
    public function unsubscribeCompany($companyId) {
        $company = Users::where('id', '=', $companyId)->first();
        $company->subscribed = false;
        $company->save();
        
        return Redirect::to('/admin/companies')->with('successMessage', 'Company successfuly unsubscribed!');
    }
    
}
