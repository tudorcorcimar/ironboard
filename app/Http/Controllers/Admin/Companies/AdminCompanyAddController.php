<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Persistent\Users\Users;
use Illuminate\Support\Facades\Input;
use App\Services\ValidatorService;
use Illuminate\Support\Facades\Redirect;
use App\Services\ImageService;
use App\Models\Persistent\Users\UserType;
use App\Models\Persistent\Users\UsersSettings;
use App\Models\Persistent\Users\UsersLocations;
use App\Services\Admin\AdminPermissionsManager;
use View;

class AdminCompanyAddController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_COMPANIES, AdminPermissionsManager::PERMISSION_ACTION_ADD);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'company';
    }
    
    /*
     * Validation rules
     * 
     * @type array
     */
    private $validationRules = [
        'company_name' => 'required|max:255|unique:users',
        'email'        => 'required|email|max:150|unique:users',
        'password'     => 'required|max:150',
        'website'      => "url|max:70",
        'created_at'   => 'date',
        'updated_at'   => 'date'
    ];
    
    /*
     * Create new user in database
     * 
     * @param array $data
     * @return void
     */
    private function createNewUser($data){
        $user = new Users;
        $currentDate = date('Y-m-d');
        
        if(isset($data['company_name'])){
            $user->company_name = $data['company_name'];
        }
        if(isset($data['name'])){
            $user->name = $data['name'];
        }
        if(isset($data['email'])){
            $user->email = $data['email'];
        }
        if(isset($data['password'])){
            $user->password = $data['password'];
        }
        if(isset($data['updated_at'])){
            $user->updated_at = $currentDate;
        }
        if(isset($data['active'])){
            $user->active = $data['active'];
        }
        if(isset($data['address'])){
            $user->address = $data['address'];
        }
        if(isset($data['description'])){
            $user->description = $data['description'];
        }
        if(isset($data['phone'])){
            $user->phone = $data['phone'];
        }
        if(isset($data['website'])){
            $user->website = $data['website'];
        }
        if(isset($data['public_email'])){
            $user->public_email = $data['public_email'];
        }
        if(isset($data['password'])){
            $user->password = $data['password'];
        }
        $user->created_at = $currentDate;
        $user->updated_at = $currentDate;
        
        $user->type = UserType::USER_COMPANY;
        $user->active = 1;
        
        $user->save();
        
        if(isset($data['logo'])){
            $this->uploadLogo($user->id);
        }
        
        if(isset($this->viewBag['form']['location']) && count($this->viewBag['form']['location']) > 0){
            foreach ($this->viewBag['form']['location'] as $location){
                $userLocations = new UsersLocations;
                $userLocations->users_id = $user->id;
                $userLocations->location_id = $location;
                $userLocations->save();
            }
        }
        
        $userSettings = new UsersSettings;
        $userSettings->user_id = $user->id;
        $userSettings->save();
    }
    
    public function addCompany() {
        $this->viewBag['subcategory'] = 'company_add';
        $this->viewBag['form'] = [];
        $this->viewBag['form'] = Input::all();
        $validator = ValidatorService::validate($this->validationRules);
        if($validator->fails()){
            $this->viewBag['form']['errors'] = $validator->errors();
            return View::make("admin.companies.add", $this->viewBag);
        }else{
            $this->createNewUser($this->viewBag['form']);
            return Redirect::to('/admin/companies')->with([
                'successMessage' => 'User successfuly created!'
            ]);
        }
    }
    
    public function showAddNewCompany() {
        $this->viewBag['subcategory'] = 'company_add';
        return View::make("admin.companies.add", $this->viewBag);
    }
    
    private function uploadLogo($userId) {
        $form = Input::all();
        $imageService = new ImageService;
        $imageService->saveImages($form['logo'], 'logo', $userId, true);
        return true;
    }
    
}
