<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Runtime\Auth\AdminAuth as Auth;
use App\Models\Runtime\Locations\LocationsList;
use App\Models\Runtime\Machine\MachineTypesList;
use App\Models\Persistent\Machine\MachineStatuses;
use App\Models\Runtime\App\AppManager;
use App\Models\Runtime\Cms\CmsManager;
use Illuminate\Support\Facades\Redirect;
use App\Models\Persistent\App\AppLanguages;
use App\Services\Admin\AdminPermissionsManager;

class BaseController extends Controller
{
    
    const MACHINE_DEFAULT_SORT_BY = 'created_at';
    const MACHINE_DEFAULT_SORT_ORDER = 'desc';
    const MACHINE_DEFAULT_PAGE_SIZE = 25;
    
    const COMPANY_DEFAULT_SORT_BY = 'updated_at';
    const COMPANY_DEFAULT_SORT_ORDER = 'desc';
    const COMPANY_DEFAULT_PAGE_SIZE = 20;
    
    private $cmsManager;

    /*
     * App manager entity
     * 
     * AppManager
     */
    private $appManager;
    
    /*
     * Admin permissions manager instance
     * 
     * AdminPermissionsManager
     */
    public $adminPermissionsManager;

    /*
     * Array with all view data
     * 
     * @type array
     */
    public $viewBag = array();

    public function __construct() {
        $this->collectBaseViewBag();
        $this->adminPermissionsManager = new AdminPermissionsManager;
    }
    
    private function getViewsData() {
        $this->cmsManager = new CmsManager;
        if(!$this->cmsManager->getLanguage()){
            $this->cmsManager->setLanguage(AppLanguages::ENGLISH);
        }
        return $this->cmsManager->getViewsData();
    }
    
    /*
     * Add locations to view bag
     */
    private function addToViewBagApplicationData(){
        $this->appManager = new AppManager();
        $this->viewBag['app'] = [];
        $this->viewBag['app'] = $this->appManager->getAppInfo();
        $this->viewBag['views'] = $this->getViewsData();
        $this->viewBag['resources_path'] = public_path();
    }
    
    /*
     * Add locations to view bag
     */
    private function addToViewBagDictionaries(){
        $this->viewBag['locations']['countries'] = LocationsList::getCountries();
        $this->viewBag['locations']['regions'] = LocationsList::getRegions();
        $this->viewBag['machine']['types'] = MachineTypesList::getAll();
        $this->viewBag['machine']['statuses'] = MachineStatuses::getAll();
    }
    
    /*
     * Prepare authentication and add to view bag
     */
    private function addToViewBagAuthentication(){
            
        
        $asdasd= Auth::getInstance()->getIsAuth();
            
        if(Auth::getInstance()->getIsAuth()){
            $this->viewBag['isAuth'] = Auth::getInstance()->getIsAuth();
            $this->viewBag['user'] = Auth::getInstance()->getUser();
        }else{
            $this->viewBag['user'] = null;
        }
    }
    
    private function addToViewBagLocalization(){
        $this->viewBag['language'] = $this->cmsManager->getLanguage();
    }

    public function collectBaseViewBag() {
        $this->addToViewBagApplicationData();
        $this->addToViewBagAuthentication();
        $this->addToViewBagDictionaries();
        $this->addToViewBagLocalization();
    }
    
    public function changeLanguage($language) {
        if($language){
            $this->cmsManager->setLanguage($language);
        }
        return Redirect::to('/home');
    }
        
}
