<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Models\Runtime\Auth\AdminAuth as Auth;
use View;

class AdminAuthController extends BaseController
{
    
    public function __construct() {
        Auth::getInstance()->logout();
        parent::__construct();
    }
    
    /*
     * Show sign in view
     * 
     * @return view
     */
    public function showSignIn(){
        if(Auth::getInstance()->getIsAuth()){
            return Redirect::to('/admin/home');
        }
        return View::make("admin.auth.view", $this->viewBag);
    }
    
    /*
     * Sign in
     * 
     * @return redirect || view
     */
    public function signIn(){
        Auth::getInstance()->login(Input::get('email'), Input::get('password'), !!Input::get('rememberme'));
        $this->viewBag['form'] = Input::all();
        if(Auth::getInstance()->getIsAuth()){
            return Redirect::to('/admin/home');
        }else{
            $this->viewBag['form']['error'] = 'Your email or password is incorrect. Please try again.';
        }
        return View::make("admin.auth.view", $this->viewBag);
    }
    
    /*
     * Sign out
     * 
     * @return redirect
     */
    public function signOut(){
        Auth::getInstance()->logout();
        return Redirect::to('/admin');
    }
    
    
    
}
