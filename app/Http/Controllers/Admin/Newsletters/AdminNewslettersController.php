<?php

namespace App\Http\Controllers\Admin\Newsletters;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\Mail;
use App\Models\Persistent\Machine\Machine;
use App\Services\MachineManager;
use App\Models\Persistent\Users\Users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use App\Models\Persistent\Hash\HashList;
use View;

class AdminNewslettersController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $this->viewBag['category'] = 'Newsletters';
    }
    
    public function showNewsletters(){
        //return View::make("admin.companies.search", $this->viewBag);
    }
    
    public function sendAllNewsletters(){
        $simple = $this->getMachinesForThisWeek();
        $highlighted = Machine::where('active', 1)->where('highlighted', 1)->get();
        $mails = $this->getAllActiveCompaniesEmailsWithHash();
        $this->viewBag['machines'] = [
            'simple' => MachineManager::machineSerialization($simple),
            'highlighted' => MachineManager::machineSerialization($highlighted)
        ];
        $appName = $this->viewBag['app']['name'];
        foreach ($mails as $mail){
            $this->viewBag['unsubscribeHash'] = $mail['hash'];
            $currentMail = $mail['email'];
            Mail::queue('emails.machineList', $this->viewBag, function ($message) use ($appName, $currentMail) {
                $message->from('axonicdesign@gmail.com', $appName);
                $message->to($currentMail)
                        ->subject($appName . ' Machine Newsletters');
            });
        }
    }
    
    
    private function getMachinesForThisWeek() 
    {
        $machines = Machine::where('active', 1)
                ->where('highlighted', 0)
                ->where('created_at', '>=', Carbon::now()->startOfWeek())
                ->take(40)
                ->get();
        
        return $machines;
    }
    
    
    private function getAllActiveCompaniesEmailsWithHash() {
        $companies = Users::where('active', 1)
                            ->where('subscribed', 1)
                            ->get();
        $emails = [];
        foreach($companies as $company){
            $hashSchema = $company->email . HashList::UNSUBSCRIBE_HASH;
            $unsubscribeHash = Crypt::encrypt($hashSchema);
            $emails[] = [
                'email' => $company->email,
                'hash' => $unsubscribeHash
            ];
        }
        $emails[] = [
            'email' => 'info.shioric@gmail.com',
            'hash' => Crypt::encrypt('info.shioric@gmail.com' . HashList::UNSUBSCRIBE_HASH)
        ];
        $emails[] = [
            'email' => 'axonicdesign@gmail.com',
            'hash' => Crypt::encrypt('axonicdesign@gmail.com' . HashList::UNSUBSCRIBE_HASH)
        ];
        
        return $emails;
    }
    
}


