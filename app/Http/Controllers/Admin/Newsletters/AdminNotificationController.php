<?php

namespace App\Http\Controllers\Admin\Newsletters;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Persistent\Users\Users;
use App\Models\Persistent\Machine\Machine;
use View;

class AdminNotificationController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $this->viewBag['category'] = 'Newsletters';
    }
    
    public function showNotificationForm(){
        
        return View::make("admin.companies.search", $this->viewBag);
    }
    
}
