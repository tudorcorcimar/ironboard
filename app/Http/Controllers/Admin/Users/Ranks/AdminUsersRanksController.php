<?php

namespace App\Http\Controllers\Admin\Users\Ranks;

use App\Http\Controllers\Admin\BaseController;
use App\Services\Admin\AdminPermissionsManager;
use App\Models\Persistent\Admin\Rank;
use View;

class AdminUsersRanksController extends BaseController
{

    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_RANK, AdminPermissionsManager::PERMISSION_ACTION_SEARCH);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'users';
    }

    
    public function showUsersRanks(){
        $this->viewBag['subcategory'] = 'users_ranks';
        $ranks = Rank::all();
        
        $this->viewBag['ranks'] = [];
        
        foreach ($ranks as $rank){
            $permissions = json_decode($rank->permissions, true);
            
            $this->viewBag['ranks'][] = [
                'id' => $rank->id,
                'name' => $rank->name,
                'permissions' => $permissions
            ];
        }
                
        return View::make('admin.users.ranks.search', $this->viewBag);
    }

}
