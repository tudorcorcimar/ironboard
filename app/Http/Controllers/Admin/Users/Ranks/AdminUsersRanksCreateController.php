<?php

namespace App\Http\Controllers\Admin\Users\Ranks;

use App\Http\Controllers\Admin\BaseController;
use App\Services\Admin\AdminPermissionsManager;
use App\Services\ValidatorService;
use Illuminate\Support\Facades\Input;
use App\Models\Persistent\Admin\Rank;
use Illuminate\Support\Facades\Redirect;
use View;

class AdminUsersRanksCreateController extends BaseController
{

    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_RANK, AdminPermissionsManager::PERMISSION_ACTION_ADD);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'users';
    }

    
    public function showUsersRankCreate(){
        $this->viewBag['subcategory'] = 'users_ranks_create';
        $this->viewBag['form']['permissions'] = json_decode($this->createPermissionsJson(), true);
        
        return View::make('admin.users.ranks.create', $this->viewBag);
    }
    
    
    public function createUsersRank() {
        if($this->viewBag['userHasPermissions']){
            
            $this->viewBag['form'] = Input::all();

            $validationRules = [
                'name' => 'required|max:155|unique:rank'
            ];

            $validator = ValidatorService::validate($validationRules);
            if($validator->fails()){
                $this->viewBag['form']['permissions'] = json_decode($this->createPermissionsJson(), true);
                $this->viewBag['form']['errors'] = $validator->errors();
                return View::make("admin.users.ranks.create", $this->viewBag);
            }
            
            $permissionsJson = $this->createPermissionsJson();
            $this->saveRank($this->viewBag['form']['name'], $permissionsJson);
        
            return Redirect::to('/admin/users/ranks')->with([
                'successMessage' => 'Rank successfuly created!'
            ]);
            
        }
    }
    
    
    private function saveRank($name, $permissions) {
        $rank = new Rank;
        $rank->name = $name;
        $rank->permissions = $permissions;
        
        $rank->save();
        
        return true;
    }
    
    
    private function createPermissionsJson() {
        
        $types   = $this->adminPermissionsManager->getPermissionTypes();
        $actions = $this->adminPermissionsManager->getPermissionActions();
        
        $permissions = [];
        
        foreach ($types as $type){
            foreach ($actions as $action){
                $permissions[$type][$action] = false;
                if(isset($this->viewBag['form']['permissions'][$type][$action])){
                    $permissions[$type][$action] = true;
                }
            }
        }
        
        return json_encode($permissions);
    }

}
