<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Persistent\Admin\Admin;
use App\Services\Admin\AdminPermissionsManager;
use View;

class AdminUsersController extends BaseController
{

    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_USERS, AdminPermissionsManager::PERMISSION_ACTION_SEARCH);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'users';
    }

    public function showAdminUsers(){
        $this->viewBag['users'] = Admin::all();
        $this->viewBag['subcategory'] = 'search';
        
        return View::make('admin.users.search', $this->viewBag);
    }

}