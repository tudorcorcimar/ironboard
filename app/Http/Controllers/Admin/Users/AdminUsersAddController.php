<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Persistent\Admin\Admin;
use App\Services\Admin\AdminPermissionsManager;
use App\Models\Persistent\Admin\Rank;
use Illuminate\Support\Facades\Input;
use App\Services\ValidatorService;
use Illuminate\Support\Facades\Redirect;
use View;

class AdminUsersAddController extends BaseController
{

    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_USERS, AdminPermissionsManager::PERMISSION_ACTION_ADD);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'users';
        $this->viewBag['subcategory'] = 'users_add';
        $this->viewBag['ranks'] = Rank::all();
    }

    public function showAdminUsersAdd(){
        $this->viewBag['users'] = Admin::all();
        
        return View::make('admin.users.create', $this->viewBag);
    }

    public function addAdminUser(){
        if($this->viewBag['userHasPermissions']){
            $input = Input::all();

            $validationRules = [
                'name'         => 'required|max:255',
                'surname'      => 'required|max:255',
                'login'        => 'required|email|max:150|unique:admin',
                'password'     => 'required|max:150|confirmed',
                'permissions'  => 'required'
            ];

            $validator = ValidatorService::validate($validationRules);

            if($validator->fails()){
                $this->viewBag['form'] = $input;
                $this->viewBag['form']['errors'] = $validator->errors();
                return View::make("admin.users.create", $this->viewBag);
            }

            $this->saveNewUser($input);

            return Redirect::to('/admin/users')->with([
                'growlSuccess' => 'New user was successfuly added!'
            ]);
        }
    }
    
    private function saveNewUser($data){
        $admin = new Admin;
        if(isset($data['name'])){
            $admin->name = $data['name'];
        }
        if(isset($data['surname'])){
            $admin->surname = $data['surname'];
        }
        if(isset($data['login'])){
            $admin->login = $data['login'];
        }
        if(isset($data['password'])){
            $admin->password = $data['password'];
        }
        if(isset($data['permissions'])){
            $admin->permissions = $data['permissions'];
        }
        
        $admin->save();
    }

}
