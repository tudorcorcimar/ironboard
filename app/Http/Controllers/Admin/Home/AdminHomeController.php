<?php

namespace App\Http\Controllers\Admin\Home;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Persistent\Users\Users;
use App\Models\Persistent\Machine\Machine;
use View;

class AdminHomeController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $this->viewBag['category'] = 'home';
    }
    
    /*
     * Show sign in view
     * 
     * @return view
     */
    public function showHome(){
        $this->viewBag['adminStatistics'] = [
            'totalUsers' => Users::get()->count(),
            'totalMachines' => Machine::get()->count(),
            'subscribe' => Users::where('type', '=', '2')->where('subscribed', '=', 1)->count(),
            'unsubscribe' => Users::where('type', '=', '2')->where('subscribed', '=', 0)->count(),
            'highlightedUsers' => Users::where('highlighted', '=', 1)->count(),
            'activeUsers' => Users::where('active', '=', 1)->count(),
            'inactiveUsers' => Users::where('active', '=', 0)->count(),
            'activeMachines' => Machine::where('active', '=', 1)->count(),
            'inactiveMachines' => Machine::where('active', '=', 0)->count(),
            'highlightedMachines' => Machine::where('highlighted', '=', 1)->count(),
        ];
        
        return View::make("admin.home.home", $this->viewBag);
    }
    
}
