<?php

namespace App\Http\Controllers\Admin\Machines;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Persistent\Machine\Machine;
use App\Services\MachineManager;
use App\Services\ImageService;
use Illuminate\Support\Facades\Input;
use App\Services\ValidatorService;
use Illuminate\Support\Facades\Redirect;
use App\Models\Persistent\Users\Users;
use App\Services\Admin\AdminPermissionsManager;
use View;

class AdminMachinesEditController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_MACHINES, AdminPermissionsManager::PERMISSION_ACTION_EDIT);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'machines';
    }
    
    private $validationRules = [
        'type'        => 'required|integer',
        'status'      => "required|integer",
        'name'        => 'required|max:155',
        'currency'    => 'max:5',
        'price'       => "numeric",
        'other_url'   => 'url',
        'description' => 'required'
    ];
    
    private function saveMachine($data, $userId = null){
        $machine = new Machine;
        if(isset($data['name'])){
            $machine->name = $data['name'];
        }
        if(isset($data['status'])){
            $machine->status = $data['status'];
        }
        if(isset($data['type'])){
            $machine->type = $data['type'];
        }
        if(isset($data['description'])){
            $machine->description = $data['description'];
        }
        if(isset($data['currency'])){
            $machine->currency = $data['currency'];
        }
        if(isset($data['price'])){
            $machine->price = $data['price'];
        }
        if(isset($data['other_url'])){
            $machine->other_url = $data['other_url'];
        }
        if($userId){
            $machine->user_id = $userId;
        }
        $machine->active = true;
        $machine->save();
        return $machine;
    }
    
    public function showEditMachine($userId, $id){
        $this->viewBag['subcategory'] = 'machine_edit';
        $details = Machine::where('id', $id)->get()->first();
        $machine = MachineManager::machineSerialization([$details])[$details->id];
        $this->viewBag['form'] = $machine;
        
        $user = Users::where('id', '=', $machine['user_id'])->get()->first();
        $this->viewBag['form']['company_id'] = $user->id;
        $this->viewBag['form']['company_name'] = $user->company_name;
        
        if(isset($machine['images']) && $machine['images'] > 0){
            $imageService = new ImageService;
            $this->viewBag['form']['imageBase64'] = $imageService->getMachineImagesArrayInBase64($machine['images'], $machine['id'], $userId);
        }
        
        return View::make('admin.machines.edit', $this->viewBag);
    }
    
    public function removeMachine($machineId) {
        $machine = Machine::where('id', $machineId)
                            ->get()->first();
        if( $machine !== null ){
            $imageService = new ImageService;
            $imageService->deleteFolderWithImages($machine->id, $machine->user_id);
            $machine->delete();
        }
    }
    
    public function editMachine(){
        $this->viewBag['form'] = Input::all();
        $validator = ValidatorService::validate($this->validationRules);
        if($validator->fails()){
            $this->viewBag['form']['errors'] = $validator->errors();
            return View::make("admin.machines.edit", $this->viewBag);
        }
        
        $this->removeMachine($this->viewBag['form']['id']);
        $machine = $this->saveMachine($this->viewBag['form'], $this->viewBag['form']['company_id']);
        
        if( isset($this->viewBag['form']['imageBase64']) && count($this->viewBag['form']['imageBase64'] ) > 0){
            $imageService = new ImageService;
            $imageService->saveImages($this->viewBag['form']['imageBase64'], $machine->id, $this->viewBag['form']['company_id']);
        }
        
        return Redirect::to('/admin/machine/edit/' . $machine->user_id . '/' . $machine->id)->with([
            'successMessage' => 'Machine successfuly updated!'
        ]);
        
    }
    
    public function activateMachine($userId, $machineId) {
        $machine = Machine::where('id', $machineId)
                            ->get()->first();
        if( $machine !== null ){
            $machine->active = true;
            $machine->save();
        }
        return Redirect::to('/admin/company/edit/'.$userId)->with('successMessage', 'Machine was succesuful activated!');
    }
    
    public function deactivateMachine($userId, $machineId) {
        $machine = Machine::where('id', $machineId)
                            ->get()->first();
        if( $machine !== null ){
            $machine->active = false;
            $machine->save();
        }
        return Redirect::to('/admin/company/edit/'.$userId)->with('successMessage', 'Machine was succesuful deactivated!');
    }
    
    public function removeMachineWithRedirect($userId, $machineId) {
        $this->removeMachine($machineId);
        return Redirect::to('/admin/company/edit/'.$userId)->with('successMessage', 'Machine was succesuful removed!');
    }
    
    public function activateHighlightMachine($machineId) {
        $machine = Machine::where('id', $machineId)
                            ->get()->first();
        if( $machine !== null ){
            $machine->highlighted = true;
            $machine->save();
        }
        
        return Redirect::to('/admin/machines')->with('successMessage', 'Machine was succesuful highlighted!');
    }
    
    public function deactivateHighlightMachine($machineId) {
        $machine = Machine::where('id', $machineId)
                            ->get()->first();
        if( $machine !== null ){
            $machine->highlighted = false;
            $machine->save();
        }
        return Redirect::to('/admin/machines')->with('successMessage', 'Machine was succesuful highlighted!');
    }
    
}