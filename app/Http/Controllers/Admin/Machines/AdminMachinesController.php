<?php

namespace App\Http\Controllers\Admin\Machines;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Persistent\Machine\Machine;
use App\Services\MachineManager;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Services\Admin\AdminPermissionsManager;
use View;

class AdminMachinesController extends BaseController
{
    private $filters = [];
    private $request;
    
    public function __construct(Request $request) {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_MACHINES, AdminPermissionsManager::PERMISSION_ACTION_SEARCH);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->request = $request;
        $this->viewBag['category'] = 'machines';
    }
    
    public function search() {
        $this->viewBag['machines'] = $this->findMachines();
        return View::make('admin.machines.search', $this->viewBag);
    }
    
    public function filterSearch() {
        $this->resetSearch();
        $this->viewBag['machines'] = $this->findMachines();
        return View::make('admin.machines.search', $this->viewBag);
    }
    
    private function searchIsEmpty($filters) {
        $isEmpty = false;
        if( count(Input::all()) < 1 ){
            $isEmpty = true;
        }elseif(count(Input::all()) == 1 && Input::get('_url')){
            $isEmpty = true;
        }
        return $isEmpty;
    }
    
    private function findMachines(){
        $filters = $this->getSearchFilters();
        
        $wasReseted = $this->request->session()->get('wasReseted');
        if($wasReseted || ($this->searchIsEmpty($filters) && count($filters) < 4)){
            $filters['highlightedOnTop'] = false;
        }
        
        $this->viewBag['form'] = $filters;
        
        $machines = Machine::where('active', 1);
        
        $machines->whereHas('user', function ($query) {
            $query->where('active', '=', 1);
        });
        
        if(isset($filters['keywords'])){
            $machines->where(function ($query) use ($filters) {
                $query->where('name', 'LIKE', '%'.$filters['keywords'].'%');
                $query->orWhere('description', 'LIKE', '%'.$filters['keywords'].'%');
            });
        }
        
        if(isset($filters['highlightedOnTop']) && $filters['highlightedOnTop']){
            $machines->orderBy('highlighted', $filters['sortOrder']);
        }
        
        $machines->orderBy($filters['sortBy'], $filters['sortOrder']);
        $pagination = $machines->paginate($filters['pageSize']);
        
        return [
            'pagination' => $pagination,
            'sort' => [
                'by' => $filters['sortBy'],
                'order' => $filters['sortOrder']
            ],
            'items' => MachineManager::machineSerialization($pagination),
        ];
    }
    
    private function getSearchFilters() {
        $this->filters['sortBy'] = self::MACHINE_DEFAULT_SORT_BY;
        $this->filters['sortOrder'] = self::MACHINE_DEFAULT_SORT_ORDER;
        $this->filters['pageSize'] = self::MACHINE_DEFAULT_PAGE_SIZE;
        
        $this->addToFilters('sortBy', 'adminMachineSortBy');
        $this->addToFilters('sortOrder', 'adminMachineSortOrder');
        $this->addToFilters('pageSize', 'adminMachinePageSize');
        
        $this->addToFilters('keywords', 'adminMachineKeywords');
        
        $this->addToFilters('highlightedOnTop', 'adminMachineHighlightedOnTop');
        
        return $this->filters;
        
    }
    
    private function addToFilters($inputKey, $sessionKey) {
        $inputValue = Input::get($inputKey);
        
        if($inputValue && $inputValue !== ''){
            $this->filters[$inputKey] = $inputValue;
            $this->request->session()->put($sessionKey, $inputValue);
        }elseif($this->request->session()->has($sessionKey)){
            $this->filters[$inputKey] =  $this->request->session()->get($sessionKey);
        }
    }
    
    private function resetSearch() {
        $this->request->session()->forget('adminMachineSortBy');
        $this->request->session()->forget('adminMachineSortOrder');
        $this->request->session()->forget('adminMachinePageSize');
        $this->request->session()->forget('adminMachineKeywords');
        $this->request->session()->forget('adminMachineHighlightedOnTop');
    }
    
    public function clearFilters() {
        $this->resetSearch();
        return Redirect::to('/admin/machines');
    }
    
    
}
