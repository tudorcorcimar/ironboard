<?php

namespace App\Http\Controllers\Admin\Machines;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Persistent\Machine\Machine;
use App\Services\ValidatorService;
use Illuminate\Support\Facades\Input;
use App\Models\Persistent\Users\Users;
use App\Services\ImageService;
use Illuminate\Support\Facades\Redirect;
use App\Services\Admin\AdminPermissionsManager;
use View;

class AdminMachinesAddController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_MACHINES, AdminPermissionsManager::PERMISSION_ACTION_ADD);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'machines';
    }
    
    private $validationRules = [
        'type'        => 'required|integer',
        'status'      => "required|integer",
        'name'        => 'required|max:155',
        'currency'    => 'max:5',
        'price'       => "numeric",
        'other_url'   => 'url',
        'description' => 'required'
    ];
    
    public function showAddMachine($userId){
        $this->viewBag['subcategory'] = 'machine_add';
        $user = Users::where('id', '=', $userId)->get()->first();
        $this->viewBag['form']['company_id'] = $user->id;
        $this->viewBag['form']['company_name'] = $user->company_name;
        return View::make("admin.machines.add", $this->viewBag);
    }
    
    private function saveMachine($data){
        $machine = new Machine;
        $currentDate = date('Y-m-d');
        if(isset($data['name'])){
            $machine->name = $data['name'];
        }
        if(isset($data['status'])){
            $machine->status = $data['status'];
        }
        if(isset($data['type'])){
            $machine->type = $data['type'];
        }
        if(isset($data['description'])){
            $machine->description = $data['description'];
        }
        if(isset($data['currency'])){
            $machine->currency = $data['currency'];
        }
        if(isset($data['price'])){
            $machine->price = $data['price'];
        }
        if(isset($data['other_url'])){
            $machine->other_url = $data['other_url'];
        }
        if(isset($data['updated_at'])){
            $machine->updated_at = $currentDate;
        }
        if(isset($data['company_id'])){
            $machine->user_id = $data['company_id'];
        }
        $machine->active = true;
        $machine->save();
        return $machine;
    }
    
    public function addMachine(){
        $this->viewBag['form'] = Input::all();
        $validator = ValidatorService::validate($this->validationRules);
        if($validator->fails()){
            $this->viewBag['form']['errors'] = $validator->errors();
            return View::make("admin.machines.add", $this->viewBag);
        }
        $machine = $this->saveMachine($this->viewBag['form']);
        
        if(isset($this->viewBag['form']['imageBase64']) && count($this->viewBag['form']['imageBase64']) > 0){
            $imageService = new ImageService;
            $imageService->saveImages($this->viewBag['form']['imageBase64'], $machine->id, $this->viewBag['form']['company_id']);
        }
        
        return Redirect::to('/admin/machine/edit/' . $machine->user_id . '/' . $machine->id)->with([
            'successMessage' => 'Machine successfuly created!'
        ]);
        
    }
    
}
