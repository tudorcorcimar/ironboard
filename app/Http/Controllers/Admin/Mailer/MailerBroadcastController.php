<?php

namespace App\Http\Controllers\Admin\Mailer;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\View;
use App\Services\Admin\AdminPermissionsManager;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use App\Models\Persistent\Users\Broadcasts;
use Illuminate\Support\Facades\DB;
use App\Models\Persistent\Users\Users;
use Illuminate\Support\Facades\Crypt;
use App\Models\Persistent\Hash\HashList;

class MailerBroadcastController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_MAILER, AdminPermissionsManager::PERMISSION_ACTION_ADD);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'mailer';
    }
    
    public function showBroadcasts() {
        $this->viewBag['subcategory'] = 'broadcast';
        $this->viewBag['broadcasts'] = DB::table('broadcasts')
                                        ->select('broadcasts.*', 'users.company_name')
                                        ->join('users', 'users.id', '=', 'broadcasts.company_id')
                                        ->get();
        return View::make('admin.mailer.broadcast', $this->viewBag);
    }
    
    public function createBroadcast() {
        $input = Input::all();
        $broadcast = new Broadcasts;
        if( isset($input['company_id']) ){
            $broadcast->company_id = $input['company_id'];
            
            if($input['description']){
                $broadcast->description = $input['description'];
            }
            
            $broadcast->save();
        }
        $this->showBroadcasts();
    }
    
    public function broadcastCompany(){
        
        $mails = $this->getAllActiveCompaniesEmailsWithHash();

        $appName = $this->viewBag['app']['name'];
        foreach ($mails as $mail){
            $this->viewBag['unsubscribeHash'] = $mail['hash'];
            $currentMail = $mail['email'];
            Mail::queue('emails.broadcasts.omap', $this->viewBag, function ($message) use ($appName, $currentMail) {
                $message->from('axonicdesign@gmail.com', $appName);
                $message->to($currentMail)
                        ->subject('O.M.A.P Officine Meccaniche Alta Precisione');
            });
        }
    }
    
    
    private function getAllActiveCompaniesEmailsWithHash() {
        $companies = Users::where('active', 1)
                            ->where('subscribed', 1)
                            ->get();
        $emails = [];
        foreach($companies as $company){
            $hashSchema = $company->email . HashList::UNSUBSCRIBE_HASH;
            $unsubscribeHash = Crypt::encrypt($hashSchema);
            $emails[] = [
                'email' => $company->email,
                'hash' => $unsubscribeHash
            ];
        }
        $emails[] = [
            'email' => 'info.shioric@gmail.com',
            'hash' => Crypt::encrypt('info.shioric@gmail.com' . HashList::UNSUBSCRIBE_HASH)
        ];
        $emails[] = [
            'email' => 'administration@ibmachine.com',
            'hash' => Crypt::encrypt('axonicdesign@gmail.com' . HashList::UNSUBSCRIBE_HASH)
        ];
        
        return $emails;
    }
    
}
