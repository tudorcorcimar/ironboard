<?php

namespace App\Http\Controllers\Admin\Mailer;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\Input;
use App\Services\ValidatorService;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use App\Services\Admin\AdminPermissionsManager;

class MailerSendController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $neddedPermissions = $this->adminPermissionsManager->getNeededPermissions(AdminPermissionsManager::PERMISSION_TYPE_MAILER, AdminPermissionsManager::PERMISSION_ACTION_ADD);
        $this->viewBag['userHasPermissions'] = $this->adminPermissionsManager->userHasPermission($this->viewBag['user'], $neddedPermissions);
        $this->viewBag['category'] = 'mailer';
    }
    
    public function showMailForm() {
        $this->viewBag['subcategory'] = 'send';
        return View::make("admin.mailer.send", $this->viewBag);
    }
    
    public function prepareForm() {
        $this->viewBag['form'] = Input::all();
        $rules =[
            'mailTo'       => 'required||email|min:5',
            'subject'      => 'required|min:5',
            'message'      => 'required|min:5'
        ];
        
        $validator = ValidatorService::validate($rules);
        if($validator->fails()){
            $this->viewBag['form']['errors'] = $validator->errors();
            return View::make("admin.mailer.send", $this->viewBag);
        }else{
            $this->sendMail();
            return Redirect::to('/admin/mailer/send')->with([
                'successMessage' => 'Your message successfully sent!'
            ]);
        }
    }
    
    private function sendMail() {
        $data = $this->viewBag;
        $data['form'] = Input::all();
        Mail::send('emails.mailer.send', $data, function ($m) use ($data) {
            $m->from( 'info@ibmachine.com', $data['app']['name'] );
            $m->to( $data['form']['mailTo'] )
              ->subject( $data['form']['subject'] );
        });
    }
    
}
