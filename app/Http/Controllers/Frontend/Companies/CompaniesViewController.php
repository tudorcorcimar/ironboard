<?php

namespace App\Http\Controllers\Frontend\Companies;

use App\Http\Controllers\Frontend\BaseController;
use App\Models\Persistent\Users\Users;
use App\Models\Persistent\Machine\Machine;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use App\Services\ImageService;
use App\Services\MachineManager;

class CompaniesViewController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $this->viewBag['module'] = 'companies';
    }
    
    private function getUserMachine($userId){
        return Machine::where('user_id', $userId)
            ->where('active', 1)
            ->orderBy('updated_at', 'desc')->get();
    }
    
    private function getUserDetails($userId){
        $user = Users::where('id', '=', $userId)
                ->get()
                ->first();
        return $user;
    }
    
    public function showCompaniesView($id) {
        $user = $this->getUserDetails($id);
        if($user->active){
            $imageService = new ImageService;
            $this->viewBag['company'] = [];
            $this->viewBag['company']['details'] = $user;
            $images = $imageService->getImages(false, $this->viewBag['company']['details']['id'], true);
            if(count($images) > 0){
                $this->viewBag['company']['logo'] = $images[0];
            }
            $this->viewBag['company']['locations'] = $user->usersLocations;
            
            $machines = $this->getUserMachine($id);
            $this->viewBag['company']['machine'] = MachineManager::machineSerialization($machines);
            return View::make("frontend.companies.view", $this->viewBag);
        }
        return Redirect::to('/companies')->with('growlSuccess', "You can't access this company!");
        
    }
    
}
