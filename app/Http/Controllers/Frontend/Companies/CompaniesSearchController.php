<?php

namespace App\Http\Controllers\Frontend\Companies;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\View;
use App\Services\CompaniesManager;
use App\Models\Persistent\Users\Users;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CompaniesSearchController extends BaseController
{
    private $filters = [];
    private $request;
    
    public function __construct(Request $request) {
        parent::__construct();
        $this->request = $request;
        $this->viewBag['module'] = 'companies';
    }
    
    public function search() {
        $this->viewBag['companies'] = $this->findCompanies();
        return View::make('frontend.companies.board', $this->viewBag);
    }
    
    public function filterSearch() {
        $this->resetSearch();
        $this->viewBag['companies'] = $this->findCompanies();
        return View::make('frontend.companies.board', $this->viewBag);
    }
    
    private function findCompanies(){
        $filters = $this->getSearchFilters();
        $this->viewBag['form'] = $filters;
        
        $companies = Users::where('active', 1)
                           ->where('type', 2);
                   
        if(isset($filters['keywords'])){
            $companies->where(function ($query) use ($filters) {
                $query->where('company_name', 'LIKE', '%'.$filters['keywords'].'%');
                $query->orWhere('description', 'LIKE', '%'.$filters['keywords'].'%');
            });
        }
        
        if(isset($filters['locations']) && count($filters['locations']) > 0){
            $companies->join('users_locations', 'users_locations.users_id', '=', 'users.id')
                      ->whereIn('users_locations.location_id', $filters['locations'])
                      ->select('*');
        }
        
        $companies->orderBy($filters['sortBy'], $filters['sortOrder']);
        $pagination = $companies->paginate($filters['pageSize']);
        
        return [
            'pagination' => $pagination,
            'sort' => [
                'by' => $filters['sortOrder'],
                'order' => $filters['sortBy']
            ],
            'items' => CompaniesManager::companySerialization($pagination),
        ];
        
    }
    
    private function getSearchFilters() {
        $this->filters['sortBy'] = self::COMPANY_DEFAULT_SORT_BY;
        $this->filters['sortOrder'] = self::COMPANY_DEFAULT_SORT_ORDER;
        $this->filters['pageSize'] = self::COMPANY_DEFAULT_PAGE_SIZE;
        
        $this->addToFilters('sortBy', 'companiesSortBy');
        $this->addToFilters('sortOrder', 'companiesSortOrder');
        $this->addToFilters('pageSize', 'companiesPageSize');
        
        $this->addToFilters('keywords', 'companiesKeywords');
        $this->addToFilters('locations', 'companiesLocations');
        
        return $this->filters;
    }
    
    private function addToFilters($inputKey, $sessionKey) {
        $inputValue = Input::get($inputKey);
        if($inputValue && $inputValue !== ''){
            $this->filters[$inputKey] = $inputValue;
            $this->request->session()->put($sessionKey, $inputValue);
        }elseif($this->request->session()->has($sessionKey)){
            $this->filters[$inputKey] =  $this->request->session()->get($sessionKey);
        }
    }
    
    private function resetSearch() {
        $this->request->session()->forget('companiesSortBy');
        $this->request->session()->forget('companiesSortOrder');
        $this->request->session()->forget('companiesPageSize');
        $this->request->session()->forget('companiesKeywords');
        $this->request->session()->forget('companiesLocations');
    }
    
    public function clearFilters() {
        $this->resetSearch();
        return Redirect::to('/companies');
    }
    
}
