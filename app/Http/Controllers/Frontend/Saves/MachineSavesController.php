<?php

namespace App\Http\Controllers\Frontend\Saves;

use App\Http\Controllers\Frontend\BaseController;
use App\Models\Persistent\Saves\SavedMachines;
use Illuminate\Http\Request;

class MachineSavesController extends BaseController
{
    private $request;
    
    public function __construct(Request $request) {
        parent::__construct();
        $this->request = $request;
    }
    
    public function saveMachine() {
        $machineId = $this->request->input('machine.id');
        $userId = $this->viewBag['user']['id'];
        
        if(isset($machineId) && isset($userId)){
            
            $machineSave = new SavedMachines;
            $currentDate = date('Y-m-d H:i:s');
            
            $machineSave->created_at = $currentDate;
            $machineSave->machine_id = $machineId;
            $machineSave->users_id = $userId;
            
            $machineSave->save();
            
            $response = [
                'type' => 'success',
                'message' => 'Macchine succssfuly saved!'
            ];

            return response()->json($response);
        }
        
        $response = [
            'type' => 'info',
            'message' => 'An error has occurred. Please contact your system administrator!'
        ];
        return response()->json($response);
    }
    
    public function unsaveMachine() {
        $machineId = $this->request->input('machine.id');
        $userId = $this->viewBag['user']['id'];
        
        if(isset($machineId) && isset($userId)){
            
            $machineSave = SavedMachines::where('machine_id', '=', $machineId)
                                        ->where('users_id', '=', $userId);
            
            $machineSave->delete();
            
            $response = [
                'type' => 'success',
                'message' => 'Saving has been successfully removed!'
            ];

            return response()->json($response);
        }
        
        $response = [
            'type' => 'info',
            'message' => 'An error has occurred. Please contact your system administrator!'
        ];
        return response()->json($response);
    }
    
}
