<?php

namespace App\Http\Controllers\Frontend\Saves;

use App\Http\Controllers\Frontend\BaseController;
use App\Models\Persistent\Saves\SavedSearch;

class SearchSavesController extends BaseController
{
    const SAVE_TYPE_MACHINE = 'machine';
    const SAVE_TYPE_COMPANY = 'company';
    
    public function __construct() {
        parent::__construct();
    }
    
    public function saveSearch($filters, $type) {
        $userId = $this->viewBag['user']['id'];
        $savedSearchExists = SavedSearch::where('name', '=', $filters['save-name'])
                                        ->where('users_id', '=', $userId)->get();
        
        if( $savedSearchExists->count() < 1 ){
            
            unset($filters['save-search']);
            $filters['save-type'] = $type;

            $savedSearch = new SavedSearch;
            $savedSearch->name = $filters['save-name'];
            
            unset($filters['save-name']);
            $savedSearch->users_id = $userId;
            
            $jsonFilters = json_encode($filters);
            $savedSearch->json_filters = $jsonFilters;
            
            $savedSearch->save();
            
            return 'Search was successfuly saved!';
        }else{
            return 'You already have a seach with this name!';
        }
    }
    
}
