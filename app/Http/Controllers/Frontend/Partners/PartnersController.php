<?php

namespace App\Http\Controllers\Frontend\Partners;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\View;

class PartnersController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $this->viewBag['module'] = 'partners';
    }
    
    public function showPartners() {
        return View::make("frontend.partners.view", $this->viewBag);
    }
        
}
