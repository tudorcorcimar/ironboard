<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Models\Persistent\Users\Users;
use App\Models\Persistent\Users\UserType;
use App\Http\Controllers\Frontend\BaseController;
use App\Models\Persistent\Users\UsersSettings;
use App\Services\ValidatorService;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use View;

class RegisterController extends BaseController
{
    public function __construct() {
        parent::__construct();
        $this->viewBag['module'] = 'register';
    }
    
    /*
     * Validation rules
     * 
     * @type array
     */
    private $validationRules = [
        'individual' => [
            'name'       => 'required|max:150',
            'email'      => "required|email|max:150|unique:users",
            'type'       => 'required|max:2',
            'password'   => 'required|max:150|confirmed|min:4',
            'created_at' => 'date',
            'updated_at' => 'date',
            'captcha'    => 'required|captcha'
        ],
        'company' => [
            'company_name' => 'required|max:255|unique:users',
            'email'        => 'required|email|max:150|unique:users',
            'type'         => 'required|max:2',
            'password'     => 'required|max:150|confirmed',
            'description'  => 'string',
            'created_at'   => 'date',
            'updated_at'   => 'date',
            'captcha'      => 'required|captcha'
        ]
    ];
    
    /*
     * Create new user in database
     * 
     * @param array $data
     * @return void
     */
    private function createNewUser($data){
        $user = new Users;
        $currentDate = date('Y-m-d');
        if(isset($data['name'])){
            $user->name = $data['name'];
        }
        if(isset($data['surname'])){
            $user->surname = $data['surname'];
        }
        if(isset($data['company_name'])){
            $user->company_name = $data['company_name'];
        }
        if(isset($data['email'])){
            $user->email = $data['email'];
        }
        if(isset($data['type'])){
            $user->type = $data['type'];
        }
        if(isset($data['phone'])){
            $user->phone = $data['phone'];
        }
        if(isset($data['password'])){
            $user->password = $data['password'];
        }
        if(isset($data['created_at'])){
            $user->created_at = $currentDate;
        }
        if(isset($data['updated_at'])){
            $user->updated_at = $currentDate;
        }
        if(isset($data['verificationKey'])){
            $user->hash = $data['verificationKey'];
        }
        $user->save();
        
        $userSettings = new UsersSettings;
        $userSettings->user_id = $user->id;
        $userSettings->save();
    }
    
    /*
     * Get validation rules
     * 
     * @param array $userType
     * @return array validation rules and user type
     */
    public function getRules($userType){
        $rules = [];
        if($userType == UserType::USER_INDIVIDUAL){
            $rules = $this->validationRules['individual'];
        }else{
            $rules = $this->validationRules['company'];
        }
        return $rules;
    }
    
    /*
     * Get registration user type
     * 
     * @param array $userType
     * @return array validation rules and user type
     */
    protected function getRegistrationUserType($formType = 1){
        $type = intval($formType);
        $userType = UserType::USER_GUEST;
        if($formType && $formType == UserType::USER_INDIVIDUAL){
            $userType = UserType::USER_INDIVIDUAL;
        }else if($formType && $formType == UserType::USER_COMPANY){
            $userType = UserType::USER_COMPANY;
        }
        return $userType;
    }
    
    /*
     * Show register view
     * 
     * @return void
     */
    public function showRegister() {
        $this->viewBag['form']['userType'] = $this->getRegistrationUserType();
        return View::make("frontend.auth.register", $this->viewBag);
    }
    
    /*
     * Send verification key
     * 
     * @return void
     */
    protected function sendRegisterVarificationKey($type){
        $data = $this->viewBag;
        $data['userName'] = $type == UserType::USER_COMPANY ? $data['form']['company_name'] : $data['form']['name']; 
        Mail::send('frontend.email.auth.verification', $data, function ($m) use ($data) {
            $m->from($data['app']['email'], $data['app']['name']);
            $m->to($data['form']['email'], $data['userName'] )
              ->subject('Register verification ' . $data['app']['name']);
        });
    }
    
    /*
     * Prepare registration for individual form after POST
     * 
     * @return redirect || view
     */
    public function register() {
        $this->viewBag['form'] = [];
        $this->viewBag['form'] = Input::all();
        $this->viewBag['form']['userType'] = $this->getRegistrationUserType($this->viewBag['form']['type']);
        $this->viewBag['form']['verificationKey'] = Hash::make($this->viewBag['form']['email']);
        $rules = $this->getRules($this->viewBag['form']['userType']);
        $validator = ValidatorService::validate($rules);
        
        if($validator->fails()){
            $this->viewBag['form']['errors'] = $validator->errors();
            return View::make("frontend.auth.register", $this->viewBag);
        }else{
            $this->createNewUser($this->viewBag['form']);
            $this->sendRegisterVarificationKey($this->viewBag['form']['userType']);
            return Redirect::to('/auth/register')->with([
                'growlSuccess' => 'Your are successful registred!',
                'registerMessage' => 'Please check your email for activation link!'
            ]);
        }
    }
    
    /*
     * Verify registration key
     * 
     * @return void
     */
    public function verifyRegistrationKey() {
        if(Input::get('key')){
            $key = Input::get('key');
            $email = Input::get('email');
            if ( $key && $email && Hash::check($email, $key) ) {
                
                $user = Users::where('email', '=', $email)->first();
                if(!!$user){
                    $user->active = 1;
                    $user->hash = "";
                    $user->save();

                    $this->viewBag['confirmation'] = 'success';
                    return View::make("frontend.auth.view", $this->viewBag);
                }
            }
        }
        return Redirect::to('/auth/signin')->with([
            'growlSuccess' => 'Your account succesuful activated!',
            'activatedMessage' => 'Your account succesuful activated!<br/>'
                                . 'Please sign in to start useing our services.'
        ]);
    }
    
    
}
