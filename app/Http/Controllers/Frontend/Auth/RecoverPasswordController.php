<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\Input;
use App\Services\ValidatorService;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Models\Persistent\Users\Users;
use View;

class RecoverPasswordController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
    }
    
    protected $validationRules = [
        'email'      => "required|email|max:150|exists:users,email",
        'captcha'    => 'required|captcha'
    ];
    
    /*
     * Show recover password view
     * 
     * @return view
     */
    public function showRecoverPassword(){
        return View::make("frontend.auth.recover", $this->viewBag);
    }
    
    /*
     * Send mail with current password
     * 
     * @return boolean
     */
    private function sendRecoverPassword($data){
        $data['app'] = $this->viewBag['app'];
        Mail::send('frontend.email.auth.recoverPassword', $data, function ($m) use ($data) {
            $m->from($data['app']['email'], $data['app']['name']);
            $m->to($data['user']['email'], $data['user']['company_name'] )
              ->subject( $data['app']['name'] . ': ' . $data['user']['company_name'] . ' password recovery! ');
        });
    }
    
    /*
     * Recover password
     * 
     * @return view
     */
    public function recoverPassword(){
        $this->viewBag['form'] = Input::all();
        $validator = ValidatorService::validate($this->validationRules);
        if($validator->fails()){
            $this->viewBag['form']['errors'] = $validator->errors();
            return View::make("frontend.auth.recover", $this->viewBag);
        }else{
            $this->viewBag['user'] = Users::where('email', $this->viewBag['form']['email'])->first();
            $this->sendRecoverPassword($this->viewBag);
            return Redirect::to('/auth/recover')->with('recoverWasSentToEmail', 'true');
        }
    }
    
}
