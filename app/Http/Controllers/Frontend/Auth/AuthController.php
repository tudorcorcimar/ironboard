<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Models\Runtime\Auth\Auth;
use View;

class AuthController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
    }
    
    /*
     * Show sign in view
     * 
     * @return view
     */
    public function showSignIn(){
        return View::make("frontend.auth.view", $this->viewBag);
    }
    
    /*
     * Sign in
     * 
     * @return redirect || view
     */
    public function signIn(){
        Auth::getInstance()->login(Input::get('email'), Input::get('password'));
        $this->viewBag['form'] = Input::all();
        if(Auth::getInstance()->getIsAuth()){
            return Redirect::to('/profile');
        }
        if(!Auth::getInstance()->getActive()){
            $this->viewBag['form']['error'] = 'Your account is not activated. Please check your mail for the activation link!';
        }else{
            $this->viewBag['form']['error'] = 'Your email or password is incorrect. Please try again.';
        }
        return View::make("frontend.auth.view", $this->viewBag);
    }
    
    /*
     * Sign out
     * 
     * @return redirect
     */
    public function signOut(){
        Auth::getInstance()->logout();
        return Redirect::to('/');
    }
    
    
    
}
