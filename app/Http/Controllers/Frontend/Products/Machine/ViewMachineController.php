<?php

namespace App\Http\Controllers\Frontend\Products\Machine;

use App\Http\Controllers\Frontend\Products\ProductsController;
use Illuminate\Support\Facades\View;
use App\Models\Persistent\Machine\Machine;
use App\Services\MachineManager;
use Illuminate\Support\Facades\Redirect;

class ViewMachineController extends ProductsController
{
    
    public function __construct() {
        parent::__construct();
        $this->viewBag['module'] = 'machine';
    }
    
    public function viewMachine($id) {
        $details = Machine::where('id', $id)
                   ->where('active', 1)
                   ->get()->first();
        if($details && $details->active){
            $machine = MachineManager::machineSerialization([$details])[$details->id];
            $this->viewBag['post'] = $machine;
            $this->viewBag['owner'] = MachineManager::getMachineOwnerByUserId($details->user_id);
            return View::make('frontend.machine.view', $this->viewBag);
        }
        return Redirect::to('/');
    }
    
    
}
