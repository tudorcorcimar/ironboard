<?php

namespace App\Http\Controllers\Frontend\Products\Machine;

use App\Http\Controllers\Frontend\Products\ProductsController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use App\Models\Persistent\Machine\Machine;
use App\Services\ValidatorService;
use App\Services\ImageService;
use App\Services\MachineManager;
use Illuminate\Support\Facades\Redirect;

class EditMachineController extends ProductsController
{
    
    public function __construct() {
        parent::__construct();
        $this->viewBag['module'] = 'machine';
    }
    
    /*
     * Validation rules
     * 
     * @type array
     */
    private $validationRules = [
        'type'        => 'required|integer',
        'status'      => "required|integer",
        'name'        => 'required|max:155',
        'currency'    => 'max:5',
        'price'       => "numeric",
        'other_url'   => 'url',
        'description' => 'required',
        'updated_at'  => 'date',
        'created_at'  => 'date',
        'captcha'     => 'required|captcha'
    ];
    
    /*
     * Create new machine
     * 
     * @param array $data
     * @return $id machine id
     */
    private function saveMachine($data, $userId = null){
        $machine = new Machine;
        $currentDate = date('Y-m-d');
        if(isset($data['name'])){
            $machine->name = $data['name'];
        }
        if(isset($data['status'])){
            $machine->status = $data['status'];
        }
        if(isset($data['type'])){
            $machine->type = $data['type'];
        }
        if(isset($data['description'])){
            $machine->description = $data['description'];
        }
        if(isset($data['currency'])){
            $machine->currency = $data['currency'];
        }
        if(isset($data['price'])){
            $machine->price = $data['price'];
        }
        if(isset($data['other_url'])){
            $machine->other_url = $data['other_url'];
        }
        if(isset($data['updated_at'])){
            $machine->updated_at = $currentDate;
        }
        if($userId){
            $machine->user_id = $userId;
        }
        $machine->active = true;
        $machine->save();
        return $machine;
    }
    
    /*
     * Save Machine
     * 
     * @return bool
     */
    private function isMachineOwner($machineId, $userId){
        return $machineId == $userId;
    }
    
    /*
     * Remove Machine
     * 
     * @return redirect || object view
     */
    private function removeMachine($machineId) {
        $machine = Machine::where('user_id', $this->viewBag['user']['id'])
                                       ->where('id', $machineId)
                                       ->get()->first();
        if( $machine !== null ){
            $imageService = new ImageService;
            $imageService->deleteFolderWithImages($machineId, $this->viewBag['user']['id']);
            
            $machine = Machine::find($machineId);
            $machine->delete();
        }
        return Redirect::to('/profile/postings')->with('growlSuccess', 'Machine was succesuful removed!');
    }
    
    /*
     * Edit new machine
     * 
     * @return object view
     */
    public function showEditMachine($id){
        $details = Machine::where('id', $id)->get()->first();
        
        $isMachineOwner = $this->isMachineOwner($details->user_id, $this->viewBag['user']['id']);
        if( !!$details && $isMachineOwner ){
            $machine = MachineManager::machineSerialization([$details])[$details->id];
            $this->viewBag['form'] = $machine;
            if(isset($machine['images']) && $machine['images'] > 0){
                $imageService = new ImageService;
                $this->viewBag['form']['imageBase64'] = $imageService->getMachineImagesArrayInBase64($machine['images'], $machine['id'], $this->viewBag['user']['id']);
            }
            return View::make('frontend.machine.edit', $this->viewBag);
        }
        return Redirect::to('/');
    }
    
    /*
     * Save Machine
     * 
     * @return redirect || object view
     */
    public function editMachine(){
        $details = Machine::where('id', Input::get('id'))->get()->first();
        $isMachineOwner = $this->isMachineOwner($details->user_id, $this->viewBag['user']['id']);
        if( !!$details && $isMachineOwner ){
            $this->viewBag['form'] = Input::all();
            $validator = ValidatorService::validate($this->validationRules);
            if($validator->fails()){
                $this->viewBag['form']['errors'] = $validator->errors();
                return View::make("frontend.machine.edit", $this->viewBag);
            }else{
                $this->removeMachine($this->viewBag['form']['id']);
                $machine = $this->saveMachine($this->viewBag['form'], $this->viewBag['user']['id']);
                if( isset($this->viewBag['form']['imageBase64']) && count($this->viewBag['form']['imageBase64'] ) > 0){
                    $imageService = new ImageService;
                    $imageService->saveImages($this->viewBag['form']['imageBase64'], $machine->id, $this->viewBag['user']['id']);
                }
                return Redirect::to('/profile/postings')->with('growlSuccess', 'Machine was succesuful Updated!');
            }
        }
        
        return Redirect::to('/');
    }
    
}
