<?php

namespace App\Http\Controllers\Frontend\Products\Machine;

use App\Http\Controllers\Frontend\Products\ProductsController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use App\Models\Persistent\Machine\Machine;
use App\Services\ValidatorService;
use App\Services\ImageService;
use Illuminate\Support\Facades\Redirect;

class PostMachineController extends ProductsController
{
    public function __construct() {
        parent::__construct();
        $this->viewBag['module'] = 'postmachine';
    }
    
    /*
     * Validation rules
     * 
     * @type array
     */
    private $validationRules = [
        'type'        => 'required|integer',
        'status'      => "required|integer",
        'name'        => 'required|max:155',
        'currency'    => 'max:5',
        'price'       => "numeric",
        'other_url'   => 'url',
        'description' => 'required',
        'updated_at'  => 'date',
        'created_at'  => 'date',
        'captcha'     => 'required|captcha'
    ];
    
    /*
     * Create new machine
     * 
     * @param array $data
     * @return $id machine id
     */
    private function saveMachine($data, $userId = null){
        $machine = new Machine;
        $currentDate = date('Y-m-d H:i:s');
        
        if(isset($data['name'])){
            $machine->name = $data['name'];
        }
        if(isset($data['status'])){
            $machine->status = $data['status'];
        }
        if(isset($data['type'])){
            $machine->type = $data['type'];
        }
        if(isset($data['description'])){
            $machine->description = $data['description'];
        }
        if(isset($data['currency'])){
            $machine->currency = $data['currency'];
        }
        if(isset($data['price'])){
            $machine->price = $data['price'];
        }
        if(isset($data['other_url'])){
            $machine->other_url = $data['other_url'];
        }
        if(isset($data['created_at'])){
            $machine->created_at = $currentDate;
        }
        if(isset($data['updated_at'])){
            $machine->updated_at = $currentDate;
        }
        if($userId){
            $machine->user_id = $userId;
        }
        $machine->active = true;
        $machine->save();
        return $machine;
    }
    
    /*
     * Create new machine
     * 
     * @return object view
     */
    public function showPostMachine(){
        return View::make('frontend.machine.post', $this->viewBag);
    }
    
    /*
     * Post Machine
     * 
     * @return redirect || object view
     */
    public function postMachine(){
        $this->viewBag['form'] = Input::all();
        $validator = ValidatorService::validate($this->validationRules);
        if($validator->fails()){
            $this->viewBag['form']['errors'] = $validator->errors();
            return View::make("frontend.machine.post", $this->viewBag);
        }else{
            $machine = $this->saveMachine($this->viewBag['form'], $this->viewBag['user']['id']);
            if(isset($this->viewBag['form']['imageBase64']) && count($this->viewBag['form']['imageBase64']) > 0){
                $imageService = new ImageService;
                $imageService->saveImages($this->viewBag['form']['imageBase64'], $machine->id, $this->viewBag['user']['id']);
            }
            return Redirect::to('/profile/postings')->with('growlSuccess', 'Machine was succesuful posted!');
        }
    }
    
}
