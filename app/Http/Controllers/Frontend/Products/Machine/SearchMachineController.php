<?php

namespace App\Http\Controllers\Frontend\Products\Machine;

use App\Http\Controllers\Frontend\Products\ProductsController;
use Illuminate\Support\Facades\View;
use App\Models\Persistent\Machine\Machine;
use Illuminate\Support\Facades\Input;
use App\Services\MachineManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\Persistent\Machine\MachineStatuses;
use App\Http\Controllers\Frontend\Saves\SearchSavesController;

class SearchMachineController extends ProductsController
{
    private $filters = [];
    private $request;
    
    public function __construct(Request $request) {
        parent::__construct();
        $this->request = $request;
        $this->viewBag['module'] = 'machine';
    }
    
    public function search() {
        $this->viewBag['machines'] = $this->findMachines();
        return View::make('frontend.machine.board', $this->viewBag);
    }
    
    public function filterSearch() {
        $this->resetSearch();
        $this->viewBag['machines'] = $this->findMachines();
        return View::make('frontend.machine.board', $this->viewBag);
    }
    
    private function searchIsEmpty($filters) {
        $isEmpty = false;
        if( count(Input::all()) < 1 ){
            $isEmpty = true;
        }elseif(count(Input::all()) == 1 && Input::get('_url')){
            $isEmpty = true;
        }
        return $isEmpty;
    }
    
    private function findMachines(){
        $filters = $this->getSearchFilters();
        
        $wasReseted = $this->request->session()->get('wasReseted');
        if($wasReseted || ($this->searchIsEmpty($filters) && count($filters) < 4)){
            $filters['highlightedOnTop'] = true;
        }
        
        $this->viewBag['form'] = $filters;
        
        $machines = Machine::where('active', 1);
        
        $machines->whereHas('user', function ($query) {
            $query->where('active', '=', 1);
        });
        
        if(isset($filters['keywords'])){
            $machines->where(function ($query) use ($filters) {
                $query->where('name', 'LIKE', '%'.$filters['keywords'].'%');
                $query->orWhere('description', 'LIKE', '%'.$filters['keywords'].'%');
            });
        }
        
        if(isset($filters['locations']) && count($filters['locations']) > 0){
            $machines->join('users_locations', 'users_locations.users_id', '=', 'machine.user_id')
                      ->whereIn('users_locations.location_id', $filters['locations'])
                      ->select('*');
        }
        
        if(isset($filters['types']) && count($filters['types']) > 0){
            $machines->whereIn('type', $filters['types']);
        }
        if(isset($filters['status']) && $filters['status'] !== ''){
            $machines->where('status', $filters['status']);
        }
        
        if(isset($filters['highlightedOnTop']) && $filters['highlightedOnTop']){
            $machines->orderBy('highlighted', $filters['sortOrder']);
        }
        
        $clonedMachine = clone $machines; 
        $newMachines = $clonedMachine->where('status', MachineStatuses::STATUS_NEW);
        
        $machines->orderBy($filters['sortBy'], $filters['sortOrder']);
        $pagination = $machines->paginate($filters['pageSize']);
        
        if(Input::get('save-search') && isset($this->viewBag['user']['id'])){
            $saveSearch = new SearchSavesController;
            $this->viewBag['growlSuccess'] = $saveSearch->saveSearch(Input::all(), SearchSavesController::SAVE_TYPE_MACHINE);
            $this->addToViewBagUserSaves();
        }
        
        return [
            'pagination' => $pagination,
            'sort' => [
                'by' => $filters['sortBy'],
                'order' => $filters['sortOrder']
            ],
            'searchStatistics' => [
                'total' => $pagination->total(),
                'new' => $newMachines->count(),
                'used' => $pagination->total() - $newMachines->count(),
            ],
            'items' => MachineManager::machineSerialization($pagination)
        ];
    }
    
    private function getSearchFilters() {
        $this->filters['sortBy'] = self::MACHINE_DEFAULT_SORT_BY;
        $this->filters['sortOrder'] = self::MACHINE_DEFAULT_SORT_ORDER;
        $this->filters['pageSize'] = self::MACHINE_DEFAULT_PAGE_SIZE;
        
        $this->addToFilters('sortBy', 'machineSortBy');
        $this->addToFilters('sortOrder', 'machineSortOrder');
        $this->addToFilters('pageSize', 'machinePageSize');
        
        $this->addToFilters('keywords', 'machineKeywords');
        $this->addToFilters('types', 'machineTypes');
        $this->addToFilters('status', 'machineStatus');
        $this->addToFilters('locations', 'machineLocations');
        
        $this->addToFilters('highlightedOnTop', 'machineHighlightedOnTop');
        
        return $this->filters;
        
    }
    
    private function addToFilters($inputKey, $sessionKey) {
        $inputValue = Input::get($inputKey);
        
        if($inputValue && $inputValue !== ''){
            $this->filters[$inputKey] = $inputValue;
            $this->request->session()->put($sessionKey, $inputValue);
        }elseif($this->request->session()->has($sessionKey)){
            $this->filters[$inputKey] =  $this->request->session()->get($sessionKey);
        }
    }
    
    private function resetSearch() {
        $this->request->session()->forget('machineSortBy');
        $this->request->session()->forget('machineSortOrder');
        $this->request->session()->forget('machinePageSize');
        $this->request->session()->forget('machineKeywords');
        $this->request->session()->forget('machineTypes');
        $this->request->session()->forget('machineStatus');
        $this->request->session()->forget('machineLocations');
        $this->request->session()->forget('machineHighlightedOnTop');
    }
    
    public function clearFilters() {
        $this->resetSearch();
        return Redirect::to('/machine')->with('wasReseted', true);
    }
    
}
