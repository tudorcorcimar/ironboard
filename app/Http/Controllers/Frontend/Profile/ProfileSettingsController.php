<?php

namespace App\Http\Controllers\Frontend\Profile;

use Illuminate\Support\Facades\View;
use App\Http\Controllers\Frontend\Profile\ProfileController;
use App\Models\Persistent\Users\UsersSettings;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

/**
 * Profile Settings Controller
 *
 * @author Corcimar Tudor
 * @version 03/05/2016
 * @package App\Http\Controllers\Frontend\Profile
 * @copyright (c) 2016, Iron Board
 */
class ProfileSettingsController extends ProfileController
{
    
    public function __construct() {
        parent::__construct();
    }
    
    /*
     * Show profile settings view
     * 
     * @return view
     */
    public function settings() {
        $this->viewBag['leftsidemenu'] = 'profile.settings';
        $this->viewBag['settings'] = UsersSettings::where('user_id', $this->viewBag['user']['id'])->get()->first();
        return View::make($this->viewName . 'settings', $this->viewBag);
    }
    
    public function editSettings() {
        $this->viewBag['form'] = Input::all();
        $settings = UsersSettings::where('user_id', $this->viewBag['user']['id'])->get()->first();
        
        $settings->account_visible = $this->viewBag['form']['account_visible'];
        $settings->hide_machine = $this->viewBag['form']['hide_machine'];
        $settings->newsletters = $this->viewBag['form']['newsletters'];
        
        $settings->save();
        return Redirect::to('/profile/settings')->with([
            'growlSuccess' => 'Settings successful changed!',
        ]);
    }
    
}