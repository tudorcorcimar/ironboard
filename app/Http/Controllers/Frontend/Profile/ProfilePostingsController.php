<?php

namespace App\Http\Controllers\Frontend\Profile;

use Illuminate\Support\Facades\View;
use App\Http\Controllers\Frontend\Profile\ProfileController;
use App\Models\Persistent\Machine\Machine;
use App\Services\ImageService;
use Illuminate\Support\Facades\Redirect;

/**
 * Profile Postings Controller
 *
 * @author Corcimar Tudor
 * @version 03/05/2016
 * @package App\Http\Controllers\Frontend\Profile
 * @copyright (c) 2016, Iron Board
 */
class ProfilePostingsController extends ProfileController
{
    /*
     * User machine
     * 
     * @type array
     */
    protected $userMachines = [];
    
    private function getUserMachine($userId){
        return Machine::where('user_id', $userId)->orderBy('updated_at', 'desc')->get();
    }
    
    public function __construct() {
        parent::__construct();
        $this->userMachines = $this->getUserMachine($this->viewBag['user']['id']);
    }
    
    /*
     * Show profile postings view
     * 
     * @return view
     */
    public function postings() {
        $this->viewBag['leftsidemenu'] = 'profile.postings';
        $this->viewBag['machines'] = $this->userMachines;
        $imageService = new ImageService;
        $this->viewBag['images'] = [];
        foreach ( $this->viewBag['machines'] as $machine ){
            $this->viewBag['images'][$machine->id] = $imageService->getImages($machine->id, $this->viewBag['user']['id']);
            $this->viewBag['thumbnail'][$machine->id] = $imageService->getThumbnail($machine->id, $this->viewBag['user']['id']);
        }
        return View::make($this->viewName . 'postings', $this->viewBag);
    }
    
    /*
     * Remove Machine
     * 
     * @return redirect || object view
     */
    public function removeMachine($machineId) {
        $machine = Machine::where('user_id', $this->viewBag['user']['id'])
                                       ->where('id', $machineId)
                                       ->get()->first();
        if( $machine !== null ){
            $imageService = new ImageService;
            $imageService->deleteFolderWithImages($machineId, $this->viewBag['user']['id']);
            
            $machine = Machine::find($machineId);
            $machine->delete();
        }
        return Redirect::to('/profile/postings')->with('growlSuccess', 'Machine was succesuful removed!');
    }
    
    /*
     * Activate Machine
     * 
     * @return redirect || object view
     */
    public function activate($machineId) {
        $machine = Machine::where('user_id', $this->viewBag['user']['id'])
                                       ->where('id', $machineId)
                                       ->get()->first();
        if( $machine !== null ){
            $machine->active = true;
            $machine->save();
            return Redirect::to('/profile/postings')->with('growlSuccess', 'Machine was succesuful activated!');
        }
        return Redirect::to('/profile/postings');
    }
    
    /*
     * Deactivate Machine
     * 
     * @return redirect || object view
     */
    public function deactivate($machineId) {
        $machine = Machine::where('user_id', $this->viewBag['user']['id'])
                                       ->where('id', $machineId)
                                       ->get()->first();
        if( $machine !== null ){
            $machine->active = false;
            $machine->save();
            return Redirect::to('/profile/postings')->with('growlSuccess', 'Machine was succesuful deactivated!');
        }
        return Redirect::to('/profile/postings');
    }
    
}