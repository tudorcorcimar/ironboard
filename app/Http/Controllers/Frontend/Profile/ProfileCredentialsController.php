<?php

namespace App\Http\Controllers\Frontend\Profile;

use Illuminate\Support\Facades\View;
use App\Http\Controllers\Frontend\Profile\ProfileController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Services\ValidatorService;
use App\Models\Persistent\Users\Users;

/**
 * Profile General Controller
 *
 * @author Corcimar Tudor
 * @version 03/05/2016
 * @package App\Http\Controllers\Frontend\Profile
 * @copyright (c) 2016, Iron Board
 */
class ProfileCredentialsController extends ProfileController
{
    
    public function __construct() {
        parent::__construct();
    }
    
    /*
     * Show profile credentials view
     * 
     * @return view
     */
    public function changeEmailPassword() {
        $this->viewBag['leftsidemenu'] = 'profile.credentials';
        return View::make($this->viewName . 'credentials', $this->viewBag);
    }
    
    /*
     * Change password
     * 
     * @return view/redirect
     */
    public function changePassword() {
        $this->viewBag['leftsidemenu'] = 'profile.credentials';
        $this->viewBag['form']['credentials_password'] = Input::all();
        $user = Users::where('id', $this->viewBag['user']['id'])->first();
        if( $user ){
            $rules = [
                'password'     => 'required|exists:users,password,id,' . $user->id,
                'new_password' => 'required|min:6',
                'captcha'      => 'required|captcha'
            ];
            $validator = ValidatorService::validate($rules);
            if($validator->fails()){
                $this->viewBag['form']['credentials_password']['errors'] = $validator->errors();
            }else{
                $user->password = $this->viewBag['form']['credentials_password']['new_password'];
                $user->save();
                return Redirect::to('/profile/credentials')->with('growlSuccess', 'Password successuful updated!');
            }
        }
        return View::make($this->viewName . 'credentials', $this->viewBag);
    }
    
    /*
     * Change email
     * 
     * @return view/redirect
     */
    public function changeEmail() {
        $this->viewBag['leftsidemenu'] = 'profile.credentials';
        $this->viewBag['form']['credentials_email'] = Input::all();
        $user = Users::where('id', $this->viewBag['user']['id'])->first();
        if( $user ){
            $rules = [
                'password'     => 'required|exists:users,password,id,' . $user->id,
                'email'        => 'required|email|min:6',
                'captcha'      => 'required|captcha'
            ];
            $validator = ValidatorService::validate($rules);
            if($validator->fails() || !$this->isValidEmail($this->viewBag['form']['email'])){
                $this->viewBag['form']['credentials_email']['errors'] = $validator->errors();
            }else{
                $user->email = $this->viewBag['form']['credentials_email']['email'];
                $user->save();
                return Redirect::to('/profile/credentials')->with('growlSuccess', 'Email successuful updated!');
            }
        }
        return View::make($this->viewName . 'credentials', $this->viewBag);
    }
    
    /*
     * Check if email is valid
     * 
     * @return bool
     */
    private function isValidEmail($email) {
        $usersCount = Users::where('email', $email)->count();
        return $usersCount > 0 || ($email == $this->viewBag['user']['email']);
    }
    
}