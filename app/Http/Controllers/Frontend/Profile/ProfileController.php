<?php

namespace App\Http\Controllers\Frontend\Profile;

use App\Http\Controllers\Frontend\BaseController;
use App\Models\Persistent\Users\UserType;

/**
 * Profile Controller
 *
 * @author Corcimar Tudor
 * @version 15/04/2016
 * @package App\Http\Controllers\Frontend\Profile
 * @copyright (c) 2016, Iron Board
 */
class ProfileController extends BaseController
{
    
    /*
     * @type string 
     */
    protected $viewName;
    
    public function __construct() {
        parent::__construct();
        $this->viewName = 'frontend.profile.company.';
        $this->viewBag['module'] = 'profile';
        $this->viewBag['form'] = [];
    }
    
}