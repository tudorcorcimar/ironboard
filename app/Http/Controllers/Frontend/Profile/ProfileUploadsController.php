<?php

namespace App\Http\Controllers\Frontend\Profile;

use Illuminate\Support\Facades\View;
use App\Http\Controllers\Frontend\Profile\ProfileController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Services\ValidatorService;
use App\Services\ImageService;

/**
 * Profile Uploads Controller
 *
 * @author Corcimar Tudor
 * @version 24/05/2016
 * @package App\Http\Controllers\Frontend\Profile
 * @copyright (c) 2016, Iron Board
 */
class ProfileUploadsController extends ProfileController
{
    
    public function __construct() {
        parent::__construct();
    }
    
    /*
     * Show profile credentials view
     * 
     * @return view
     */
    public function uploads() {
        $imageService = new ImageService;
        $images = $imageService->getImages(false, $this->viewBag['user']['id'], true);
        if(count($images) > 0){
            $this->viewBag['form']['logo'] = $imageService->getLogoImageInBase64($this->viewBag['user']['id'], $images);
        }
        $this->viewBag['leftsidemenu'] = 'profile.uploads';
        return View::make($this->viewName . 'uploads', $this->viewBag);
    }
    
    public function uploadLogo() {
        $this->viewBag['leftsidemenu'] = 'profile.uploads';
        $this->viewBag['form'] = Input::all();
        $rules = [
            'captcha' => 'required|captcha',
            'logo' => 'required'
        ];
        $validator = ValidatorService::validate($rules);
        if($validator->fails()){
            $this->viewBag['form']['errors'] = $validator->errors();
            return View::make($this->viewName . 'uploads', $this->viewBag);
        }
        $isLogo = true;
        $imageService = new ImageService;
        $imageService->saveImages($this->viewBag['form']['logo'], 'logo', $this->viewBag['user']['id'], $isLogo);
        return Redirect::to('/profile/uploads')->with('growlSuccess', 'Logo was succesuful uploaded!');
    }
    
}