<?php

namespace App\Http\Controllers\Frontend\Profile;

use Illuminate\Support\Facades\View;
use App\Http\Controllers\Frontend\Profile\ProfileController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Services\ValidatorService;
use App\Models\Persistent\Users\Users;
use App\Models\Persistent\Users\UsersLocations;

/**
 * Profile General Controller
 *
 * @author Corcimar Tudor
 * @version 03/05/2016
 * @package App\Http\Controllers\Frontend\Profile
 * @copyright (c) 2016, Iron Board
 */
class ProfileGeneralController extends ProfileController
{
    
    public function __construct() {
        parent::__construct();
    }
    
    /*
     * Show profile general view
     * 
     * @return view
     */
    public function general() {
        $this->viewBag['leftsidemenu'] = 'profile.general';
        return View::make($this->viewName . 'general', $this->viewBag);
    }
    
    /*
     * Update general details
     * 
     * @return void
     */
    public function updateDetails() {
        $this->viewBag['form'] = Input::all();
        
        $companyNameRules = "required|max:150";
        
        if(trim($this->viewBag['form']['company_name']) !== trim($this->viewBag['user']['company_name'])){
            $companyNameRules .= "|unique:users";
        }
        
        $rules = [
            'company_name' => $companyNameRules,
            'name'         => "max:70",
            'public_email' => "email|max:70",
            'website'      => "url|max:70",
            'phone'        => "max:50",
            'captcha'      => 'required|captcha'
        ];
        
        $validator = ValidatorService::validate($rules);
        if($validator->fails()){
            $this->viewBag['form']['errors'] = $validator->errors();
            return View::make($this->viewName . 'general', $this->viewBag);
        }else{
            $user = Users::find($this->viewBag['user']['id']);
            if(isset($this->viewBag['form']['company_name'])){
                $user->company_name = trim($this->viewBag['form']['company_name']);
            }
            if(isset($this->viewBag['form']['name'])){
                $user->name = $this->viewBag['form']['name'];
            }
            if(isset($this->viewBag['form']['public_email'])){
                $user->public_email = $this->viewBag['form']['public_email'];
            }
            if(isset($this->viewBag['form']['website'])){
                $user->website = $this->viewBag['form']['website'];
            }
            if(isset($this->viewBag['form']['phone'])){
                $user->phone = $this->viewBag['form']['phone'];
            }
            if(isset($this->viewBag['form']['address'])){
                $user->address = $this->viewBag['form']['address'];
            }
            if(isset($this->viewBag['form']['description'])){
                $user->description = $this->viewBag['form']['description'];
            }
            $user->save();
            
            $userLocations = UsersLocations::where('users_id', $this->viewBag['user']['id']);
            $userLocations->delete();
                
            if(isset($this->viewBag['form']['location']) && count($this->viewBag['form']['location']) > 0){
                foreach ($this->viewBag['form']['location'] as $location){
                    $userLocations = new UsersLocations;
                    $userLocations->users_id = $this->viewBag['user']['id'];
                    $userLocations->location_id = $location;
                    $userLocations->save();
                }
            }
            
            return Redirect::to('/profile')->with('growlSuccess', 'Details successuful updated!');
        }
        
    }
    
}