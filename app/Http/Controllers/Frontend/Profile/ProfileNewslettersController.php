<?php

namespace App\Http\Controllers\Frontend\Profile;

use Illuminate\Support\Facades\View;
use App\Http\Controllers\Frontend\Profile\ProfileController;

/**
 * Profile Newsletters Controller
 *
 * @author Corcimar Tudor
 * @version 03/05/2016
 * @package App\Http\Controllers\Frontend\Profile
 * @copyright (c) 2016, Iron Board
 */
class ProfileNewslettersController extends ProfileController
{
    
    public function __construct() {
        parent::__construct();
    }
    
    /*
     * Show profile postings view
     * 
     * @return view
     */
    public function newsletters() {
        $this->viewBag['leftsidemenu'] = 'profile.newsletters';
        return View::make($this->viewName . 'newsletters', $this->viewBag);
    }
    
}