<?php

namespace App\Http\Controllers\Frontend\Profile;

use Illuminate\Support\Facades\View;
use App\Http\Controllers\Frontend\Profile\ProfileController;

/**
 * Profile Resources Controller
 *
 * @author Corcimar Tudor
 * @version 03/05/2016
 * @package App\Http\Controllers\Frontend\Profile
 * @copyright (c) 2016, Iron Board
 */
class ProfileResourcesController extends ProfileController
{
    
    public function __construct() {
        parent::__construct();
    }
    
    /*
     * Show profile postings view
     * 
     * @return view
     */
    public function resources() {
        $this->viewBag['leftsidemenu'] = 'profile.resources';
        return View::make($this->viewName . 'resources', $this->viewBag);
    }
    
}