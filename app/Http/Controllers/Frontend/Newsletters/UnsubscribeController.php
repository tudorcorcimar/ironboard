<?php

namespace App\Http\Controllers\Frontend\Newsletters;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use App\Models\Persistent\Hash\HashList;
use Illuminate\Support\Facades\Crypt;
use App\Models\Persistent\Users\Users;

class UnsubscribeController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $this->viewBag['module'] = 'Newsletters';
    }
    
    public function unsubscribeUser() {
        
        $hash = Crypt::decrypt(Input::get('unsubscribe'));
        
        $email = str_replace(HashList::UNSUBSCRIBE_HASH,"",$hash);
        
        $user = Users::where('email', '=', $email)->get()->first();
        
        if($email !== '' && $user){
            $user->subscribed = false;
            $user->save();
        }
        
        return View::make("frontend.newsletters.unsubscribe", $this->viewBag);
    }
        
}
