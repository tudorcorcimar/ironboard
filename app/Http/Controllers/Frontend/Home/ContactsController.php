<?php

namespace App\Http\Controllers\Frontend\Home;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Services\ValidatorService;
use Illuminate\Support\Facades\Mail;

class ContactsController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $this->viewBag['module'] = 'home';
    }
    
    public function showContacts() {
        return View::make("frontend.home.contacts", $this->viewBag);
    }
    
    public function sendContacts() {
        $this->viewBag['form'] = Input::all();
        
        $data['form'] = $this->viewBag['form'];
        $data['app'] = $this->viewBag['app'];
        $data['mailTo'] = '';
        
        $rules =[
            'name'         => 'required|min:2',
            'email'        => 'required|email|max:150',
            'subject'      => 'required|min:5',
            'message'      => 'required|min:5',
            'captcha'      => 'required|captcha'
        ];
        $validator = ValidatorService::validate($rules);
        if($validator->fails()){
            $this->viewBag['form']['errors'] = $validator->errors();
            return View::make("frontend.home.contacts", $this->viewBag);
        }else{
            $mails = ['info.shioric@gmail.com', 'axonicdesign@gmail.com'];
            foreach($mails as $mail){
                $data['mailTo'] = $mail;
                $this->sendContactMail($data);
            }
            return Redirect::to('/contacts')->with([
                'growlSuccess' => 'Your message successfully sent!'
            ]);
        }
    }
    
    private function sendContactMail($data) {
        Mail::send('frontend.email.general.contacts', $data, function ($m) use ($data) {
            $m->from($data['form']['email'], $data['form']['name']);
            $m->to($data['mailTo'], $data['app']['name'] )
              ->subject( $data['form']['name'].' contacts form' );
        });
    }
    
}
