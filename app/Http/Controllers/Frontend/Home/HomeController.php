<?php

namespace App\Http\Controllers\Frontend\Home;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\View;
use App\Models\Persistent\Machine\Machine;
use App\Services\MachineManager;

class HomeController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
        $this->viewBag['module'] = 'home';
    }
    
    public function showHome() {
        $this->viewBag['machines'] = [];
        $this->viewBag['machines']['items'] = MachineManager::machineSerialization($this->getLast20Machines());
        return View::make("frontend.home.home", $this->viewBag);
    }
    
    public function showAboutUs() {
        return View::make("frontend.home.aboutus", $this->viewBag);
    }
    
    public function showServices() {
        return View::make("frontend.home.servicesView", $this->viewBag);
    }
    
    public function showTerms() {
        return View::make("frontend.home.termsandconditions", $this->viewBag);
    }
    
    public function showPrivacy() {
        return View::make("frontend.home.privacypolicy", $this->viewBag);
    }
    
    public function showAddsAdvertising() {
        return View::make("frontend.home.addsAdvertising", $this->viewBag);
    }
    
    public function showBannerAdvertising() {
        return View::make("frontend.home.bannerAdvertising", $this->viewBag);
    }
    
    public function showSocial() {
        return View::make("frontend.home.socialPromotion", $this->viewBag);
    }
    
    public function getLast20Machines() {
        $machine = Machine::where('active', 1)
                          ->orderByRaw("RAND()")
                          ->take(8)->get();
        return $machine;
    }
    
}
