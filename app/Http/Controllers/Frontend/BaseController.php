<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Runtime\Auth\Auth;
use App\Models\Runtime\Locations\LocationsList;
use App\Models\Runtime\Machine\MachineTypesList;
use App\Models\Persistent\Machine\MachineStatuses;
use App\Models\Runtime\App\AppManager;
use App\Models\Runtime\Cms\CmsManager;
use Illuminate\Support\Facades\Redirect;
use App\Models\Persistent\App\AppLanguages;
use App\Services\StatisticsService;
use App\Models\Persistent\Saves\SavedMachines;
use App\Models\Persistent\Machine\Machine;
use App\Models\Persistent\Saves\SavedSearch;

class BaseController extends Controller
{
    
    const MACHINE_DEFAULT_SORT_BY = 'updated_at';
    const MACHINE_DEFAULT_SORT_ORDER = 'desc';
    const MACHINE_DEFAULT_PAGE_SIZE = 20;
    
    const COMPANY_DEFAULT_SORT_BY = 'updated_at';
    const COMPANY_DEFAULT_SORT_ORDER = 'desc';
    const COMPANY_DEFAULT_PAGE_SIZE = 20;
    
    private $cmsManager;

    /*
     * App manager entity
     * 
     * AppManager
     */
    private $appManager;
    
    /*
     * Array with all view data
     * 
     * @type array
     */
    public $viewBag = array();

    public function __construct() {
        $this->collectBaseViewBag();
    }
    
    private function getViewsData() {
        $this->cmsManager = new CmsManager;
        if(!$this->cmsManager->getLanguage()){
            $this->cmsManager->setLanguage(AppLanguages::ENGLISH);
        }
        return $this->cmsManager->getViewsData();
    }
    
    /*
     * Add locations to view bag
     */
    private function addToViewBagApplicationData(){
        $this->appManager = new AppManager();
        $this->viewBag['app'] = [];
        $this->viewBag['app'] = $this->appManager->getAppInfo();
        $this->viewBag['views'] = $this->getViewsData();
        $this->viewBag['resources_path'] = public_path();
        
    }
    
    /*
     * Add locations to view bag
     */
    private function addToViewBagDictionaries(){
        $this->viewBag['locations']['countries'] = LocationsList::getCountries();
        $this->viewBag['locations']['regions'] = LocationsList::getRegions();
        $this->viewBag['machine']['types'] = MachineTypesList::getAll();
        $this->viewBag['machine']['statuses'] = MachineStatuses::getAll();
    }
    
    /*
     * Prepare authentication and add to view bag
     */
    private function addToViewBagAuthentication(){
        if(Auth::getInstance()->getIsAuth()){
            $this->viewBag['isAuth'] = Auth::getInstance()->getIsAuth();
            $this->viewBag['user'] = Auth::getInstance()->getUser();
        }else{
            $this->viewBag['user'] = null;
        }
    }
    
    private function addToViewBagLocalization(){
        $this->viewBag['language'] = $this->cmsManager->getLanguage();
    }

    public function collectBaseViewBag() {
        $this->addToViewBagApplicationData();
        $this->addToViewBagAuthentication();
        $this->addToViewBagDictionaries();
        $this->addToViewBagLocalization();
        $this->addToViewBagStatistics();
        $this->addToViewBagUserSaves();
    }
    
    public function changeLanguage($language) {
        if($language){
            $this->cmsManager->setLanguage($language);
        }
        return Redirect::to('/home');
    }
    
    public function addToViewBagStatistics() {
        $this->viewBag['statistics'] = StatisticsService::get();
    }
    
    public function addToViewBagUserSaves() {
        if(isset($this->viewBag['user']->id)){
            $savedMachines = SavedMachines::where('users_id', '=', $this->viewBag['user']->id)->get();
            $machineIds = [];
            foreach($savedMachines as $mac){
                $machineIds[] = $mac->machine_id;
            }
            $machines = Machine::whereIn('id', $machineIds)->get();
            
            $savedSearch = SavedSearch::where('users_id', '=', $this->viewBag['user']->id)->get();
            $filters = [];
            foreach ($savedSearch as $search){
                $filters[] = [
                    'name' => $search->name,
                    'filters' => json_decode($search->json_filters, true)
                ];
            }
            
            $this->viewBag['saves'] = [
                'machines' => $machines,
                'search' => $filters
            ];
        }
    }
        
}
