<?php

namespace App\Http\Controllers\Frontend\Forbidden;

use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Support\Facades\View;

class ForbiddenController extends BaseController
{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function pageNotFound(){
        return View::make('frontend.forbidden.pageNotFound', $this->viewBag);
    }
    
}