<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Runtime\Auth\Auth;
use Illuminate\Support\Facades\Redirect;

class AuthenticateUser
{

    /**
     * Handle an incoming request.
     * Check if user is authenticated, redirect to home page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @throws AdminNotAuthenticatedException
     */
    public function handle($request, Closure $next, $restriction = null)
    {
        $isAuth = Auth::getInstance()->getIsAuth();
        
        if( $isAuth && $restriction ){
            return Redirect::to('/');
        }elseif(!$isAuth && !$restriction){
            return Redirect::to('/');
        }

        return $next($request);
    }
}
