<?php

use App\Services\Captcha;
use App\Http\Controllers\Frontend\Auth\RegisterController;
use App\Http\Controllers\Frontend\Auth\AuthController;
use App\Http\Controllers\Frontend\Auth\RecoverPasswordController;
use App\Http\Controllers\Frontend\Home\HomeController;
use App\Http\Controllers\Frontend\Companies\CompaniesSearchController;
use App\Http\Controllers\Frontend\Companies\CompaniesViewController;
use App\Http\Controllers\Frontend\Partners\PartnersController;
use App\Http\Controllers\Frontend\Products\Machine\PostMachineController;
use App\Http\Controllers\Frontend\Products\Machine\SearchMachineController;
use App\Http\Controllers\Frontend\Saves\MachineSavesController;
use App\Http\Controllers\Frontend\Saves\SearchSavesController;
use App\Http\Controllers\Frontend\Profile\ProfileGeneralController;
use App\Http\Controllers\Frontend\Profile\ProfilePostingsController;
use App\Http\Controllers\Frontend\Profile\ProfileCredentialsController;
use App\Http\Controllers\Frontend\Forbidden\ForbiddenController;
use App\Http\Controllers\Frontend\Products\Machine\ViewMachineController;
use App\Http\Controllers\Frontend\Products\Machine\EditMachineController;
use App\Http\Controllers\Frontend\Profile\ProfileUploadsController;
use App\Http\Controllers\Frontend\Home\ContactsController;
use App\Http\Controllers\Frontend\BaseController;
use App\Http\Controllers\Frontend\Newsletters\UnsubscribeController;

// Admin Routes
use App\Http\Controllers\Admin\Users\AdminUsersController;
use App\Http\Controllers\Admin\Users\AdminUsersAddController;
use App\Http\Controllers\Admin\Users\AdminUsersEditController;

use App\Http\Controllers\Admin\Users\Ranks\AdminUsersRanksController;
use App\Http\Controllers\Admin\Users\Ranks\AdminUsersRanksCreateController;
use App\Http\Controllers\Admin\Users\Ranks\AdminUsersRanksEditController;

use App\Http\Controllers\Admin\Auth\AdminAuthController;
use App\Http\Controllers\Admin\Home\AdminHomeController;
use App\Http\Controllers\Admin\Companies\AdminCompaniesController;
use App\Http\Controllers\Admin\Companies\AdminCompanyEditController;
use App\Http\Controllers\Admin\Companies\AdminCompanyAddController;
use App\Http\Controllers\Admin\Machines\AdminMachinesController;
use App\Http\Controllers\Admin\Machines\AdminMachinesAddController;
use App\Http\Controllers\Admin\Machines\AdminMachinesEditController;

use App\Http\Controllers\Admin\Spells\SpellsHomeController;
use App\Http\Controllers\Admin\Spells\SpellAddController;
use App\Http\Controllers\Admin\Spells\SpellEditController;
use App\Http\Controllers\Admin\Spells\SpellsHeaderController;
use App\Http\Controllers\Admin\Spells\SpellsFooterController;
use App\Http\Controllers\Admin\Spells\SpellsMachineController;
use App\Http\Controllers\Admin\Spells\SpellsPostMachineController;
use App\Http\Controllers\Admin\Spells\SpellsCompaniesController;
use App\Http\Controllers\Admin\Spells\SpellsProfileMenuController;
use App\Http\Controllers\Admin\Spells\SpellsRegisterController;
use App\Http\Controllers\Admin\Spells\SpellsAuthenticationController;
use App\Http\Controllers\Admin\Spells\SpellsRecoverPasswordController;
use App\Http\Controllers\Admin\Spells\SpellsProfileGeneralController;
use App\Http\Controllers\Admin\Spells\SpellsProfileMachineController;
use App\Http\Controllers\Admin\Spells\SpellsProfileUploadsController;
use App\Http\Controllers\Admin\Spells\SpellsProfileCredentialsController;
use App\Http\Controllers\Admin\Spells\SpellsAboutUsController;
use App\Http\Controllers\Admin\Spells\SpellsServicesController;
use App\Http\Controllers\Admin\Spells\SpellsViewMachineController;
use App\Http\Controllers\Admin\Spells\SpellsViewCompaniesController;
use App\Http\Controllers\Admin\Spells\SpellsContactsController;
use App\Http\Controllers\Admin\Spells\SpellsAlertsController;
use App\Http\Controllers\Admin\Spells\SpellApplicationController;

use App\Http\Controllers\Admin\Fix\FixController;

use App\Http\Controllers\Admin\Mailer\MailerSendController;

use App\Http\Controllers\Admin\Mailer\MailerBroadcastController;



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

    /*
     * ADMIN routes
     */
    Route::get('/admin', [ 'uses' => AdminAuthController::class . '@showSignIn']);
    Route::get('/admin/auth', [ 'uses' => AdminAuthController::class . '@showSignIn']);
    Route::post('/admin/auth/signin', [ 'uses' => AdminAuthController::class . '@signIn']);
    Route::get('/admin/auth/signout', [ 'uses' => AdminAuthController::class . '@signOut']);

    Route::get('/admin/home', ['middleware' => 'adminAuth', 'uses' => AdminHomeController::class . '@showHome']);

    Route::get('/admin/companies', ['middleware' => 'adminAuth', 'uses' => AdminCompaniesController::class . '@showCompanies']);
    Route::get('/admin/companies/search', ['middleware' => 'adminAuth', 'uses' => AdminCompaniesController::class . '@filterSearch']);
    Route::get('/admin/companies/search/reset', ['middleware' => 'adminAuth', 'uses' => AdminCompaniesController::class . '@clearFilters']);
    Route::get('/admin/company/add', ['middleware' => 'adminAuth', 'uses' => AdminCompanyAddController::class . '@showAddNewCompany']);
    Route::post('/admin/company/add', ['middleware' => 'adminAuth', 'uses' => AdminCompanyAddController::class . '@addCompany']);
    Route::get('/admin/company/edit/{id}', ['middleware' => 'adminAuth', 'uses' => AdminCompanyEditController::class . '@showEditCompany']);
    Route::post('/admin/company/edit', ['middleware' => 'adminAuth', 'uses' => AdminCompanyEditController::class . '@editCompany']);
    Route::get('/admin/company/remove/{id}', ['middleware' => 'adminAuth', 'uses' => AdminCompanyEditController::class . '@removeCompany']);

    Route::get('/admin/company/deactivate/{id}', ['middleware' => 'adminAuth', 'uses' => AdminCompanyEditController::class . '@deactivateCompany']);
    Route::get('/admin/company/activate/{id}', ['middleware' => 'adminAuth', 'uses' => AdminCompanyEditController::class . '@activateCompany']);

    Route::get('/admin/company/subscribe/{id}', ['middleware' => 'adminAuth', 'uses' => AdminCompanyEditController::class . '@subscribeCompany']);
    Route::get('/admin/company/unsubscribe/{id}', ['middleware' => 'adminAuth', 'uses' => AdminCompanyEditController::class . '@unsubscribeCompany']);

    Route::get('/admin/company/highlight/activate/{companyId}', ['middleware' => 'adminAuth', 'uses' => AdminCompanyEditController::class . '@activateHighlightCompany']);
    Route::get('/admin/company/highlight/deactivate/{companyId}', ['middleware' => 'adminAuth', 'uses' => AdminCompanyEditController::class . '@deactivateHighlightCompany']);

    Route::post('/admin/company/comment/add', ['middleware' => 'adminAuth', 'uses' => AdminCompanyEditController::class . '@addCompanyAdminComment']);
    Route::get('/admin/company/comment/remove/{commentId}', ['middleware' => 'adminAuth', 'uses' => AdminCompanyEditController::class . '@removeCompanyAdminComment']);

    Route::get('/admin/machines', ['middleware' => 'adminAuth', 'uses' => AdminMachinesController::class . '@search']);
    Route::get('/admin/machines/search', ['middleware' => 'adminAuth', 'uses' => AdminMachinesController::class . '@filterSearch']);
    Route::get('/admin/machine/search/reset', ['middleware' => 'adminAuth', 'uses' => AdminMachinesController::class . '@clearFilters']);

    Route::get('/admin/machine/add/{id}', ['middleware' => 'adminAuth', 'uses' => AdminMachinesAddController::class . '@showAddMachine']);
    Route::post('/admin/machine/add', ['middleware' => 'adminAuth', 'uses' => AdminMachinesAddController::class . '@addMachine']);
    Route::get('/admin/machine/edit/{useId}/{id}', ['middleware' => 'adminAuth', 'uses' => AdminMachinesEditController::class . '@showEditMachine']);
    Route::post('/admin/machine/edit', ['middleware' => 'adminAuth', 'uses' => AdminMachinesEditController::class . '@editMachine']);

    Route::get('/admin/machine/activate/{userId}/{machineId}', ['middleware' => 'adminAuth', 'uses' => AdminMachinesEditController::class . '@activateMachine']);
    Route::get('/admin/machine/deactivate/{userId}/{machineId}', ['middleware' => 'adminAuth', 'uses' => AdminMachinesEditController::class . '@deactivateMachine']);
    Route::get('/admin/machine/remove/{userId}/{machineId}', ['middleware' => 'adminAuth', 'uses' => AdminMachinesEditController::class . '@removeMachineWithRedirect']);

    Route::get('/admin/machine/highlight/activate/{machineId}', ['middleware' => 'adminAuth', 'uses' => AdminMachinesEditController::class . '@activateHighlightMachine']);
    Route::get('/admin/machine/highlight/deactivate/{machineId}', ['middleware' => 'adminAuth', 'uses' => AdminMachinesEditController::class . '@deactivateHighlightMachine']);

    Route::post('/admin/spells/edit', ['middleware' => 'adminAuth', 'uses' => SpellEditController::class . '@updateSpell']);
    
    Route::get('/admin/spells/add', ['middleware' => 'adminAuth', 'uses' => SpellAddController::class . '@showAddSpellForm']);
    Route::post('/admin/spells/add', ['middleware' => 'adminAuth', 'uses' => SpellAddController::class . '@addSpell']);

    Route::get('/admin/spells/home', ['middleware' => 'adminAuth', 'uses' => SpellsHomeController::class . '@showHomeSpells']);
    Route::get('/admin/spells/aboutus', ['middleware' => 'adminAuth', 'uses' => SpellsAboutUsController::class . '@showAboutUsSpells']);
    Route::get('/admin/spells/services', ['middleware' => 'adminAuth', 'uses' => SpellsServicesController::class . '@showServicesSpells']);
    Route::get('/admin/spells/contacts', ['middleware' => 'adminAuth', 'uses' => SpellsContactsController::class . '@showContactsSpells']);
    Route::get('/admin/spells/alerts', ['middleware' => 'adminAuth', 'uses' => SpellsAlertsController::class . '@showAlertsSpells']);

    Route::get('/admin/spells/header', ['middleware' => 'adminAuth', 'uses' => SpellsHeaderController::class . '@showHeaderSpells']);
    Route::get('/admin/spells/footer', ['middleware' => 'adminAuth', 'uses' => SpellsFooterController::class . '@showFooterSpells']);

    Route::get('/admin/spells/machine', ['middleware' => 'adminAuth', 'uses' => SpellsMachineController::class . '@showMachineSpells']);
    Route::get('/admin/spells/machine/view', ['middleware' => 'adminAuth', 'uses' => SpellsViewMachineController::class . '@showViewMachineSpells']);
    Route::get('/admin/spells/machine/post', ['middleware' => 'adminAuth', 'uses' => SpellsPostMachineController::class . '@showPostMachineSpells']);

    Route::get('/admin/spells/companies', ['middleware' => 'adminAuth', 'uses' => SpellsCompaniesController::class . '@showCompaniesSpells']);
    Route::get('/admin/spells/companies/view', ['middleware' => 'adminAuth', 'uses' => SpellsViewCompaniesController::class . '@showViewCompaniesSpells']);

    Route::get('/admin/spells/register', ['middleware' => 'adminAuth', 'uses' => SpellsRegisterController::class . '@showRegisterSpells']);
    Route::get('/admin/spells/authentication', ['middleware' => 'adminAuth', 'uses' => SpellsAuthenticationController::class . '@showAuthenticationSpells']);
    Route::get('/admin/spells/recover', ['middleware' => 'adminAuth', 'uses' => SpellsRecoverPasswordController::class . '@showRecoverSpells']);
    Route::get('/admin/spells/profile/menu', ['middleware' => 'adminAuth', 'uses' => SpellsProfileMenuController::class . '@showProfileMenuSpells']);
    Route::get('/admin/spells/profile/general', ['middleware' => 'adminAuth', 'uses' => SpellsProfileGeneralController::class . '@showProfileGeneralSpells']);
    Route::get('/admin/spells/profile/machine', ['middleware' => 'adminAuth', 'uses' => SpellsProfileMachineController::class . '@showProfileMachineSpells']);
    Route::get('/admin/spells/profile/uploads', ['middleware' => 'adminAuth', 'uses' => SpellsProfileUploadsController::class . '@showProfileUploadsSpells']);
    Route::get('/admin/spells/profile/credentials', ['middleware' => 'adminAuth', 'uses' => SpellsProfileCredentialsController::class . '@showProfileCredentialsSpells']);
    
    Route::get('/admin/spells/application', ['middleware' => 'adminAuth', 'uses' => SpellApplicationController::class . '@showApplicationSpellForm']);
    Route::post('/admin/spells/application', ['middleware' => 'adminAuth', 'uses' => SpellApplicationController::class . '@updateApplicationInfo']);






    Route::get('/admin/users', ['middleware' => 'adminAuth', 'uses' => AdminUsersController::class . '@showAdminUsers']);
    Route::get('/admin/users/add', ['middleware' => 'adminAuth', 'uses' => AdminUsersAddController::class . '@showAdminUsersAdd']);
    Route::post('/admin/users/add', ['middleware' => 'adminAuth', 'uses' => AdminUsersAddController::class . '@addAdminUser']);
    Route::get('/admin/users/edit/{id}', ['middleware' => 'adminAuth', 'uses' => AdminUsersEditController::class . '@showAdminUsersEdit']);
    Route::post('/admin/users/edit', ['middleware' => 'adminAuth', 'uses' => AdminUsersEditController::class . '@editAdminUser']);

    Route::get('/admin/users/ranks', ['middleware' => 'adminAuth', 'uses' => AdminUsersRanksController::class . '@showUsersRanks']);
    Route::get('/admin/users/rank/add', ['middleware' => 'adminAuth', 'uses' => AdminUsersRanksCreateController::class . '@showUsersRankCreate']);
    Route::post('/admin/users/rank/add', ['middleware' => 'adminAuth', 'uses' => AdminUsersRanksCreateController::class . '@createUsersRank']);
    Route::get('/admin/users/rank/edit/{id}', ['middleware' => 'adminAuth', 'uses' => AdminUsersRanksEditController::class . '@showUsersRankUpdate']);
    Route::post('/admin/users/rank/edit', ['middleware' => 'adminAuth', 'uses' => AdminUsersRanksEditController::class . '@updateUsersRank']);



    Route::get('/admin/mailer/send', ['middleware' => 'adminAuth', 'uses' => MailerSendController::class . '@showMailForm']);
    Route::post('/admin/mailer/send', ['middleware' => 'adminAuth', 'uses' => MailerSendController::class . '@prepareForm']);
    
    Route::get('/admin/mailer/broadcast', ['middleware' => 'adminAuth', 'uses' => MailerBroadcastController::class . '@showBroadcasts']);
    Route::post('/admin/mailer/broadcast/create', ['middleware' => 'adminAuth', 'uses' => MailerBroadcastController::class . '@createBroadcast']);
    

    Route::get('/admin/fixbugs', ['middleware' => 'adminAuth', 'uses' => FixController::class . '@executeFix']);


    /*
     * --------------------------------------------------------------------------------------------------
     */

    /*
     * Home routes
     */
    Route::get('/', [ 'uses' => HomeController::class . '@showHome']);
    Route::get('/home', [ 'uses' => HomeController::class . '@showHome']);
    Route::get('/aboutus', [ 'uses' => HomeController::class . '@showAboutUs']);
    Route::get('/services', [ 'uses' => HomeController::class . '@showServices']);
    Route::get('/termsandconditions', [ 'uses' => HomeController::class . '@showTerms']);
    Route::get('/privacypolicy', [ 'uses' => HomeController::class . '@showPrivacy']);
    Route::get('/addsAdvertising', [ 'uses' => HomeController::class . '@showAddsAdvertising']);
    Route::get('/bannerAdvertising', [ 'uses' => HomeController::class . '@showBannerAdvertising']);
    Route::get('/socialPromotion', [ 'uses' => HomeController::class . '@showSocial']);
    Route::get('/contacts', [ 'uses' => ContactsController::class . '@showContacts']);
    Route::post('/contacts', [ 'uses' => ContactsController::class . '@sendContacts']);

    /*
     * Macchine routes
     */
    Route::get('/machine', [ 'uses' => SearchMachineController::class . '@search']);
    Route::get('/machine/search', [ 'uses' => SearchMachineController::class . '@filterSearch']);
    Route::get('/machine/search/reset', [ 'uses' => SearchMachineController::class . '@clearFilters']);
    Route::get('/machine/post', [ 'middleware' => 'auth', 'uses' => PostMachineController::class . '@showPostMachine']);
    Route::post('/machine/post', [ 'middleware' => 'auth', 'uses' => PostMachineController::class . '@postMachine']);
    Route::get('/machine/view/{id}', [ 'uses' => ViewMachineController::class . '@viewMachine']);
    Route::get('/machine/edit/{id}', [ 'uses' => EditMachineController::class . '@showEditMachine']);
    Route::post('/machine/edit', [ 'uses' => EditMachineController::class . '@editMachine']);

    Route::post('/machine/saveMachine', [ 'middleware' => 'auth', 'uses' => MachineSavesController::class . '@saveMachine']);
    Route::post('/machine/unsaveMachine', [ 'middleware' => 'auth', 'uses' => MachineSavesController::class . '@unsaveMachine']);
    Route::post('/machine/saveSearch', [ 'middleware' => 'auth', 'uses' => SearchSavesController::class . '@saveSearch']);

    /*
     * Companies routes
     */
    Route::get('/companies', [ 'uses' => CompaniesSearchController::class . '@search']);
    Route::get('/companies/search', [ 'uses' => CompaniesSearchController::class . '@filterSearch']);
    Route::get('/companies/search/reset', [ 'uses' => CompaniesSearchController::class . '@clearFilters']);
    Route::get('/company/view/{id}', [ 'uses' => CompaniesViewController::class . '@showCompaniesView']);

    /*
     * Partners routes
     */
    Route::get('/partners', [ 'uses' => PartnersController::class . '@showPartners']);

    /*
     * Newsletters routes
     */
    Route::get('/unsubscribe', [ 'uses' => UnsubscribeController::class . '@unsubscribeUser']);

    /*
     * Authentication routes
     */
    Route::get('auth/register', ['middleware' => 'auth:ifIsNotAuthenticated', 'uses' => RegisterController::class . '@showRegister']);
    Route::post('auth/register', ['middleware' => 'auth:ifIsNotAuthenticated', 'uses' => RegisterController::class . '@register']);
    Route::get('auth/verification', ['middleware' => 'auth:ifIsNotAuthenticated', 'uses' => RegisterController::class . '@verifyRegistrationKey']);
    Route::get('auth/signin', ['middleware' => 'auth:ifIsNotAuthenticated',  'uses' => AuthController::class . '@showSignIn']);
    Route::post('auth/signin', ['middleware' => 'auth:ifIsNotAuthenticated', 'uses' => AuthController::class . '@signIn']);
    Route::get('auth/signout', [ 'uses' => AuthController::class . '@signOut']);
    Route::get('auth/recover', [ 'middleware' => 'auth:ifIsNotAuthenticated', 'uses' => RecoverPasswordController::class . '@showRecoverPassword']);
    Route::post('auth/recover', [ 'middleware' => 'auth:ifIsNotAuthenticated', 'uses' => RecoverPasswordController::class . '@recoverPassword']);

    /*
     * Profile company routes
     */
    Route::get('profile', ['middleware' => 'auth', 'uses' => ProfileGeneralController::class . '@general']);
    Route::post('/profile/general/details', [ 'middleware' => 'auth', 'uses' => ProfileGeneralController::class . '@updateDetails']);

    Route::get('/profile/uploads', [ 'middleware' => 'auth', 'uses' => ProfileUploadsController::class . '@uploads']);
    Route::post('/profile/uploads/logo', [ 'middleware' => 'auth', 'uses' => ProfileUploadsController::class . '@uploadLogo']);

    Route::get('/profile/credentials', [ 'middleware' => 'auth', 'uses' => ProfileCredentialsController::class . '@changeEmailPassword']);
    Route::post('/profile/credentials/password', [ 'middleware' => 'auth', 'uses' => ProfileCredentialsController::class . '@changePassword']);
    Route::post('/profile/credentials/email', [ 'middleware' => 'auth', 'uses' => ProfileCredentialsController::class . '@changeEmail']);

    Route::get('profile/postings', ['middleware' => 'auth', 'uses' => ProfilePostingsController::class . '@postings']);
    Route::get('profile/postings/machine/deactivate/{id}', ['middleware' => 'auth', 'uses' => ProfilePostingsController::class . '@deactivate']);
    Route::get('profile/postings/machine/activate/{id}', ['middleware' => 'auth', 'uses' => ProfilePostingsController::class . '@activate']);
    Route::get('/machine/remove/{id}', [ 'middleware' => 'auth', 'uses' => ProfilePostingsController::class . '@removeMachine']);

//    Route::get('profile/newsletters', ['middleware' => 'auth', 'uses' => ProfileNewslettersController::class . '@newsletters']);
//    Route::get('profile/resources', ['middleware' => 'auth', 'uses' => ProfileResourcesController::class . '@resources']);
//
//    Route::get('profile/settings', ['middleware' => 'auth', 'uses' => ProfileSettingsController::class . '@settings']);
//    Route::post('profile/settings', ['middleware' => 'auth', 'uses' => ProfileSettingsController::class . '@editSettings']);


    /*
     * Forbidden routes
     */
    Route::get('/forbidden/pageNotFound', [ 'uses' => ForbiddenController::class . '@pageNotFound']);

    /*
     * Languages routes
     */
    Route::get('/lang/{language}', [ 'uses' => BaseController::class . '@changeLanguage']);

    /*
     * Ajax routes
     */
    Route::post('/admin/company/festsearch', [ 'uses' => AdminCompaniesController::class . '@simpleSearch']);

    /**
     * Get captcha
     */
    Route::get('/captcha', function(){
        return Captcha::getCaptcha();
    });
