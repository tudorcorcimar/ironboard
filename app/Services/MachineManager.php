<?php


namespace App\Services;

use App\Services\ImageService;
use App\Models\Runtime\Machine\MachineTypesList;
use App\Models\Persistent\Users\Users;

class MachineManager
{
    public static function machineSerialization($machines) {
        $items = [];
        $imageService = new ImageService;
        foreach ($machines as $machine){
            $items[$machine->id] = [
                'active'      => $machine->active,
                'id'          => $machine->id,
                'name'        => $machine->name,
                'description' => $machine->description,
                'updated_at'  => $machine->updated_at,
                'status'      => $machine->status,
                'other_url'   => $machine->other_url,
                'user_id'     => $machine->user_id,
                'owner'       => self::getMachineOwnerByUserId($machine->user_id),
                'currency'    => $machine->currency,
                'price'       => $machine->price,
                'type'        => MachineTypesList::getType($machine->type),
                'images'      => $imageService->getImages($machine->id, $machine->user_id),
                'thumbnail'   => $imageService->getThumbnail($machine->id, $machine->user_id),
                'highlighted' => $machine->highlighted
            ]; 
            if(isset($machine->location_id)){
                $items[$machine->id]['location'] = $machine->location_id;
            }
        }
        return $items;
    }
    
    public static function getMachineOwnerByUserId($userId) {
        $user = Users::where('id', $userId)->first();
        $items = [
            'id'           => $user->id,
            'company_name' => $user->company_name,
            'name'         => $user->name,
            'public_email' => $user->public_email,
            'address'      => $user->address,
            'website'      => $user->website,
            'phone'        => $user->phone
        ];
        return $items;
    }
}