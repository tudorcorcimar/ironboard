<?php


namespace App\Services\Admin;

use App\Models\Persistent\Admin\Admin;
use App\Models\Persistent\Admin\Rank;

class AdminPermissionsManager
{
    const PERMISSION_ADMIN          = 'admin';
    
    const PERMISSION_TYPE_USERS     = 'users';
    const PERMISSION_TYPE_RANK      = 'rank';
    const PERMISSION_TYPE_MACHINES  = 'machines';
    const PERMISSION_TYPE_COMPANIES = 'companies';
    const PERMISSION_TYPE_SPELLS    = 'spells';
    const PERMISSION_TYPE_MAILER    = 'mailer';
    
    const PERMISSION_ACTION_SEARCH  = 'search';
    const PERMISSION_ACTION_ADD     = 'create';
    const PERMISSION_ACTION_EDIT    = 'edit';
    
    const PERMISSION_SPARATOR       = '.';

    private $user                   = null;
    private $permissions            = '';
    
    /*
     * Constructor
     */
    public function __construct() {}
    
    /*
     * Check if user has permissions
     */
    public function userHasPermission($user, $permissions) {
        
        $this->user = $user;
        
        
        $this->permissions = $permissions;
        
        $userHasPermissions = false;
        
        if($this->user){
            $userHasPermissions = $this->checkUserPerrmission();
        }
        return $userHasPermissions;
    }
    
    /*
     * Check user permissions
     */
    private function checkUserPerrmission() {
        $hasPermission = false;
        if($this->userIsAdmin()){
            $hasPermission = true;
            return $hasPermission;
        }
        
        $rank = $this->getRank();
        if($rank){
            $hasPermission = $this->checkPermissions($rank);
        }
        
        return $hasPermission;
    }
    
    /*
     * Check if user is admin
     */
    private function userIsAdmin() {
        return $this->user->permissions == self::PERMISSION_ADMIN;
    }
    
    /*
     * Get perrmisions by rank name
     */
    private function getRank() {
        return Rank::where('name', '=', $this->user->permissions)->first();
    }
    
    /*
     * Check permissions
     */
    private function checkPermissions($rank) {
         $permission = explode(self::PERMISSION_SPARATOR, $this->permissions);
         
         $type = $permission[0];
         $action = $permission[1];
         
         $rankPermissions = json_decode($rank->permissions, true);
         
         return $rankPermissions[$type][$action];
    }
    
    /*
     * Array of permissions to string with PERMISSION_SPARATOR betewen them
     * 
     * @param $type string
     * @param $action string
     * 
     * @return string
     */
    public function getNeededPermissions($type, $action) {
        return $type . self::PERMISSION_SPARATOR . $action;
    }
    
    public function getPermissionTypes() {
        return [
            self::PERMISSION_TYPE_USERS,
            self::PERMISSION_TYPE_RANK,
            self::PERMISSION_TYPE_MACHINES,
            self::PERMISSION_TYPE_COMPANIES,
            self::PERMISSION_TYPE_SPELLS,
            self::PERMISSION_TYPE_MAILER
        ];
    }
    
    public function getPermissionActions() {
        return [
            self::PERMISSION_ACTION_SEARCH,
            self::PERMISSION_ACTION_ADD,
            self::PERMISSION_ACTION_EDIT
        ];
    }
    
}