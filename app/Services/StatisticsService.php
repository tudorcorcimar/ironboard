<?php


namespace App\Services;
use App\Models\Persistent\Machine\Machine;
use App\Models\Persistent\Users\Users;

class StatisticsService
{
    public static function get(){
        
        $statistics = [
            'companies' => [
                'total' => Users::count()
            ],
            'machines' => [
                'total' => Machine::count()
            ]
        ];
        
        return $statistics;
        
    }
}