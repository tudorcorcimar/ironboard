<?php


namespace App\Services;

use App\Services\ImageService;
use App\Models\Runtime\Auth\AdminAuth as AdminAuthentication;

class CompaniesManager
{
    
    public static function companySerialization($companies) {
        $items = [];
        $imageService = new ImageService;
        
        foreach ($companies as $company){
            $items[$company->id] = [
                'id'           => $company->id,
                'active'       => $company->active,
                'subscribed'   => $company->subscribed,
                'name'         => $company->name,
                'company_name' => $company->company_name,
                'phone'        => $company->phone,
                'email'        => $company->email,
                'description'  => $company->description,
                'updated_at'   => $company->updated_at,
                'address'      => $company->address,
                'website'      => $company->website,
                'public_email' => $company->public_email,
                'highlighted'  => $company->highlighted,
                'locations'    => $company->usersLocations,
                'logo'         => $imageService->getImages(false, $company->id, true)
            ];
            
            if( AdminAuthentication::getInstance()->getIsAuth() ){
                $items[$company->id]['password'] = $company->password;
                $items[$company->id]['last_comment'] = $company->last_comment;
            }
            
        }
        return $items;
    }
    
}