<?php


namespace App\Services;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ValidatorService
{
    public static function validate($rules){
        
        Validator::extend('captcha', function($attribute, $value, $parameters, $validator){
            return strtolower($value) == strtolower(Session::get('captcha'));
        });
        
        $messages = [
            'captcha' => 'Captha faild ..',
        ];
        
        return Validator::make(Input::all(), $rules, $messages);
        
    }
}