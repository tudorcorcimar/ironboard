<?php

namespace App\Services;

use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class ImageService
{
    const MAX_IMAGE_COUNT = 3;
    
    protected $generalPath;
    protected $companyPath;
    protected $thumbnailPathName;
    
    public function __construct(){
        $this->thumbnailPathName = public_path().'/images/thumbnail/';
        $this->generalPath = public_path().'/images/machine/';
        $this->companyPath = public_path().'/images/companies/';
        
        if(!File::exists($this->thumbnailPathName)){
            File::makeDirectory($this->thumbnailPathName, 0777, true);
        }
    }
    
    private function convertBase64ToJpg($base64String, $path) {
        $filePath = $path.'/'.rand(999, 9999).'.jpg';
        $file = fopen($filePath, "wb");
        $data = explode(',', $base64String);
        fwrite($file, base64_decode($data[1]));
        fclose($file); 
        return $filePath;
    }
    
    private function convertJpgToBase64($imagePath) {
        if(File::exists($imagePath)) {
            $type = pathinfo($imagePath, PATHINFO_EXTENSION);
            $data = file_get_contents($imagePath);
            $base64String = 'data:image/' . $type . ';base64,' . base64_encode($data);
        }
        return $base64String;
    }
    
    private function getPath($userId, $machineId, $isLogo) {
        if($isLogo){
            return $this->companyPath.$userId;
        }
        return $this->generalPath.$userId.'/'.$machineId;
    }
    
    public function getMachineImagesArrayInBase64($images, $machineId, $userId) {
        $base64ImagesArray = [];
        foreach ($images as $image){
            $path = $this->generalPath.$userId.'/'.$machineId.'/'.$image->getFilename();
            $base64ImagesArray[] = $this->convertJpgToBase64($path);
        }
        return $base64ImagesArray;
    }
    
    public function getLogoImageInBase64($userId) {
        $base64ImagesArray = [];
        
        $path = $this->companyPath.$userId;
        $images = File::allFiles($path);
        $path = $path.'/'.$images[0]->getFilename();
        $base64ImagesArray[] = $this->convertJpgToBase64($path);
        
        return $base64ImagesArray;
    }
    
    public function saveImages($imagesInBase64 = [], $machineId, $userId, $isLogo = false){
        $path = $this->getPath($userId, $machineId, $isLogo);
        if($isLogo){
            File::deleteDirectory($path);
            
        }
        if(!File::exists($path)) {
            File::makeDirectory($path, 0777, true);
        }
        $count = 0;
        foreach ($imagesInBase64 as $image){
            $currentImagePath = '';
            $count ++;
            if($count < 4){
                $currentImagePath = $this->convertBase64ToJpg($image, $path);
            }
            if($count === 1 && !$isLogo){
                $this->createThumnail($currentImagePath, $machineId, $userId);
            }
        }
    }
    
    public function getImages($machineId, $userId, $isLogo = false){
        $path = $this->getPath($userId, $machineId, $isLogo);
        $images = [];
        if(count(File::files($path)) > 0 ){
            $images = File::allFiles($path);
        }
        return $images;
    }
    
    public function getThumbnail($machineId, $userId){
        
        $thumbPath = $this->thumbnailPathName.$userId."/".$machineId;
        
        if(!File::exists($thumbPath)){
            File::makeDirectory($thumbPath, 0777, true);
        }
        $thumbnail = null;
        $thumbnails = [];
        if(count(File::files($thumbPath)) > 0 ){
            $thumbnails = File::allFiles($thumbPath);
            $thumbnail = $thumbnails[0];
        }
        
        return $thumbnail;
    }
    
    public function deleteFolderWithImages($machineId, $userId){
        $path = $this->generalPath.$userId.'/'.$machineId;
        $thumbnail = $this->thumbnailPathName.$userId.'/'.$machineId;
        File::deleteDirectory($path);
        File::deleteDirectory($thumbnail);
    }
    
    public function createThumnail($imagePath, $machineId, $userId) {
        $thumbnailPath = $this->thumbnailPathName.$userId."/".$machineId;
        
        if(!File::exists($thumbnailPath)){
            File::makeDirectory($thumbnailPath, 0777, true);
        }
        
        File::cleanDirectory($thumbnailPath);
        
        $image = Image::make($imagePath);
        $resizedImage = $image->heighten('150');
        
        if(File::exists($thumbnailPath)){
            $resizedImage->save($thumbnailPath."/thumbnail.jpg");
        }
    }
    
}