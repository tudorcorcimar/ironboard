<?php

namespace App\Models\Runtime\Cms;

use App\Models\Persistent\App\AppLanguages;

class CmsManager
{
    public function __construct(){}
    
    public function getLanguage() {
        return session('language');
    }
    
    public function setLanguage($lang){
        $appLaguages = new AppLanguages;
        if(in_array($lang, $appLaguages->getAllLanguages())){
            session(['language' => $lang]);
        }
    }
    
    private function serializeViewData($viewsDataAll) {
        $viewData = [];
        foreach($viewsDataAll as $view){
            $viewData[$view->cms_name] = [
                'id' => $view->cms_id,
                'name' => $view->cms_name,
                'content' => $view->cms_content
            ];
        }
        return $viewData;
    }
    
    public function getSpellsFromArrayOfNames($arrayOfNames) {
        $appLaguages = new AppLanguages;
        $languages = $appLaguages->getAllLanguages();
        $spells = [];
        foreach ($languages as $language){
            $viewDataEntityClassName = "App\Models\Persistent\Cms\Cms" . $language;
            $data = $viewDataEntityClassName::whereIn('cms_name', $arrayOfNames)->get()->sortBy('cms_id');
            $spells[$language] = $this->serializeViewData($data);
        }
        
        return $spells;
    }
    
    public function getViewsData(){
        $viewDataEntityClassName = "App\Models\Persistent\Cms\Cms" . $this->getLanguage();
        $viewsData = self::serializeViewData($viewDataEntityClassName::all());
        return $viewsData;
    }
    
}
