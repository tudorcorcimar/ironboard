<?php
namespace App\Models\Runtime\Auth;

use App\Models\Persistent\Users\Users;
use Illuminate\Support\Facades\Session;

/**
 * User Authentication
 *
 * @author Tudor Corcimar
 * @version 13/04/2016
 * @package App/Models/Runtime/Auth
 * @copyright (c) 2016, IronBoard
 */
class Auth {

    private $isAuth;
    private $user;
    
    private $type = null;
    private $active = true;
    
    //singleton instance
    private static $instance;
    
    public function getIsAuth() {
        return $this->isAuth;
    }
    
    public function getUser() {
        return $this->user;
    }
    
    public function getActive() {
        return $this->active;
    }
    
    public function isAdmin() {
        $this->user = Users::where('id', Session::get('user.id'))->first();
        return !empty($this->user) && $this->user->type == 9;
    }

    /**
     * Singleton
     * The constructor is private
     */
    private function __construct(){
        if(Session::has('user.id')){
            $this->user = Users::where('id', Session::get('user.id'))->first();
            if(!empty($this->user)){
                $this->isAuth = true;
                $this->type = $this->user->type;
            } else {
                $this->isAuth = false;
                $this->type = null;
            }
        }
    }

    /**
     * Singleton
     * Instance generator
     */
    public static function getInstance(){
        if(self::$instance instanceof self){
            return self::$instance;
        } else {
            self::$instance = new self();
            return self::$instance;
        }
    }

    /**s
     * Checks username/password validity loads properties and stores auth data in session
     *
     * @param String $username
     * @param String $password
     * @return boolean
     */
    public function login($username = "", $password = ""){
        
        $user = Users::where('email', $username)->first();
        
        if($user){
            $this->user = $user;
            if(!$this->user->active){
                $this->active = false;
            }
            if($this->active && $this->user->password == $password){
                $this->isAuth = true;

                //store in session
                Session::put('user.id', $this->user->id);
                $this->type = $this->user->type;

                return true;
            } else{
                $this->isAuth = false;
                return false;
            }

        } else {
            $this->isAuth = false;
            return false;
        }
        
    }

    /**
     * logout
     * Checks username/password validity loads properties and stores auth data in session
     *
     * @return boolean
     */
    public function logout(){
        $this->isAuth = false;
        $this->user = null;
        $this->type = null;
        Session::forget('user.id');
    }
}