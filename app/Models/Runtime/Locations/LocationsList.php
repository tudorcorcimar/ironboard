<?php
namespace App\Models\Runtime\Locations;

use App\Models\Persistent\Users\Users;
use Illuminate\Support\Facades\Session;
use App\Models\Persistent\Locations\Locations;
use App\Models\Persistent\Locations\Regions;

/**
 * Locations singleton
 *
 * @author Tudor Corcimar
 * @version 13/04/2016
 * @package App/Models/Runtime/Locations
 * @copyright (c) 2016, IronBoard
 */
class LocationsList {
    
    public static function getCountries() {
        return Locations::all();
    }
    
    public static function getRegions() {
        return Regions::all();
    }

}