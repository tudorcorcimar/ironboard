<?php

namespace App\Models\Runtime\App;

use App\Models\Persistent\App\AppInfo;
use Illuminate\Support\Facades\Cache;

class AppManager
{
    const CACHE_TIME = 20;
    
    public function __construct() {
        
    }
    
    public function getAppInfo() {
        $appInfo = Cache::remember('app.info.data', self::CACHE_TIME, function() {
            return AppInfo::where('id', '=', '1')
                          ->get()
                          ->first();
        });      
        $serializedAppInfo = $this->serializeAppInfo($appInfo);
        return ($serializedAppInfo);
    }
    
    private function serializeAppInfo($appInfo) {
        $app = [];
        
        $app['meta'] = [];
        $app['meta']['description'] = $appInfo->meta_description;
        $app['meta']['keywords'] = $appInfo->meta_keywords;
        $app['meta']['robots'] = $appInfo->meta_robots;
        $app['meta']['charset'] = $appInfo->meta_charset;
        $app['meta']['lang'] = $appInfo->meta_lang;
        
        $app['url'] = $appInfo->url;
        $app['email'] = $appInfo->email;
        $app['name'] = $appInfo->name;
        $app['address'] = $appInfo->address;
        $app['phone'] = $appInfo->phone;
        $app['fax'] = $appInfo->fax;
        
        return $app;
    }
    
}
