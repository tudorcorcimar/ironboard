<?php
namespace App\Models\Runtime\Machine;

use App\Models\Persistent\Machine\MachineTypes;

/**
 * Machine Types List
 *
 * @author Tudor Corcimar
 * @version 13/04/2016
 * @package App/Models/Runtime/Machine
 * @copyright (c) 2016, IronBoard
 */
class MachineTypesList {
    
    public static function getAll() {
        return MachineTypes::all();
    }
    
    public static function getType($id) {
        return MachineTypes::where('id', $id)->get()->first();
    }
}