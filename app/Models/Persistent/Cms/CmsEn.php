<?php

namespace App\Models\Persistent\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsEn extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_eng';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
