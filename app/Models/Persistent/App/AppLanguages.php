<?php

namespace App\Models\Persistent\App;

class AppLanguages
{
    const ENGLISH = 'En';
    const ITALIAN = 'It';
    const RUSSIAN = 'Ru';
    const DEUTSCH = 'De';
    const FRENCH  = 'Fr';
    
    public function getAllLanguages() {
        return [
            self::ENGLISH, 
            self::ITALIAN,
            self::RUSSIAN,
            self::DEUTSCH,
            self::FRENCH
        ];
    }
}
