<?php

namespace App\Models\Persistent\App;

use Illuminate\Database\Eloquent\Model;

class AppInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'app_info';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
