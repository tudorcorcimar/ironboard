<?php

namespace App\Models\Persistent\Saves;

use Illuminate\Database\Eloquent\Model;

class SavedSearch extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'saved_search';
    
}
