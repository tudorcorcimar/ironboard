<?php

namespace App\Models\Persistent\Saves;

use Illuminate\Database\Eloquent\Model;

class SavedMachines extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'saved_machines';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
}
