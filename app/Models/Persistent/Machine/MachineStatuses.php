<?php

namespace App\Models\Persistent\Machine;

class MachineStatuses
{
    const STATUS_USED = 1;
    const STATUS_NEW = 2;
    
    public static function getAll(){
        return array(
            [ 'id' => self::STATUS_NEW, 'name' => 'new' ],
            [ 'id' => self::STATUS_USED, 'name' => 'used' ]
        );
    }
    
}
