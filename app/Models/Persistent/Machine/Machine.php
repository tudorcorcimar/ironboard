<?php

namespace App\Models\Persistent\Machine;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'machine';
    
    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Persistent\Users\Users', 'user_id');
    }
}
