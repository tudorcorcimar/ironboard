<?php

namespace App\Models\Persistent\Users;

use Illuminate\Database\Eloquent\Model;


class UsersSettings extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_settings';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
}
