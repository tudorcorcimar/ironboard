<?php

namespace App\Models\Persistent\Users;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';
    
    /**
     * Get the locations for users.
     */
    public function usersLocations()
    {
        return $this->hasMany('App\Models\Persistent\Users\UsersLocations');
    }
    
    /**
     * Get the locations for users.
     */
    public function usersAdminComments()
    {
        return $this->hasMany('App\Models\Persistent\Users\UsersAdminComments');
    }
    
}
