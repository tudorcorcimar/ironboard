<?php

namespace App\Models\Persistent\Users;

class UserType 
{
    const USER_GUEST = 1;
    const USER_COMPANY = 2;
    const USER_INDIVIDUAL = 3;
    const USER_ADMIN = 4;
    
}
