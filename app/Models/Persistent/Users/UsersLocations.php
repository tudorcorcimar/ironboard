<?php

namespace App\Models\Persistent\Users;

use Illuminate\Database\Eloquent\Model;

class UsersLocations extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_locations';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
}
