<?php

namespace App\Models\Persistent\Users;

use Illuminate\Database\Eloquent\Model;


class UsersAdminComments extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_admin_comments';
    
}
