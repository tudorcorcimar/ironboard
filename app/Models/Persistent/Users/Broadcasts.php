<?php

namespace App\Models\Persistent\Users;

use Illuminate\Database\Eloquent\Model;

class Broadcasts extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'broadcasts';
    
}
