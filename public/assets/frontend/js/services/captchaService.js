var captchaService = (function(){
    
    /*
    * Refrash Captcha
    */
   this.refrashCaptcha = function(btn){
      var image = $(btn).attr('data-captha');
      var rand  = Math.floor((Math.random() * 9999) + 1111);
      $(".captchaImg img").attr('src', image + '?' + rand);
   };
   
   return this;
   
}());