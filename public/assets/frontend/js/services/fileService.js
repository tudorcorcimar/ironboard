
var currentFile;
var imagesCount = 0;
var isLogo = false;


var getImageCount = function(){
    return $('.uploaded-image').length;
};

if(getImageCount() < 3){
    $('.upload-image-button').removeClass('hidden');
}
  
/*
 * Prepare file and set max width
 */
function dropChangeHandler (e) {
    e.preventDefault()
    e = e.originalEvent
    var target = e.dataTransfer || e.target
    var file = target && target.files && target.files[0]

    var options = {
        maxWidth: 700,
        maxHeight: 700,
        minWidth: 500,
        minHeight: 500,
        canvas: true
    }
    
    if(isLogo){
        options = {
            maxWidth: 200,
            maxHeight: 200,
            minWidth: 150,
            minHeight: 150,
            canvas: true
        }
    }
    
    if (!file) { return; }
    loadImage.parseMetaData(file, function (data) {
    displayImage(file, options, isLogo);
    imagesCount++;
    if(imagesCount == 3 && !isLogo){
        $('.upload-image-button').fadeOut('500');
    }
  });
}

/*
 * Create image
 */
function displayImage (file, options) {
    currentFile = file;
    if (!loadImage( file, replaceResults, options )){
        $('.temp-block').replaceWith(
            $('<span>Your browser does not support the URL or FileReader API.</span>')
        );
    }
}

/*
 * Add new image to content
 */
function replaceResults (img) {
    var content;
    if (!(img.src || img instanceof HTMLCanvasElement)) {
        content = $('<span>Loading image file failed</span>');
    } else {
        var clon = $('#uploaded-image-template').clone(true);
        var imageUrl = img.src || img.toDataURL();
        $('.image-back', clon).css('background', 'url(' + imageUrl + ') center center no-repeat')
                              .css('background-size', 'cover');
        clon.removeAttr('id')
            .removeClass('hidden').addClass('uploaded-image');
        $('textarea.base-64-storage', $(clon)).append(imageUrl)
                                           .prop('disabled', false);
        if(isLogo){
            $('.image-back', clon).css('background-size', 'contain');
            removeImage($('.uploaded-image'));
        }
        if(isLogo && !$('#uploaded-image-template').hasClass('hidden')){
            $('#uploaded-image-template').addClass('hidden');
        }
        $('.image-uploader-loader').hide();
    }
    $('.uploaded-images').append(clon);
}

/*
 * Remove image
 */
function removeImage(element) {
    $(element).remove();
}

$('.image-remove-button').on('click', function(){
    var clon = $(this).closest('.uploaded-image');
    removeImage($(clon));
    imagesCount = getImageCount();
    
    if(imagesCount < 3){
        $('.upload-image-button').fadeIn('500');
        $('.upload-image-button').removeClass('hidden');
    }
});

$('.upload-image-button .clicable-zone').on('click', function(){
    $('.images-uploader-block > input[type="file"]').click();
});

$('.images-uploader-block > input[type="file"]').on('change', function (e) {
    if($(this).hasClass('logo-image')){
        isLogo = true;
    }
    if( isLogo && $('.uploaded-image').length > 0 ){
        var clon = $(this).closest('.uploaded-image');
        removeImage($(clon));
    }
    if( this.files.length === 0 ){
        $('.image-uploader-loader').hide();
    }else{
        dropChangeHandler(e, isLogo);
    }
});