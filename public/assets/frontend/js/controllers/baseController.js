$('form').on('submit', function(event){
    $('.web-loader').removeClass('hidden');
});
$('.tablet-nav-button, .tablet-nav-close-button').on('click', function(event){
    $('.tablet-nav').toggleClass('hidden');
    if($('.tablet-nav').hasClass('hidden')){
        $('html, body').css('overflow', 'auto');
    }else{
        $('html, body').css('overflow', 'hidden');
    }
});

/*
 * Saves Navigation Controller
 */
var closeAllSavesWindows = function(){
    $('.saves-button').removeClass('active');
    $('.saves-list').addClass(function(){
        if($(this).hasClass('hidden')){
            return '';
        }
        return 'hidden';
    });
};
$('html').click(function(event) {
    if ($(event.target).parents('.settings-on-search').length===0) {
        closeAllSavesWindows();
    }
});
$('.saves-button').on('click', function(e){
    var selector = $(this).attr('id');
    $('.saves-button').removeClass('active');
    $(this).addClass('active');
    $('.saves-list').addClass(function(){
        if($(this).hasClass('hidden')){
            return '';
        }
        return 'hidden';
    });
    $('.saves-list.'+selector).removeClass('hidden');
});
$('.saves-list-close').on('click', function(e){
    closeAllSavesWindows();
});

// Save machine
var saveMachine = function(machineId){
    var id = machineId.replace("post-id-","");
    var url = baseUrl + "/machine/saveMachine";
    $.ajax({
        method: "POST",
        url: url,
        data: { 
            machine: {
                id: id
            } 
        }
    })
    .done(function( data ) {
        if(typeof data.type !== 'undefined'){
            growlMessage = data;
            showGrowl();
        }
    });
};

var unsaveMachine = function(machineId, button){
    var id = machineId.replace("post-id-","");
    var url = baseUrl + "/machine/unsaveMachine";
    $.ajax({
        method: "POST",
        url: url,
        data: { 
            machine: {
                id: id
            } 
        }
    })
    .done(function( data ) {
        if(typeof data.type !== 'undefined'){
            growlMessage = data;
            showGrowl();
        }
    });
};

$('.save-machine-button').on('click', function(){
    if($(this).hasClass('is-saved')){
        var machineId = $(this).attr('id');
        unsaveMachine(machineId, this);
        $(this).removeClass('is-saved');
        $(this).removeClass('is-saved-machine');
        $(this).addClass('save-machine-button');
    }else{
        if(!$(this).hasClass('notAuthenticatedAction')){
            var machineId = $(this).attr('id');
            saveMachine(machineId, this);
            $(this).addClass('is-saved');
            $(this).addClass('is-saved-machine');
            $(this).removeClass('save-machine-button');
        }
    }
});

// Save search
$('.save-machine-search-button').on('click', function(e){
    if(!$(this).hasClass('notAuthenticatedAction')){
        e.preventDefault();
        $('.save-search-popup-form').modal('show');
        return false;
    }
});
$('.save-search-popup-button').on('click', function(e){
    var searchName = $('.save-search-name-input').val();
    $('.save-search-form').append('<input type="hidden" name="save-name" value="'+searchName+'"/>');
    $('.save-search-form').append('<input type="hidden" name="save-search" value="1"/>');
    $('.save-search-form').submit();
});


// Hide banners

var showBanners = function(){
    $('.banners').removeClass('hide-banners');
    $('.hide-banners-button').removeClass('hidden');
    $('.show-banners-button').addClass('hidden');
    localStorage.setItem("hideBannersStatus", 'false');
};
var hideBanners = function(){
    $('.banners').addClass('hide-banners');
    $('.show-banners-button').removeClass('hidden');
    $('.hide-banners-button').addClass('hidden');
    localStorage.setItem("hideBannersStatus", 'true');
};

$('.show-banners-button').on('click', function(){
    if( $(this).hasClass('notAuthenticatedAction') ){
        localStorage.setItem("hideBannersStatus", 'false');
    }else{
        showBanners();
    }
});
$('.hide-banners-button').on('click', function(){
    if( $(this).hasClass('notAuthenticatedAction') ){
        localStorage.setItem("hideBannersStatus", 'false');
    }else{
        hideBanners();
    }
});
if( $('.show-banners-button').hasClass('notAuthenticatedAction') ){
    localStorage.setItem("hideBannersStatus", 'false');
}
var hideBannersStatus = localStorage.getItem("hideBannersStatus");
if(hideBannersStatus === 'true'){
    hideBanners();
}else{
}


// Folow us popup
var followUsPopup = localStorage.getItem("followUsPopup");
if(!followUsPopup && followUsPopup !== 'true'){
    $('.follow-us-popup').removeClass('hidden');
}
$('.follow-us-popup-close .glyphicon').on('click', function(){
    $('.follow-us-popup').addClass('hidden');
    localStorage.setItem("followUsPopup", 'true');
});
