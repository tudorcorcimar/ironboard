lightbox.option({
    'resizeDuration': 250,
    'fadeDuration': 0,
    'wrapAround': true,
    'showImageNumberLabel': false
});
    
$( ".board-linear .post .image-block .floating-block.logo-image img" ).each(function( index, val ) {
    var height = $(val).height();
    if(height < 80){
        var margin = Math.round(40 - height/2);
        $(this).css('margin-top', margin + 'px');
    }
});
