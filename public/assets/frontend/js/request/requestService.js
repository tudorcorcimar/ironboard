var request = {};

request.call = function(url, data, controller){
    data.url = app.config.baseUrl + url;
    
    // CSRF protection
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-Token': csrfToken.value
        }
    });
    
    $.ajax({
        method: "POST",
        url: url,
        data: data.json,
        success: function (response) {
            controller.result(response);
        }
    });
    
};