<?php

use Phinx\Migration\AbstractMigration;

class CreateRankTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE rank ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " name VARCHAR(255) DEFAULT '', "
                       . " permissions TEXT NOT NULL DEFAULT '', "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE rank;");
    }
}
