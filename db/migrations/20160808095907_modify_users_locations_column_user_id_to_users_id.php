<?php

use Phinx\Migration\AbstractMigration;

class ModifyUsersLocationsColumnUserIdToUsersId extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("ALTER TABLE users_locations CHANGE user_id users_id INT(11);");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
