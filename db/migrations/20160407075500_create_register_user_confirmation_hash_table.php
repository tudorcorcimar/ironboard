<?php

use Phinx\Migration\AbstractMigration;

class CreateRegisterUserConfirmationHashTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE users_register_key ("
                       . " user_id INT(11) NOT NULL UNIQUE, "
                       . " hash VARCHAR(255), "
                       . " created_at DATE, "
                       . " updated_at DATE );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
