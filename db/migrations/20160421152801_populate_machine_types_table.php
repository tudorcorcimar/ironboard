<?php

use Phinx\Migration\AbstractMigration;

class PopulateMachineTypesTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("INSERT INTO machine_types "
                       . " (id, name) "
                       . " VALUES "
                       . " (1, 'Bar loaders'), "
                       . " (2, 'Beading machines'), "
                       . " (3, 'Bending machines'), "
                       . " (4, 'Bending rolls'), "
                       . " (5, 'Boring'), "
                       . " (6, 'Boring machines'), "
                       . " (7, 'Broaching machines'), "
                       . " (8, 'Centring and facing machines'), "
                       . " (9, 'Chamfering machines'), "
                       . " (10, 'Cutting off machines'), "
                       . " (11, 'Deburring machines'), "
                
                       . " (12, 'Drilling machines multi-spindle'), "
                       . " (13, 'Drilling machines single-spindle'), "
                
                       . " (14, 'End forming machines'), "
                       . " (15, 'Engraving machines'), "
                       . " (16, 'Flattening machines'), "
                       . " (17, 'Fly presses'), "
                       . " (18, 'Forklift'), "
                       . " (19, 'Gear machines'), "
                
                       . " (20, 'Grinding machines - centreless'), "
                       . " (21, 'Grinding machines - external'), "
                       . " (22, 'Grinding machines - horiz. spindle'), "
                       . " (23, 'Grinding machines - internal'), "
                       . " (24, 'Grinding machines - spec. purposes'), "
                       . " (25, 'Grinding machines - unclassified'), "
                       . " (26, 'Grinding machines - universal'), "
                       . " (27, 'Hacksaws'), "
                       . " (28, 'Hammers'), "
                       . " (29, 'Honing machines'), "
                       . " (30, 'Jig borers'), "
                       . " (31, 'Lapping machines'), "
                       . " (32, 'Laser cutting machines'), "
                
                       . " (33, 'Lathes - automatic CNC'), "
                       . " (34, 'Lathes - automatic multi-spindle'), "
                       . " (35, 'Lathes - automatic single-spindle'), "
                       . " (36, 'Lathes - centre'), "
                       . " (37, 'Lathes - CN/CNC'), "
                       . " (38, 'Lathes - copying'), "
                       . " (39, 'Lathes - facing'), "
                       . " (40, 'Lathes - semifacing'), "
                       . " (41, 'Lathes - unclassified'), "
                       . " (42, 'Lathes - vertical'), "
                
                       . " (43, 'Machining centres'), "
                       . " (44, 'Machining lines'), "
                       . " (45, 'Material testing machines'), "
                       . " (46, 'Measuring and testing'), "
                       . " (47, 'Milling and boring machines'), "
                
                       . " (48, 'Milling machines - bed type'), "
                       . " (49, 'Milling machines - copying'), "
                       . " (50, 'Milling machines - die-sinking'), "
                       . " (51, 'Milling machines - high speed'), "
                       . " (52, 'Milling machines - horizontal'), "
                       . " (53, 'Milling machines - plano'), "
                       . " (54, 'Milling machines - spec. purposes'), "
                       . " (55, 'Milling machines - tool and die'), "
                       . " (56, 'Milling machines - unclassified'), "
                       . " (57, 'Milling machines - universal'), "
                       . " (58, 'Milling machines - vertical'), "
                
                       . " (59, 'Notching machines'), "
                       . " (60, 'Ovens'), "
                       . " (61, 'Planing machines'), "
                       . " (62, 'Plasma'), "
                       . " (63, 'Polishing machines'), "
                       . " (64, 'Positioners'), "
                
                       . " (65, 'Presses - brake'), "
                       . " (66, 'Presses - eccentric'), "
                       . " (67, 'Presses - forging'), "
                       . " (68, 'Presses - hydraulic'), "
                       . " (69, 'Presses - mechanical'), "
                       . " (70, 'Presses - unclassified'), "
                
                       . " (71, 'Profile projectors'), "
                       . " (72, 'Profiling machines'), "
                       . " (73, 'Punching machines'), "
                       . " (74, 'Reels'), "
                       . " (75, 'Riveting machines'), "
                       . " (76, 'Robots'), "
                       . " (77, 'Rolling machines'), "
                       . " (78, 'Rolling mills'), "
                       . " (79, 'Sandblasting machines'), "
                       . " (80, 'Sanding machines'), "
                       . " (81, 'Sawing machines'), "
                       . " (82, 'Shaping machines'), "
                       . " (83, 'Sharpening machines'), "
                       . " (84, 'Shaving machines'), "
                       . " (85, 'Shears'), "
                       . " (86, 'Sheet metal bending machines'), "
                       . " (87, 'Slotting machines'), "
                       . " (88, 'Spark erosion machines'), "
                       . " (89, 'Spot welding machines'), "
                       . " (90, 'Straightening machines'), "
                       . " (91, 'Swing-frame grinding machines'), "
                       . " (92, 'Tapping machines'), "
                       . " (93, 'Threading machines'), "
                       . " (94, 'Transfer machines'), "
                       . " (95, 'Tube-bending machines'), "
                       . " (96, 'Turning centres'), "
                       . " (97, 'Welding machines'), "
                       . " (98, 'Work tables'), "
                       . " (99, 'Working plates'), "
                       . " (100, 'Other') ;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
