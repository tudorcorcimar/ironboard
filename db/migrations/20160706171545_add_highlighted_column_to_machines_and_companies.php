<?php

use Phinx\Migration\AbstractMigration;

class AddHighlightedColumnToMachinesAndCompanies extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        
        $this->execute("ALTER TABLE machine "
            . " ADD highlighted BOOLEAN NOT NULL DEFAULT FALSE ;");
        
        $this->execute("ALTER TABLE users "
            . " ADD highlighted BOOLEAN NOT NULL DEFAULT FALSE ;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}