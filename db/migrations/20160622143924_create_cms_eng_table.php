<?php

use Phinx\Migration\AbstractMigration;

class CreateCmsEngTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE cms_eng ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " cms_id INT(11), "
                       . " cms_name TEXT DEFAULT '', "
                       . " cms_content TEXT DEFAULT '', "
                       . " PRIMARY KEY (id) );");
        
        $this->execute("CREATE TABLE cms_it ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " cms_id INT(11), "
                       . " cms_name TEXT DEFAULT '', "
                       . " cms_content TEXT DEFAULT '', "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE cms_eng;");
        $this->execute("DROP TABLE cms_it;");
    }
}
