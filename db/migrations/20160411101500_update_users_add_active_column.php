<?php

use Phinx\Migration\AbstractMigration;

class UpdateUsersAddActiveColumn extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("ALTER TABLE users "
                       . " ADD active TinyInt DEFAULT 0;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
