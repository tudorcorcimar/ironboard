<?php

use Phinx\Migration\AbstractMigration;

class ApplicationInfoTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE app_info ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " name VARCHAR(255) DEFAULT '', "
                       . " phone VARCHAR(255) DEFAULT '', "
                       . " fax VARCHAR(255) DEFAULT '', "
                       . " url VARCHAR(255) DEFAULT '', "
                       . " email VARCHAR(255) DEFAULT '', "
                       . " address TEXT DEFAULT '', "
                       . " meta_description TEXT DEFAULT '', "
                       . " meta_keywords TEXT DEFAULT '', "
                       . " meta_robots VARCHAR(255) DEFAULT 'noindex, nofollow', "
                       . " meta_charset VARCHAR(10) DEFAULT 'utf-8', "
                       . " meta_lang VARCHAR(10) DEFAULT 'en', "
                       . " PRIMARY KEY (id) );");
        
        $this->execute("INSERT INTO app_info "
                       . " (name, phone, fax, url, email, address, meta_description, meta_keywords) "
                       . " VALUES "
                       . " ('Iron Board', "
                       . "'+373 79177707', "
                       . "'', "
                       . "'www.ibmachine.com', "
                       . "'info@ibmachine.com', "
                       . "'UE - Castello (Venezia), 6208 p. T<br/>30122 VENEZIA (VE), Italy',"
                       . "'Iron BORD offers a wide specter of new and used machines covering almost all manufacturers. It also opens great opportunities for machines that come from auctions, liquidations and bankruptcies.',"
                       . "'Machine, Machines, Industrial, Industriale, Iron Board, Iron, Board') ;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE app_info;");
    }
}
