<?php

use Phinx\Migration\AbstractMigration;

class AddAdminCompanyComments extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE admin_company_comments ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " company_id INT(11) NOT NULL, "
                       . " comment TEXT NOT NULL, "
                       . " created_at DATE, "
                       . " updated_at DATE, "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE admin_company_comments;");
    }
}
