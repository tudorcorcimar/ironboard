<?php

use Phinx\Migration\AbstractMigration;

class DropColumnIdInUsersLocations extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("ALTER TABLE users_locations DROP COLUMN id;");
        $this->execute("ALTER TABLE users_locations DROP COLUMN region_id;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
