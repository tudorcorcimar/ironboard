<?php

use Phinx\Migration\AbstractMigration;

class PopulateUsersStatusTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("INSERT INTO users_status "
                       . " (id, name) "
                       . " VALUES "
                       . " (1, 'Guest'), "
                       . " (2, 'Company'), "
                       . " (3, 'Individual Saller'), "
                       . " (4, 'Admin') ;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
