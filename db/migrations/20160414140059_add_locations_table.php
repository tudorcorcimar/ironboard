<?php

use Phinx\Migration\AbstractMigration;

class AddLocationsTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE locations ("
                       . " id INT(4) NOT NULL AUTO_INCREMENT, "
                       . " name VARCHAR(255) NOT NULL, "
                       . " region_id INT(3) NOT NULL, "
                       . " active BOOLEAN NOT NULL DEFAULT TRUE, "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE locations;");
    }
}
