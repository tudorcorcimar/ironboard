<?php

use Phinx\Migration\AbstractMigration;

class RemoveUserRegisterKeyTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("DROP TABLE users_register_key;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
