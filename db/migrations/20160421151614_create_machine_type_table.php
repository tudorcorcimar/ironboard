<?php

use Phinx\Migration\AbstractMigration;

class CreateMachineTypeTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE machine_types ("
                       . " id INT(3) NOT NULL, "
                       . " name VARCHAR(255) NOT NULL, "
                       . " category_id INT(3) DEFAULT 1, "
                       . " active BOOLEAN NOT NULL DEFAULT TRUE, "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE machine_types;");
    }
}
