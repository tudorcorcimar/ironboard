<?php

use Phinx\Migration\AbstractMigration;

class AddLastCommentDateColumnToUsers extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("ALTER TABLE admin_company_comments CHANGE company_id users_id INT(11);");
        $this->execute("RENAME TABLE admin_company_comments TO users_admin_comments;");
        $this->execute("ALTER TABLE users ADD last_comment VARCHAR(100) NOT NULL DEFAULT '2016-08-08';");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("RENAME TABLE users_admin_comments TO admin_company_comments;");
    }
}
