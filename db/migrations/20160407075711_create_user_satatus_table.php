<?php

use Phinx\Migration\AbstractMigration;

class CreateUserSatatusTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE users_status ("
                       . " id INT(2), "
                       . " name VARCHAR(50) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE users_status;");
    }
}
