<?php

use Phinx\Migration\AbstractMigration;

class RemoveIdFromSaves extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("ALTER TABLE saved_machines DROP COLUMN id;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
