<?php

use Phinx\Migration\AbstractMigration;

class CreateUserTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE users ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " type INT(2) DEFAULT 1, "
                       . " name VARCHAR(255) DEFAULT '', "
                       . " surname VARCHAR(255) DEFAULT '', "
                       . " company_name VARCHAR(255) DEFAULT '', "
                       . " phone TEXT DEFAULT '', "
                       . " email VARCHAR(150) DEFAULT '', "
                       . " password VARCHAR(150) DEFAULT '', "
                       . " remember_token VARCHAR(100) DEFAULT '', "
                       . " description TEXT NOT NULL DEFAULT '', "
                       . " created_at DATE, "
                       . " updated_at DATE, "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE users;");
    }
}
