<?php

use Phinx\Migration\AbstractMigration;

class CreateMachineTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE machine ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " type INT(4) DEFAULT 100, "
                       . " status INT(2) DEFAULT 2, "
                       . " name TEXT DEFAULT '', "
                       . " user_id INT(11) NOT NULL, "
                       . " other_url TEXT DEFAULT '', "
                       . " description TEXT NOT NULL DEFAULT '', "
                       . " currency VARCHAR(10) DEFAULT '', "
                       . " price INT(11), "
                       . " active BOOLEAN NOT NULL DEFAULT FALSE, "
                       . " created_at DATE, "
                       . " updated_at DATE, "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE machine;");
    }
}
