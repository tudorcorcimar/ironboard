<?php

use Phinx\Migration\AbstractMigration;

class CreateUserSavesMachine extends AbstractMigration
{
    
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE saved_machines ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " machine_id INT(11), "
                       . " users_id INT(11), "
                       . " created_at DATETIME, "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE saved_machines;");
    }
    
}
