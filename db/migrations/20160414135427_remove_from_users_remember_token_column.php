<?php

use Phinx\Migration\AbstractMigration;

class RemoveFromUsersRememberTokenColumn extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("ALTER TABLE users DROP COLUMN remember_token;");
        $this->execute("ALTER TABLE users DROP COLUMN surname;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
