<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersSettingsTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE users_settings ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " user_id INT(11) UNIQUE, "
                       . " account_visible BOOLEAN NOT NULL DEFAULT TRUE, "
                       . " hide_machine BOOLEAN NOT NULL DEFAULT FALSE, "
                       . " newsletters BOOLEAN NOT NULL DEFAULT TRUE, "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE users_settings;");
    }
}
