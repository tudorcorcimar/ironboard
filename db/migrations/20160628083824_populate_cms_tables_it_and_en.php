<?php

use Phinx\Migration\AbstractMigration;

class PopulateCmsTablesItAndEn extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("INSERT INTO cms_eng "
            . " (cms_id, cms_name, cms_content) "
            . " VALUES "

            // Home
            . "(1, "
            . " 'home.title', "
            . " 'Find and post machines absolutely free!'),"
            . "(2, "
            . " 'home.title.short.info', "
            . " 'Iron BOARD offers a wide spectrum of new and used machines covering almost all manufacturers.<br/>It also opens great opportunities for machines that come from auctions, liquidations and bankruptcies.'),"
            . "(6, "
            . " 'home.button.post.machine', "
            . " 'Post Machine'),"
            . "(10, "
            . " 'home.button.find.machines', "
            . " 'Find machine'),"
            . "(12, "
            . " 'home.button.find.companies', "
            . " 'Find companies'),"
            . "(13, "
            . " 'home.button.contact.us', "
            . " 'Contact us'),"
            . "(14, "
            . " 'home.under.search.text', "
            . " 'In just a few clicks you will be able to quickly find the needed machine tools and equipment, new or used.<br/>Simply open the ad and contact the machine owner directly'),"
            . "(3, "
            . " 'form.field.search', "
            . " 'Search'),"

               
            // Header
            . "(4, "
            . " 'header.menu.button.home', "
            . " 'Home'),"
            . "(5, "
            . " 'header.menu.button.machines', "
            . " 'Machines'),"
            . "(7, "
            . " 'header.menu.button.companies', "
            . " 'Companies'),"
            . "(8, "
            . " 'header.menu.button.register', "
            . " 'Register'),"
            . "(9, "
            . " 'header.menu.button.sign.in', "
            . " 'Sign In'),"
            . "(122, "
            . " 'header.menu.button.post.machine', "
            . " 'Post machine'),"
            . "(123, "
            . " 'header.menu.button.profile', "
            . " 'Profile'),"
            . "(124, "
            . " 'header.menu.dropdown.general', "
            . " 'General'),"
            . "(125, "
            . " 'header.menu.dropdown.postings', "
            . " 'Postings'),"
            . "(126, "
            . " 'header.menu.dropdown.uploads', "
            . " 'Uploads'),"
            . "(127, "
            . " 'header.menu.dropdown.sign.out', "
            . " 'Sign Out'),"

               
            // Footer
            . "(128, "
            . " 'footer.block.suport.title', "
            . " 'Support'),"
            . "(129, "
            . " 'footer.block.suport.contact.us', "
            . " 'Contact Us'),"
            . "(130, "
            . " 'footer.block.suport.faq', "
            . " 'F.A.Q.'),"
            . "(132, "
            . " 'footer.block.aboutus.title', "
            . " 'About Us'),"
            . "(133, "
            . " 'footer.block.aboutus.aboutus', "
            . " 'About Us'),"
            . "(134, "
            . " 'footer.block.aboutus.services', "
            . " 'Services and Pakages'),"
            . "(135, "
            . " 'footer.block.aboutus.terms', "
            . " 'Terms & Conditions'),"
            . "(136, "
            . " 'footer.block.aboutus.privacy', "
            . " 'Privacy policy'),"
            . "(137, "
            . " 'footer.block.promo.title', "
            . " 'Promo'),"
            . "(138, "
            . " 'footer.block.promo.banner', "
            . " 'Banner advertising'),"
            . "(139, "
            . " 'footer.block.promo.adds', "
            . " 'Adds sdvertising'),"
            . "(140, "
            . " 'footer.block.promo.social', "
            . " 'Social Promotion'),"

               
            // Machine
            . "(141, "
            . " 'machine.search.form.title', "
            . " 'Find machine'),"
            . "(142, "
            . " 'machine.search.form.keywords', "
            . " 'Keywords'),"
            . "(143, "
            . " 'machine.search.form.type', "
            . " 'Type'),"
            . "(144, "
            . " 'machine.search.form.condition', "
            . " 'Condition'),"
            . "(145, "
            . " 'machine.search.form.button.search', "
            . " 'Search'),"
            . "(146, "
            . " 'machine.board.sort.name', "
            . " 'Name'),"
            . "(147, "
            . " 'machine.board.sort.condition', "
            . " 'Condition'),"
            . "(148, "
            . " 'machine.board.sort.type', "
            . " 'Type'),"
            . "(149, "
            . " 'machine.board.sort.last.update', "
            . " 'Last Update'),"
            . "(150, "
            . " 'machine.under.board.text', "
            . " 'Iron BOARD is an international platform for new and used machine tools trading. Easily post your own machine or quickly find a desired one.'),"
            . "(151, "
            . " 'machine.condition.new', "
            . " 'New'),"
            . "(152, "
            . " 'machine.condition.used', "
            . " 'Used'),"
            . "(153, "
            . " 'machine.title', "
            . " 'Machine'),"

               
            // Post Machine
            . "(154, "
            . " 'post.machine.title', "
            . " 'Post Machine'),"
            . "(155, "
            . " 'post.machine.breadcrumbs.machine', "
            . " 'Machines'),"
            . "(156, "
            . " 'post.machine.form.title', "
            . " 'Post machine'),"
            . "(157, "
            . " 'post.machine.form.machine.name', "
            . " 'Machine name'),"
            . "(158, "
            . " 'post.machine.form.condition', "
            . " 'Condition'),"
            . "(159, "
            . " 'post.machine.form.type', "
            . " 'Type'),"
            . "(160, "
            . " 'post.machine.form.description', "
            . " 'Description'),"
            . "(161, "
            . " 'post.machine.form.upload.image', "
            . " 'Upload image'),"
            . "(162, "
            . " 'post.machine.form.click.to.add.new.image', "
            . " '<b class=\"font-28\">Click </b> to add new image!'),"
            . "(163, "
            . " 'post.machine.form.image.uploaded', "
            . " 'Image succesful uploaded!'),"
            . "(164, "
            . " 'post.machine.form.more.details', "
            . " '<b>Click</b> to add more details! (optional)'),"
            . "(165, "
            . " 'post.machine.form.currency', "
            . " 'Currency'),"
            . "(166, "
            . " 'post.machine.form.url', "
            . " 'Website'),"
            . "(167, "
            . " 'post.machine.form.price', "
            . " 'Price'),"
            . "(168, "
            . " 'post.machine.form.button.post.machine', "
            . " 'Post machine'),"
            . "(169, "
            . " 'post.machine.condition.new', "
            . " 'New'),"
            . "(170, "
            . " 'post.machine.condition.used', "
            . " 'Used'),"

               
            // Companies
            . "(171, "
            . " 'companies.title', "
            . " 'Companies'),"
            . "(172, "
            . " 'companies.search.form.title', "
            . " 'Companies'),"
            . "(173, "
            . " 'companies.breadcrumbs.search', "
            . " 'Search'),"
            . "(174, "
            . " 'companies.board.sort.name', "
            . " 'Name'),"
            . "(175, "
            . " 'companies.board.sort.locations', "
            . " 'Locations'),"
            . "(176, "
            . " 'companies.board.sort.last.update', "
            . " 'Last Update'),"
            . "(177, "
            . " 'companies.under.board.text', "
            . " 'Iron Board offers you a list machine builders and and other major international players in the industry.'),"

               
            // Profile menu
            . "(178, "
            . " 'profile.menu.title', "
            . " 'Profile'),"
            . "(179, "
            . " 'profile.menu.general', "
            . " 'General'),"
            . "(180, "
            . " 'profile.menu.postings', "
            . " 'Postings'),"
            . "(181, "
            . " 'profile.menu.uploads', "
            . " 'Uploads'),"
            . "(182, "
            . " 'profile.menu.credentials', "
            . " 'Email/Password'),"
            . "(183, "
            . " 'profile.menu.post.machine', "
            . " 'Post machine'),"

               
            // Register
            . "(184, "
            . " 'register.title', "
            . " 'Registration'),"
            . "(185, "
            . " 'register.breadcrumbs.register', "
            . " 'Register and post your machine absolutley free!'),"
            . "(186, "
            . " 'register.general.info.title', "
            . " 'General info'),"
            . "(187, "
            . " 'register.general.info', "
            . " '
                    Create an account in a few steps and post your machine absolutely free of charge.
                    <br/><br/>
                    <b>Required:</b>
                    <br/>
                    <ul>
                        <li>
                            Kindly fill in the blanck filds with the relvant information
                        </li>
                        <li>
                            Check your email for the confirmation link
                        </li>
                        <li>
                            Click on the activation link in order to activate your account
                        </li>
                        <li>
                            Feel free to post your machine
                        </li>
                    </ul>
                    <br/>
                    <b>Optional:</b>
                    <ul>
                        <li>
                            Complete your account information for a better visibility on the machine board
                        </li>
                    </ul>
                    <br/>
                    <i>
                        For any questions, comments or observations plase contact one of our representatives directly at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"
            . "(188, "
            . " 'register.form.title', "
            . " 'General'),"
            . "(189, "
            . " 'register.form.company.name', "
            . " 'Company name'),"
            . "(190, "
            . " 'register.form.email', "
            . " 'Email'),"
            . "(191, "
            . " 'register.form.password', "
            . " 'Password'),"
            . "(191, "
            . " 'register.form.repeat.password', "
            . " 'Repeat password'),"
            . "(192, "
            . " 'register.form.button.register', "
            . " 'Register'),"
            . "(193, "
            . " 'register.successfuly.registered.title', "
            . " 'Thank you for submitting the required fields!'),"
            . "(194, "
            . " 'register.successfuly.registered.info', "
            . " '
                    <b>Account activation:</b> 
                    <br/><br/>
                    <ul>
                        <li>
                            Check your email for the confirmation link
                        </li>
                        <li>
                            Click on the activation link in order to activate your account
                        </li>
                        <li>
                            Feel free to post your machine
                        </li>
                    </ul>
                    <br/>
                    <i>
                        For other questions please contact us at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"

                
               
            // Authentication
            . "(195, "
            . " 'authentication.title', "
            . " 'Authentication'),"
            . "(196, "
            . " 'authentication.form.title', "
            . " 'Sign In'),"
            . "(197, "
            . " 'authentication.form.login', "
            . " 'Login/Email'),"
            . "(198, "
            . " 'authentication.form.password', "
            . " 'Password'),"
            . "(199, "
            . " 'authentication.form.link.register', "
            . " 'Register with us!'),"
            . "(200, "
            . " 'authentication.form.link.forgot.password', "
            . " 'Forgot password?'),"
            . "(201, "
            . " 'authentication.form.button.sign.in', "
            . " 'Sign In'),"
            . "(202, "
            . " 'authentication.confirmation.message', "
            . " '
                    Your account was successfully created and confirmed!<br/>
                    Please Sign In to your account to get immediate access to the machine board.
                '),"
            . "(203, "
            . " 'authentication.message.authentication.required', "
            . " 'Only an authenticated user can perform this action!'),"


                
               
            // Recover password
            . "(204, "
            . " 'recover.title', "
            . " 'Password recovery'),"
            . "(205, "
            . " 'recover.note.title', "
            . " 'Note'),"
            . "(206, "
            . " 'recover.note.info', "
            . " '
                    After completing and submitting the required information please check your inbox/spam 
                    folder for the current password.
                    <br/><br/>
                    <i>
                        We kindly suggest that you remove the email containing your personal login details right 
                        after you receive them or to simply change them in your profile menu because this will 
                        increase your account protection.
                        <br/><br/>
                        If you can’t remember your Login details or Password kindly proceed with the Password 
                        recovery process on the right hand side or contact one of our representatives directly at
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"
            . "(207, "
            . " 'recover.form.title', "
            . " 'Recover password'),"
            . "(208, "
            . " 'recover.form.email', "
            . " 'Email/Login'),"
            . "(208, "
            . " 'recover.form.button.recover', "
            . " 'Recover'),"
            . "(209, "
            . " 'recover.check.email.title', "
            . " 'Check your email'),"
            . "(210, "
            . " 'recover.check.email.info', "
            . " '
                    Your password was send to your email.
                    <br/><br/>
                    <i>
                        We sugest you remove the email with your password after you get it, 
                        or visit the profile menu and change it, this will increase 
                        your account protection!
                        <br/><br/>
                        If you can’t remember your email to recover your password please contact us at:
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"


                
               
            // Profile General
            . "(211, "
            . " 'profile.general.info.title', "
            . " 'General account info'),"
            . "(212, "
            . " 'profile.general.info', "
            . " 'Here you can display your company description and contact details to gain better visibility and be contacted by other byers.'),"
            . "(213, "
            . " 'profile.general.public.page.title', "
            . " 'Public page'),"
            . "(214, "
            . " 'profile.general.form.title', "
            . " 'Details'),"
            . "(215, "
            . " 'profile.general.form.name', "
            . " 'Name'),"
            . "(216, "
            . " 'profile.general.form.contact.person', "
            . " 'Contact person'),"
            . "(217, "
            . " 'profile.general.form.phone', "
            . " 'Phone'),"
            . "(218, "
            . " 'profile.general.form.countries', "
            . " 'Countries'),"
            . "(219, "
            . " 'profile.general.form.address', "
            . " 'Address'),"
            . "(220, "
            . " 'profile.general.form.public.email', "
            . " 'Public email'),"
            . "(221, "
            . " 'profile.general.form.website', "
            . " 'Website'),"
            . "(222, "
            . " 'profile.general.form.button.update', "
            . " 'Update'),"
            . "(223, "
            . " 'profile.general.form.description', "
            . " 'Description'),"


                
               
            // Profile Machines
            . "(224, "
            . " 'profile.machine.info.title', "
            . " 'My posts'),"
            . "(225, "
            . " 'profile.machine.info', "
            . " 'Here you will be able to post machines as well as manage your products: activate or deactivate, edit or delete a post.'),"
            . "(226, "
            . " 'profile.machine.button.post.machine', "
            . " 'Post machine'),"
            . "(227, "
            . " 'profile.machine.board.title', "
            . " 'Macchine'),"


                
               
            // Profile Uploads
            . "(228, "
            . " 'profile.uploads.logo.form.title', "
            . " 'Company logo'),"
            . "(229, "
            . " 'profile.uploads.logo.form.button.add', "
            . " '<b class=\"font-28\">Click </b> to add new Logo!'),"
            . "(230, "
            . " 'profile.uploads.logo.form.button.update', "
            . " 'Upldate Logo!'),"
            . "(230, "
            . " 'profile.uploads.logo.info.title', "
            . " 'Note'),"
            . "(231, "
            . " 'profile.uploads.logo.info', "
            . " '
                    Upload your company Logo so that it appears on the board along with 
                    your company details in order to be easily identified by the customers.
                    <br><br>
                    <b>Uploading suggestions:</b>
                    <br>
                    <ul>
                        <li>
                            Accepted file extensions are: JPG, PNG, GIF.
                        </li>
                        <li>
                            Images will be automatically resized to 200x200
                        </li>
                        <li>
                            Maximum file size accepted is 20MB
                        </li>
                        <li>
                            Attempt to upload uncensored content can lead to permanent banning from our website
                        </li>
                    </ul>
                    <br>
                    <i>
                        For other questions please contact us at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"


                
               
            // Profile Credentials
            . "(232, "
            . " 'profile.credentials.info.title', "
            . " 'Authentication settings'),"
            . "(233, "
            . " 'profile.credentials.info', "
            . " 'Here you can manage your account settings: charge your current login and password.'),"
            . "(234, "
            . " 'profile.credentials.change.email.title', "
            . " 'Change email'),"
            . "(235, "
            . " 'profile.credentials.change.password.title', "
            . " 'Change password'),"
            . "(236, "
            . " 'profile.credentials.change.email.current.password', "
            . " 'Current password'),"
            . "(237, "
            . " 'profile.credentials.change.email.new.email', "
            . " 'New email'),"
            . "(238, "
            . " 'profile.credentials.change.password.new.password', "
            . " 'New password'),"
            . "(239, "
            . " 'profile.credentials.change.password.current.password', "
            . " 'Current password'),"
            . "(240, "
            . " 'profile.credentials.button.save', "
            . " 'Save'),"


                
               
            // About Us
            . "(241, "
            . " 'about.us.title', "
            . " 'About Us'),"
            . "(242, "
            . " 'about.us.info', "
            . " '
                    Want to find a machine? Iron BORD is a large platform that offers a wide 
                    specter of new and used machines covering all international manufacturers 
                    and offers direct access to the list of major companies in the field. Here 
                    you will be able to quickly find the needed machine tools and equipment, 
                    either new or used.
                    <br/><br/>
                    It also opens great opportunities for machines that come from auctions, 
                    liquidations and bankruptcies.
                    <br/><br/>
                    Iron Board was created to ease up the search process of the right machine 
                    tool for you as well as assist with the quick selling of your own machines.
                '),"


                
               
            // Services
            . "(243, "
            . " 'services.title', "
            . " 'Services'),"
            . "(244, "
            . " 'services.info', "
            . " '
                    Iron Board platform offers: a broad spectrum of machines, easy access 
                    to the machine tool of your interest, direct access to each registered 
                    company information, visibility on the largest machine tool platform, 
                    opportunity of regular broadcasts, individual potential customer research
                    by our product managers etc.
                '),"
            . "(245, "
            . " 'services.table.public.register', "
            . " 'Register as a company and become visible to all major machine tool buyers'),"
            . "(246, "
            . " 'services.table.public.post', "
            . " 'Find or post machines, manage you posts'),"
            . "(247, "
            . " 'services.table.partner.highlighting', "
            . " 'Your company will appear highlighting and on of any machine search'),"
            . "(248, "
            . " 'services.table.partner.marketing', "
            . " 'Manage and post your machines with the help of our marketing department'),"
            . "(249, "
            . " 'services.table.marketing.highlighted', "
            . " 'Your company and machines will always appear highlighted and on top of any search ans well as all the newsletters'),"
            . "(250, "
            . " 'services.table.marketing.broadcast', "
            . " 'Broadcast your machines and company services to all the companies'),"
            . "(251, "
            . " 'services.table.marketing.shioric', "
            . " 'Finding more buyers for you via external resources www.shioric.com'),"
                
                


                
               
            // Machine View
            . "(252, "
            . " 'view.machine.breadcrumbs.machine', "
            . " 'Machine'),"
            . "(253, "
            . " 'view.machine.title', "
            . " 'Machine'),"
            
            . "(254, "
            . " 'view.machine.name', "
            . " 'Name'),"
            . "(255, "
            . " 'view.machine.status', "
            . " 'Status'),"
            . "(256, "
            . " 'view.machine.type', "
            . " 'Type'),"
            . "(257, "
            . " 'view.machine.url', "
            . " 'Url'),"
            . "(258, "
            . " 'view.machine.price', "
            . " 'Price'),"
            . "(259, "
            . " 'view.machine.description', "
            . " 'Description'),"
            
            . "(160, "
            . " 'view.machine.condition.new', "
            . " 'New'),"
            . "(161, "
            . " 'view.machine.condition.used', "
            . " 'Used'),"
            . "(162, "
            . " 'view.machine.last.update', "
            . " 'Last Update'),"
                
            . "(163, "
            . " 'view.machine.image.total', "
            . " 'Total'),"
            . "(164, "
            . " 'view.machine.image.images', "
            . " 'images'),"
                
            . "(165, "
            . " 'view.machine.company.title', "
            . " 'Company'),"
            . "(166, "
            . " 'view.machine.company.name', "
            . " 'Name'),"
            . "(167, "
            . " 'view.machine.company.person', "
            . " 'Person'),"
            . "(168, "
            . " 'view.machine.company.email', "
            . " 'Email'),"
            . "(169, "
            . " 'view.machine.company.phone', "
            . " 'Phone'),"
            . "(170, "
            . " 'view.machine.company.website', "
            . " 'Website'),"
            . "(171, "
            . " 'view.machine.company.button.open', "
            . " 'Open'),"
                
                


                
               
            // Company View
            . "(272, "
            . " 'view.company.breadcrumbs.companies', "
            . " 'Companies'),"
            . "(273, "
            . " 'view.company.title', "
            . " 'Company'),"
            . "(274, "
            . " 'view.company.name', "
            . " 'Name'),"
            . "(275, "
            . " 'view.company.person', "
            . " 'Person'),"
            . "(276, "
            . " 'view.company.email', "
            . " 'Email'),"
            . "(277, "
            . " 'view.company.phone', "
            . " 'Phone'),"
            . "(278, "
            . " 'view.company.website', "
            . " 'Website'),"
            . "(279, "
            . " 'view.company.description', "
            . " 'Description'),"
            . "(280, "
            . " 'view.company.machine.title', "
            . " 'Machine'),"
            . "(281, "
            . " 'view.company.last.update', "
            . " 'Last Update'),"
                
                


                
               
            // Contact Us
            . "(282, "
            . " 'contacts.title', "
            . " 'Contact Us'),"
            . "(283, "
            . " 'contacts.form.title', "
            . " 'Leave a message'),"
            . "(284, "
            . " 'contacts.form.name', "
            . " 'Name'),"
            . "(285, "
            . " 'contacts.form.email', "
            . " 'Email'),"
            . "(286, "
            . " 'contacts.form.subject', "
            . " 'Subject'),"
            . "(287, "
            . " 'contacts.form.message', "
            . " 'Message'),"
            . "(288, "
            . " 'contacts.form.button.send', "
            . " 'Send'),"
            . "(289, "
            . " 'contacts.info.title', "
            . " 'Contacts'),"







            // Alerts
            . "(290, "
            . " 'alert.confirm.action', "
            . " 'Please reconfirm your action!'),"
            . "(291, "
            . " 'alert.confirm.activate.machine', "
            . " 'Are you sure you want to activate the machine?'),"
            . "(292, "
            . " 'alert.confirm.deactivate.machine', "
            . " 'Are you sure you want to deactivate the machine?'),"
            . "(293, "
            . " 'alert.confirm.button.deactivate', "
            . " 'Deactivate'),"
            . "(294, "
            . " 'alert.confirm.button.activate', "
            . " 'Activate'),"
            . "(294, "
            . " 'alert.confirm.button.remove', "
            . " 'Remove'),"
            . "(295, "
            . " 'alert.confirm.remove.machine', "
            . " 'Are you sure you want to remove the machine?'),"
            . "(296, "
            . " 'alert.confirm.button.cancel', "
            . " 'Cancel'),"
            . "(297, "
            . " 'alert.notification.no.posted.machines', "
            . " 'You don’t have a posted machines'),"







            // Machine Edit
            . "(298, "
            . " 'edit.machine.title', "
            . " 'Update Machine'),"
            . "(299, "
            . " 'edit.machine.breadcrumbs.machine', "
            . " 'Machines'),"
            . "(300, "
            . " 'edit.machine.form.title', "
            . " 'Update machine'),"
            . "(301, "
            . " 'edit.machine.form.machine.name', "
            . " 'Machine name'),"
            . "(302, "
            . " 'edit.machine.form.condition', "
            . " 'Condition'),"
            . "(303, "
            . " 'edit.machine.form.type', "
            . " 'Type'),"
            . "(304, "
            . " 'edit.machine.form.description', "
            . " 'Description'),"
            . "(305, "
            . " 'edit.machine.form.upload.image', "
            . " 'Upload image'),"
            . "(306, "
            . " 'edit.machine.form.click.to.add.new.image', "
            . " '<b class=\"font-28\">Click </b> to add new image!'),"
            . "(307, "
            . " 'edit.machine.form.image.uploaded', "
            . " 'Image succesful uploaded!'),"
            . "(308, "
            . " 'edit.machine.form.more.details', "
            . " '<b>Click</b> to add more details! (optional)'),"
            . "(309, "
            . " 'edit.machine.form.currency', "
            . " 'Currency'),"
            . "(310, "
            . " 'edit.machine.form.url', "
            . " 'Website'),"
            . "(311, "
            . " 'edit.machine.form.price', "
            . " 'Price'),"
            . "(312, "
            . " 'edit.machine.form.button.post.machine', "
            . " 'Update'),"
            . "(313, "
            . " 'edit.machine.condition.new', "
            . " 'New'),"
            . "(314, "
            . " 'edit.machine.condition.used', "
            . " 'Used');");
        
        
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        
        $this->execute("INSERT INTO cms_it "
            . " (cms_id, cms_name, cms_content) "
            . " VALUES "

            // Home
            . "(1, "
            . " 'home.title', "
            . " 'Inserisci o trova una macchina gratuitamente!'),"
            . "(2, "
            . " 'home.title.short.info', "
            . " 'Iron Board offre un ampio spettro di macchine nuove ed usate di quasi tutti i produttori.<br/>Offre anche grandi opportunità per le macchine che provengono da aste, liquidazioni e fallimenti.'),"
            . "(3, "
            . " 'form.field.search', "
            . " 'Ricerca'),"
            . "(6, "
            . " 'home.button.post.machine', "
            . " 'Aggiungi'),"
            . "(10, "
            . " 'home.button.find.machines', "
            . " 'Machine'),"
            . "(12, "
            . " 'home.button.find.companies', "
            . " 'Aziende'),"
            . "(13, "
            . " 'home.button.contact.us', "
            . " 'Contattaci'),"
            . "(14, "
            . " 'home.under.search.text', "
            . " 'In pochi click si sarà in grado di trovare rapidamente le macchine utensili e le attrezzature necessari, nuove o usate.<br/>Basta aprire l’annuncio e contattare il proprietario della macchina direttamente.'),"



            // Header
            . "(4, "
            . " 'header.menu.button.home', "
            . " 'Pagina principale'),"
            . "(5, "
            . " 'header.menu.button.machines', "
            . " 'Macchine'),"
            . "(7, "
            . " 'header.menu.button.companies', "
            . " 'Aziende'),"
            . "(8, "
            . " 'header.menu.button.register', "
            . " 'Registrati'),"
            . "(9, "
            . " 'header.menu.button.sign.in', "
            . " 'Accedi'),"
            . "(122, "
            . " 'header.menu.button.post.machine', "
            . " 'Inserisci una macchina'),"
            . "(123, "
            . " 'header.menu.button.profile', "
            . " 'Profilo'),"
            . "(124, "
            . " 'header.menu.dropdown.general', "
            . " 'Generali'),"
            . "(125, "
            . " 'header.menu.dropdown.postings', "
            . " 'Machine'),"
            . "(126, "
            . " 'header.menu.dropdown.uploads', "
            . " 'Upload'),"
            . "(127, "
            . " 'header.menu.dropdown.sign.out', "
            . " 'Uscita'),"

               
            // Footer
            . "(128, "
            . " 'footer.block.suport.title', "
            . " 'Supporto'),"
            . "(129, "
            . " 'footer.block.suport.contact.us', "
            . " 'Contatti'),"
            . "(130, "
            . " 'footer.block.suport.faq', "
            . " 'Domande frequenti'),"
            . "(132, "
            . " 'footer.block.aboutus.title', "
            . " 'Chi siamo'),"
            . "(133, "
            . " 'footer.block.aboutus.aboutus', "
            . " 'Chi siamo'),"
            . "(134, "
            . " 'footer.block.aboutus.services', "
            . " 'Servizi Correlati'),"
            . "(135, "
            . " 'footer.block.aboutus.terms', "
            . " 'Termini & Condizioni'),"
            . "(136, "
            . " 'footer.block.aboutus.privacy', "
            . " 'Politica sulla riservatezza'),"
            . "(137, "
            . " 'footer.block.promo.title', "
            . " 'Promozioni'),"
            . "(138, "
            . " 'footer.block.promo.banner', "
            . " 'Banner pubblicitari'),"
            . "(139, "
            . " 'footer.block.promo.adds', "
            . " 'Annunci pubblicitari'),"
            . "(140, "
            . " 'footer.block.promo.social', "
            . " 'Promozione sociale'),"

               
            // Machine
            . "(141, "
            . " 'machine.search.form.title', "
            . " 'Riccerca una macchina'),"
            . "(142, "
            . " 'machine.search.form.keywords', "
            . " 'Ricerca libera'),"
            . "(143, "
            . " 'machine.search.form.type', "
            . " 'Tipo'),"
            . "(144, "
            . " 'machine.search.form.condition', "
            . " 'Stato'),"
            . "(145, "
            . " 'machine.search.form.button.search', "
            . " 'Ricerca'),"
            . "(146, "
            . " 'machine.board.sort.name', "
            . " 'Nome'),"
            . "(147, "
            . " 'machine.board.sort.condition', "
            . " 'Stato'),"
            . "(148, "
            . " 'machine.board.sort.type', "
            . " 'Tipo'),"
            . "(149, "
            . " 'machine.board.sort.last.update', "
            . " 'Nuovi arrivi'),"
            . "(150, "
            . " 'machine.under.board.text', "
            . " 'Iron Board è una piattaforma internazionale per le macchine utensili  nuove e usate. Aggiungi facilemente o trova rapidamente una macchina desiderata.'),"
            . "(151, "
            . " 'machine.condition.new', "
            . " 'Nuovo'),"
            . "(152, "
            . " 'machine.condition.used', "
            . " 'Usato'),"
            . "(153, "
            . " 'machine.title', "
            . " 'Macchine'),"

               
            // Post Machine
            . "(154, "
            . " 'post.machine.title', "
            . " 'Inserisci una macchina'),"
            . "(155, "
            . " 'post.machine.breadcrumbs.machine', "
            . " 'Machine'),"
            . "(156, "
            . " 'post.machine.form.title', "
            . " 'Inserisci una macchina'),"
            . "(157, "
            . " 'post.machine.form.machine.name', "
            . " 'Nome della macchina'),"
            . "(158, "
            . " 'post.machine.form.condition', "
            . " 'Stato'),"
            . "(159, "
            . " 'post.machine.form.type', "
            . " 'Tipo'),"
            . "(160, "
            . " 'post.machine.form.description', "
            . " 'Descrizione'),"
            . "(161, "
            . " 'post.machine.form.upload.image', "
            . " 'Carica immagini'),"
            . "(162, "
            . " 'post.machine.form.click.to.add.new.image', "
            . " 'Fai click per aggiungere una nuova immagine!'),"
            . "(163, "
            . " 'post.machine.form.image.uploaded', "
            . " 'Image succesful uploaded!'),"
            . "(164, "
            . " 'post.machine.form.more.details', "
            . " 'Aggiungere ulteriori dettagli!'),"
            . "(165, "
            . " 'post.machine.form.currency', "
            . " 'Moneta'),"
            . "(166, "
            . " 'post.machine.form.url', "
            . " 'Website'),"
            . "(167, "
            . " 'post.machine.form.price', "
            . " 'Prezzo'),"
            . "(168, "
            . " 'post.machine.form.button.post.machine', "
            . " 'Inserisci la macchina'),"
            . "(169, "
            . " 'post.machine.condition.new', "
            . " 'Nuovo'),"
            . "(170, "
            . " 'post.machine.condition.used', "
            . " 'Usato'),"

               
            // Companies
            . "(171, "
            . " 'companies.title', "
            . " 'Aziende'),"
            . "(172, "
            . " 'companies.search.form.title', "
            . " 'Aziende/Rivenditori'),"
            . "(173, "
            . " 'companies.breadcrumbs.search', "
            . " 'Ricerca'),"
            . "(174, "
            . " 'companies.board.sort.name', "
            . " 'Nome'),"
            . "(175, "
            . " 'companies.board.sort.locations', "
            . " 'Sedi'),"
            . "(176, "
            . " 'companies.board.sort.last.update', "
            . " 'Data'),"
            . "(177, "
            . " 'companies.under.board.text', "
            . " 'Iron Board è una piattaforma internazionale per le macchine utensili  nuove e usate.<br/>Aggiungi facilemente o trova rapidamente una macchina desiderata.'),"

               
            // Profile menu
            . "(178, "
            . " 'profile.menu.title', "
            . " 'Profilo'),"
            . "(179, "
            . " 'profile.menu.general', "
            . " 'Generali'),"
            . "(180, "
            . " 'profile.menu.postings', "
            . " 'Macchine inserite'),"
            . "(181, "
            . " 'profile.menu.uploads', "
            . " 'Uploads'),"
            . "(182, "
            . " 'profile.menu.credentials', "
            . " 'Email/Parola'),"
            . "(183, "
            . " 'profile.menu.post.machine', "
            . " 'Inserisci macchina'),"

               
            // Register
            . "(184, "
            . " 'register.title', "
            . " 'Registrazione'),"
            . "(185, "
            . " 'register.breadcrumbs.register', "
            . " 'Registrati e inserisci la tua macchina assolutamente gratis!'),"
            . "(186, "
            . " 'register.general.info.title', "
            . " 'Informazioni generali'),"
            . "(187, "
            . " 'register.general.info', "
            . " '
                    Crea un account in pochi passaggi e inserisci la tu macchina gratuitamente.
                    <br/><br/>
                    <b>Vi richiedeamo:</b>
                    <br/>
                    <ul>
                        <li>
                            Si prega di compilare i campi vuoti con le informazioni richieste.
                        </li>
                        <li>
                            Controlla la tua e-mail per il link di conferma
                        </li>
                        <li>
                            Clicca sul link di attivazione per attivare il tuo account
                        </li>
                        <li>
                            Potete procedere con la pubblicazione della vostra macchina
                        </li>
                    </ul>
                    <br/>
                    <b>Facoltativo:</b>
                    <ul>
                        <li>
                            Vi chiediamo di compilare le informazioni dell’account per una migliore visibilità sul sito
                        </li>
                    </ul>
                    <br/>
                    <i>
                        Per eventuali domande, commenti o osservazioni si prega di contattare uno dei nostri rappresentanti direttamente a 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"
            . "(188, "
            . " 'register.form.title', "
            . " 'Registrati'),"
            . "(189, "
            . " 'register.form.company.name', "
            . " 'Nome della ditta'),"
            . "(190, "
            . " 'register.form.email', "
            . " 'Email'),"
            . "(191, "
            . " 'register.form.password', "
            . " 'Parole'),"
            . "(191, "
            . " 'register.form.repeat.password', "
            . " 'Digitare nuovamente la parole'),"
            . "(192, "
            . " 'register.form.button.register', "
            . " 'Registrati'),"
            . "(193, "
            . " 'register.successfuly.registered.title', "
            . " 'Grazie per aver completato i campi obbligatori!'),"
            . "(194, "
            . " 'register.successfuly.registered.info', "
            . " '
                    <b>Attivazione dell’account:</b> 
                    <br/><br/>
                    <ul>
                        <li>
                            Controlla la tua e-mail per il link di conferma
                        </li>
                        <li>
                            Clicca sul link di attivazione per attivare il tuo account
                        </li>
                        <li>
                            Potete procedere con la pubblicazione della vostra macchina
                        </li>
                    </ul>
                    <br/>
                    <i>
                        Per ulteriori informazioni scriveteci a 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"

                
               
            // Authentication
            . "(195, "
            . " 'authentication.title', "
            . " 'Autenticazione'),"
            . "(196, "
            . " 'authentication.form.title', "
            . " 'Accedi'),"
            . "(197, "
            . " 'authentication.form.login', "
            . " 'Nome utente'),"
            . "(198, "
            . " 'authentication.form.password', "
            . " 'Parole'),"
            . "(199, "
            . " 'authentication.form.link.register', "
            . " 'Registrati con noi!'),"
            . "(200, "
            . " 'authentication.form.link.forgot.password', "
            . " 'Ha dimenticato la parole?'),"
            . "(201, "
            . " 'authentication.form.button.sign.in', "
            . " 'Accedi'),"
            . "(202, "
            . " 'authentication.confirmation.message', "
            . " '
                    Il tuo account è stato creato e confermato con successo!<br/>
                    La preghiamo di accedere al suo account per avere accesso immediato al all’elenco delle macchine.
                '),"
            . "(203, "
            . " 'authentication.message.authentication.required', "
            . " 'Solo un utente autenticato può eseguire questa azione!'),"


                
               
            // Recover password
            . "(204, "
            . " 'recover.title', "
            . " 'Recupero della parola'),"
            . "(205, "
            . " 'recover.note.title', "
            . " 'La pregiamo di notare che'),"
            . "(206, "
            . " 'recover.note.info', "
            . " '
                    Dopo aver completato e inviato le informazioni richieste vi preghiamo di controllare 
                    la vostra Posta in arrivo / Spam per la password corrente.
                    <br/><br/>
                    <i>
                        Vi suggeriamo di gentilmente rimuovere la email contenente dei vostri dati personali 
                        d’accesso immediatamente dopo il ricevimento o di semplicemente cambiarli nel menu del
                        vostro profilo perché cosi si aumenterà la protezione del vostro profilo.
                        <br/><br/>
                        Se non si ricorda i Login o la Password gentilmente procedete con il processo di recupero 
                        della Password sul lato destro o contattate direttamente uno dei nostri rappresentanti a 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"
            . "(207, "
            . " 'recover.form.title', "
            . " 'Recupera la parola'),"
            . "(208, "
            . " 'recover.form.email', "
            . " 'Nome utente'),"
            . "(208, "
            . " 'recover.form.button.recover', "
            . " 'Recupera'),"
            . "(209, "
            . " 'recover.check.email.title', "
            . " 'Controlla la tua email'),"
            . "(210, "
            . " 'recover.check.email.info', "
            . " '
                    La sua password è stata inviato al suo indirizzo email.
                    <br/><br/>
                    <i>
                        Vi consigliamo di rimuovere la email con la password dopo che lo ottenete, o di visitare 
                        il menu del profilo e di cambiarla subito questo aumenterà la protezione del vostro account.
                        <br/><br/>
                        Se non si ricorda la email per il recupero della password contattaci a 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"


                
               
            // Profile General
            . "(211, "
            . " 'profile.general.info.title', "
            . " 'Informazioni generali del account'),"
            . "(212, "
            . " 'profile.general.info', "
            . " 'Qui è possibile esporre i vostri dettagli e contatti aziendali per ottenere una migliore visibilità ed essere contattato da altri rivenditori.'),"
            . "(213, "
            . " 'profile.general.public.page.title', "
            . " 'Pagina pubblica'),"
            . "(214, "
            . " 'profile.general.form.title', "
            . " 'Dati account'),"
            . "(215, "
            . " 'profile.general.form.name', "
            . " 'Nome'),"
            . "(216, "
            . " 'profile.general.form.contact.person', "
            . " 'Contatto'),"
            . "(217, "
            . " 'profile.general.form.phone', "
            . " 'Telephono'),"
            . "(218, "
            . " 'profile.general.form.countries', "
            . " 'Paesi'),"
            . "(219, "
            . " 'profile.general.form.address', "
            . " 'Indrizzo'),"
            . "(220, "
            . " 'profile.general.form.public.email', "
            . " 'Email publico'),"
            . "(221, "
            . " 'profile.general.form.website', "
            . " 'Sito web'),"
            . "(222, "
            . " 'profile.general.form.button.update', "
            . " 'Aggiorna!'),"
            . "(223, "
            . " 'profile.general.form.description', "
            . " 'Descrizione'),"


                
               
            // Profile Machines
            . "(224, "
            . " 'profile.machine.info.title', "
            . " 'Macchine inserite'),"
            . "(225, "
            . " 'profile.machine.info', "
            . " 'Qui puoi inserire le sue macchine ed anche gestire i suoi prodotti: attivare o disattivare, modificare o eliminare un annuncio.'),"
            . "(226, "
            . " 'profile.machine.button.post.machine', "
            . " 'Inserisci una macchina'),"
            . "(227, "
            . " 'profile.machine.board.title', "
            . " 'Macchine'),"


                
               
            // Profile Uploads
            . "(228, "
            . " 'profile.uploads.logo.form.title', "
            . " 'Logo della ditta'),"
            . "(229, "
            . " 'profile.uploads.logo.form.button.add', "
            . " 'Click to add new Logo!'),"
            . "(230, "
            . " 'profile.uploads.logo.form.button.update', "
            . " 'Aggiorna il logo!'),"
            . "(230, "
            . " 'profile.uploads.logo.info.title', "
            . " 'Nota'),"
            . "(231, "
            . " 'profile.uploads.logo.info', "
            . " '
                    Carica il logo aziendale in modo che appaia sul sito insieme 
                    ai vostri dati aziendali per poter essere facilmente identificati dai clienti.
                    <br><br>
                    <b>Suggerimenti per il caricamento:</b>
                    <br>
                    <ul>
                        <li>
                            Estensioni di file accettati sono: JPG, PNG, GIF.
                        </li>
                        <li>
                            Le immagini verranno ridimensionate automaticamente a 200x200
                        </li>
                        <li>
                            La dimensione massima accettata del file è 20MB
                        </li>
                        <li>
                            La tentativa di caricare contenuti senza censure può portare a un vito permanente dal nostro sito
                        </li>
                    </ul>
                    <br>
                    <i>
                        Per altre domande non esitate a contattarci al  
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"


                
               
            // Profile Credentials
            . "(232, "
            . " 'profile.credentials.info.title', "
            . " 'Impostazioni di autenticazione'),"
            . "(233, "
            . " 'profile.credentials.info', "
            . " 'Qui è possibile gestire le impostazioni del vostro account: caricare i vostro login e password correnti.'),"
            . "(234, "
            . " 'profile.credentials.change.email.title', "
            . " 'Cambia email'),"
            . "(235, "
            . " 'profile.credentials.change.password.title', "
            . " 'Cambia parola'),"
            . "(236, "
            . " 'profile.credentials.change.email.current.password', "
            . " 'Parola attuale'),"
            . "(237, "
            . " 'profile.credentials.change.email.new.email', "
            . " 'Nuova email'),"
            . "(238, "
            . " 'profile.credentials.change.password.new.password', "
            . " 'Nuova parole'),"
            . "(239, "
            . " 'profile.credentials.change.password.current.password', "
            . " 'Parola attuale'),"
            . "(240, "
            . " 'profile.credentials.button.save', "
            . " 'Salva'),"


                
               
            // About Us
            . "(241, "
            . " 'about.us.title', "
            . " 'Chi siamo'),"
            . "(242, "
            . " 'about.us.info', "
            . " '
                    Vuoi trovare una macchina? Iron BOARD è una piattaforma molto grande che
                    offre una vasta gamma di macchine nuove ed usate, che copre tutti i produttori 
                    internazionali e offre accesso diretto al elenco delle aziende più importanti
                    nel settore. Qui puoi trovare rapidamente le macchine utensili e le attrezzature 
                    necessarie, sia nuove che usate.
                    <br/><br/>
                    Apre anche grandi opportunità per le macchine che provengono da aste, 
                    liquidazioni e fallimenti.
                    <br/><br/>
                    Iron Board è stato creato per facilitare il processo di ricerca della macchina 
                    giusta per voi e per aiutarvi con la vendita rapida delle proprie macchine.
                '),"


                
               
            // Services
            . "(243, "
            . " 'services.title', "
            . " 'Servizi'),"
            . "(244, "
            . " 'services.info', "
            . " '
                    La piattaforma Iron Board offre: una vasta gamma di macchine, accesso facile 
                    alla macchina utensile del tuo interesse, accesso diretto a informazioni su ogni 
                    società, visibilità sulla più grande piattaforma di macchine utensili, possibilità 
                    di broadcast regolari, ricerca individuale dei potenziali clienti eseguita dai 
                    nostri product managers.
                '),"
            . "(245, "
            . " 'services.table.public.register', "
            . " 'Registrati come azienda e diventa visibile a tutti i principali acquirenti di macchine utensili'),"
            . "(246, "
            . " 'services.table.public.post', "
            . " 'Trova o pubblicizza le tue macchine, gestisci i tuoi annunci'),"
            . "(247, "
            . " 'services.table.partner.highlighting', "
            . " 'La tua azienda verrà evidenziata e visibile su qualsiasi motore di ricerca'),"
            . "(248, "
            . " 'services.table.partner.marketing', "
            . " 'Potrai gestire e inviare le tue macchine con l’aiuto diretto del nostro ufficio marketing'),"
            . "(249, "
            . " 'services.table.marketing.highlighted', "
            . " 'La tua azienda e le tue macchine saranno sempre evidenziati e in cima su qualsiasi motore di ricerca e newsletter'),"
            . "(250, "
            . " 'services.table.marketing.broadcast', "
            . " 'Broadcast le tue macchine e servizi aziendali a tutte le aziende nel industria'),"
            . "(251, "
            . " 'services.table.marketing.shioric', "
            . " 'Troveremo più acquirenti per te tramite risorse esterne www.shioric.com'),"
                


                
               
            // Machine View
            . "(252, "
            . " 'view.machine.breadcrumbs.machine', "
            . " 'Macchine'),"
            . "(253, "
            . " 'view.machine.title', "
            . " 'Macchine'),"
            
            . "(254, "
            . " 'view.machine.name', "
            . " 'Nome'),"
            . "(255, "
            . " 'view.machine.status', "
            . " 'Stato'),"
            . "(256, "
            . " 'view.machine.type', "
            . " 'Tipo'),"
            . "(257, "
            . " 'view.machine.url', "
            . " 'Url'),"
            . "(258, "
            . " 'view.machine.price', "
            . " 'Prezzo'),"
            . "(259, "
            . " 'view.machine.description', "
            . " 'Descrizione'),"
            
            . "(160, "
            . " 'view.machine.condition.new', "
            . " 'Nuovo'),"
            . "(161, "
            . " 'view.machine.condition.used', "
            . " 'Nuovo'),"
            . "(162, "
            . " 'view.machine.last.update', "
            . " 'Ultimo aggiornamento'),"
                
            . "(163, "
            . " 'view.machine.image.total', "
            . " 'Totale'),"
            . "(164, "
            . " 'view.machine.image.images', "
            . " 'immagini'),"
                
            . "(165, "
            . " 'view.machine.company.title', "
            . " 'Azienda'),"
            . "(166, "
            . " 'view.machine.company.name', "
            . " 'Nome'),"
            . "(167, "
            . " 'view.machine.company.person', "
            . " 'Persona'),"
            . "(168, "
            . " 'view.machine.company.email', "
            . " 'Email'),"
            . "(169, "
            . " 'view.machine.company.phone', "
            . " 'Telefono'),"
            . "(170, "
            . " 'view.machine.company.website', "
            . " 'Website'),"
            . "(171, "
            . " 'view.machine.company.button.open', "
            . " 'Aperto'),"
                
                


                
               
            // Company View
            . "(272, "
            . " 'view.company.breadcrumbs.companies', "
            . " 'Aziende'),"
            . "(273, "
            . " 'view.company.title', "
            . " 'Azienda'),"
            . "(274, "
            . " 'view.company.name', "
            . " 'Nome'),"
            . "(275, "
            . " 'view.company.person', "
            . " 'Persona'),"
            . "(276, "
            . " 'view.company.email', "
            . " 'Email'),"
            . "(277, "
            . " 'view.company.phone', "
            . " 'Telefono'),"
            . "(278, "
            . " 'view.company.website', "
            . " 'Website'),"
            . "(279, "
            . " 'view.company.description', "
            . " 'Descrizione'),"
            . "(280, "
            . " 'view.company.machine.title', "
            . " 'Macchine'),"
            . "(281, "
            . " 'view.company.last.update', "
            . " 'Ultimo aggiornamento'),"
                
                


                
               
            // Contact Us
            . "(282, "
            . " 'contacts.title', "
            . " 'Contattaci'),"
            . "(283, "
            . " 'contacts.form.title', "
            . " 'Lasciate un messaggio'),"
            . "(284, "
            . " 'contacts.form.name', "
            . " 'Nome'),"
            . "(285, "
            . " 'contacts.form.email', "
            . " 'Email'),"
            . "(286, "
            . " 'contacts.form.subject', "
            . " 'Soggetto'),"
            . "(287, "
            . " 'contacts.form.message', "
            . " 'Messaggio'),"
            . "(288, "
            . " 'contacts.form.button.send', "
            . " 'Inviare'),"
            . "(289, "
            . " 'contacts.info.title', "
            . " 'Contatti'),"
                
                
                


                
               
            // Alerts
            . "(290, "
            . " 'alert.confirm.action', "
            . " 'Vi pregiamo di riconfermare la vostra azione!'),"
            . "(291, "
            . " 'alert.confirm.activate.machine', "
            . " 'Siete sicuri della attivazione della macchina?'),"
            . "(292, "
            . " 'alert.confirm.deactivate.machine', "
            . " 'Siete sicuri della disattivazione della macchina?'),"
            . "(293, "
            . " 'alert.confirm.button.deactivate', "
            . " 'Disattivare'),"
            . "(294, "
            . " 'alert.confirm.button.activate', "
            . " 'Attivare'),"
            . "(294, "
            . " 'alert.confirm.button.remove', "
            . " 'Rimuovere'),"
            . "(295, "
            . " 'alert.confirm.remove.machine', "
            . " 'Siete sicuri della rimozione della macchina?'),"
            . "(296, "
            . " 'alert.confirm.button.cancel', "
            . " 'Cancellare'),"
            . "(297, "
            . " 'alert.notification.no.posted.machines', "
            . " 'Non si dispone sulla macchina postato'),"

               
            // Post Machine
            . "(298, "
            . " 'edit.machine.title', "
            . " 'Aggiornare'),"
            . "(299, "
            . " 'edit.machine.breadcrumbs.machine', "
            . " 'Macchine'),"
            . "(300, "
            . " 'edit.machine.form.title', "
            . " 'Aggiornare'),"
            . "(301, "
            . " 'edit.machine.form.machine.name', "
            . " 'Nome della macchina'),"
            . "(302, "
            . " 'edit.machine.form.condition', "
            . " 'Stato'),"
            . "(303, "
            . " 'edit.machine.form.type', "
            . " 'Tipo'),"
            . "(304, "
            . " 'edit.machine.form.description', "
            . " 'Descrizione'),"
            . "(305, "
            . " 'edit.machine.form.upload.image', "
            . " 'Carica immagini'),"
            . "(306, "
            . " 'edit.machine.form.click.to.add.new.image', "
            . " 'Fai click per aggiungere una nuova immagine!'),"
            . "(307, "
            . " 'edit.machine.form.image.uploaded', "
            . " 'Image succesful uploaded!'),"
            . "(308, "
            . " 'edit.machine.form.more.details', "
            . " 'Aggiungere ulteriori dettagli!'),"
            . "(309, "
            . " 'edit.machine.form.currency', "
            . " 'Moneta'),"
            . "(310, "
            . " 'edit.machine.form.url', "
            . " 'Website'),"
            . "(311, "
            . " 'edit.machine.form.price', "
            . " 'Prezzo'),"
            . "(312, "
            . " 'edit.machine.form.button.post.machine', "
            . " 'Aggiornare'),"
            . "(313, "
            . " 'edit.machine.condition.new', "
            . " 'Nuovo'),"
            . "(314, "
            . " 'edit.machine.condition.used', "
            . " 'Usato');");
        
        
        
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        
        
        $this->execute("CREATE TABLE cms_de ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " cms_id INT(11), "
                       . " cms_name TEXT DEFAULT '', "
                       . " cms_content TEXT DEFAULT '', "
                       . " PRIMARY KEY (id) );");
        
        $this->execute("CREATE TABLE cms_fr ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " cms_id INT(11), "
                       . " cms_name TEXT DEFAULT '', "
                       . " cms_content TEXT DEFAULT '', "
                       . " PRIMARY KEY (id) );");
        
        
        
        $this->execute("INSERT INTO cms_de "
            . " (cms_id, cms_name, cms_content) "
            . " VALUES "

            // Home
            . "(1, "
            . " 'home.title', "
            . " 'Find and post machines absolutely free!'),"
            . "(2, "
            . " 'home.title.short.info', "
            . " 'Iron BOARD offers a wide spectrum of new and used machines covering almost all manufacturers.<br/>It also opens great opportunities for machines that come from auctions, liquidations and bankruptcies.'),"
            . "(6, "
            . " 'home.button.post.machine', "
            . " 'Post Machine'),"
            . "(10, "
            . " 'home.button.find.machines', "
            . " 'Find machine'),"
            . "(12, "
            . " 'home.button.find.companies', "
            . " 'Find companies'),"
            . "(13, "
            . " 'home.button.contact.us', "
            . " 'Contact us'),"
            . "(14, "
            . " 'home.under.search.text', "
            . " 'In just a few clicks you will be able to quickly find the needed machine tools and equipment, new or used.<br/>Simply open the ad and contact the machine owner directly'),"
            . "(3, "
            . " 'form.field.search', "
            . " 'Search'),"

               
            // Header
            . "(4, "
            . " 'header.menu.button.home', "
            . " 'Home'),"
            . "(5, "
            . " 'header.menu.button.machines', "
            . " 'Machines'),"
            . "(7, "
            . " 'header.menu.button.companies', "
            . " 'Companies'),"
            . "(8, "
            . " 'header.menu.button.register', "
            . " 'Register'),"
            . "(9, "
            . " 'header.menu.button.sign.in', "
            . " 'Sign In'),"
            . "(122, "
            . " 'header.menu.button.post.machine', "
            . " 'Post machine'),"
            . "(123, "
            . " 'header.menu.button.profile', "
            . " 'Profile'),"
            . "(124, "
            . " 'header.menu.dropdown.general', "
            . " 'General'),"
            . "(125, "
            . " 'header.menu.dropdown.postings', "
            . " 'Postings'),"
            . "(126, "
            . " 'header.menu.dropdown.uploads', "
            . " 'Uploads'),"
            . "(127, "
            . " 'header.menu.dropdown.sign.out', "
            . " 'Sign Out'),"

               
            // Footer
            . "(128, "
            . " 'footer.block.suport.title', "
            . " 'Support'),"
            . "(129, "
            . " 'footer.block.suport.contact.us', "
            . " 'Contact Us'),"
            . "(130, "
            . " 'footer.block.suport.faq', "
            . " 'F.A.Q.'),"
            . "(132, "
            . " 'footer.block.aboutus.title', "
            . " 'About Us'),"
            . "(133, "
            . " 'footer.block.aboutus.aboutus', "
            . " 'About Us'),"
            . "(134, "
            . " 'footer.block.aboutus.services', "
            . " 'Services and Pakages'),"
            . "(135, "
            . " 'footer.block.aboutus.terms', "
            . " 'Terms & Conditions'),"
            . "(136, "
            . " 'footer.block.aboutus.privacy', "
            . " 'Privacy policy'),"
            . "(137, "
            . " 'footer.block.promo.title', "
            . " 'Promo'),"
            . "(138, "
            . " 'footer.block.promo.banner', "
            . " 'Banner advertising'),"
            . "(139, "
            . " 'footer.block.promo.adds', "
            . " 'Adds sdvertising'),"
            . "(140, "
            . " 'footer.block.promo.social', "
            . " 'Social Promotion'),"

               
            // Machine
            . "(141, "
            . " 'machine.search.form.title', "
            . " 'Find machine'),"
            . "(142, "
            . " 'machine.search.form.keywords', "
            . " 'Keywords'),"
            . "(143, "
            . " 'machine.search.form.type', "
            . " 'Type'),"
            . "(144, "
            . " 'machine.search.form.condition', "
            . " 'Condition'),"
            . "(145, "
            . " 'machine.search.form.button.search', "
            . " 'Search'),"
            . "(146, "
            . " 'machine.board.sort.name', "
            . " 'Name'),"
            . "(147, "
            . " 'machine.board.sort.condition', "
            . " 'Condition'),"
            . "(148, "
            . " 'machine.board.sort.type', "
            . " 'Type'),"
            . "(149, "
            . " 'machine.board.sort.last.update', "
            . " 'Last Update'),"
            . "(150, "
            . " 'machine.under.board.text', "
            . " 'Iron BOARD is an international platform for new and used machine tools trading. Easily post your own machine or quickly find a desired one.'),"
            . "(151, "
            . " 'machine.condition.new', "
            . " 'New'),"
            . "(152, "
            . " 'machine.condition.used', "
            . " 'Used'),"
            . "(153, "
            . " 'machine.title', "
            . " 'Machine'),"

               
            // Post Machine
            . "(154, "
            . " 'post.machine.title', "
            . " 'Post Machine'),"
            . "(155, "
            . " 'post.machine.breadcrumbs.machine', "
            . " 'Machines'),"
            . "(156, "
            . " 'post.machine.form.title', "
            . " 'Post machine'),"
            . "(157, "
            . " 'post.machine.form.machine.name', "
            . " 'Machine name'),"
            . "(158, "
            . " 'post.machine.form.condition', "
            . " 'Condition'),"
            . "(159, "
            . " 'post.machine.form.type', "
            . " 'Type'),"
            . "(160, "
            . " 'post.machine.form.description', "
            . " 'Description'),"
            . "(161, "
            . " 'post.machine.form.upload.image', "
            . " 'Upload image'),"
            . "(162, "
            . " 'post.machine.form.click.to.add.new.image', "
            . " '<b class=\"font-28\">Click </b> to add new image!'),"
            . "(163, "
            . " 'post.machine.form.image.uploaded', "
            . " 'Image succesful uploaded!'),"
            . "(164, "
            . " 'post.machine.form.more.details', "
            . " '<b>Click</b> to add more details! (optional)'),"
            . "(165, "
            . " 'post.machine.form.currency', "
            . " 'Currency'),"
            . "(166, "
            . " 'post.machine.form.url', "
            . " 'Website'),"
            . "(167, "
            . " 'post.machine.form.price', "
            . " 'Price'),"
            . "(168, "
            . " 'post.machine.form.button.post.machine', "
            . " 'Post machine'),"
            . "(169, "
            . " 'post.machine.condition.new', "
            . " 'New'),"
            . "(170, "
            . " 'post.machine.condition.used', "
            . " 'Used'),"

               
            // Companies
            . "(171, "
            . " 'companies.title', "
            . " 'Companies'),"
            . "(172, "
            . " 'companies.search.form.title', "
            . " 'Companies'),"
            . "(173, "
            . " 'companies.breadcrumbs.search', "
            . " 'Search'),"
            . "(174, "
            . " 'companies.board.sort.name', "
            . " 'Name'),"
            . "(175, "
            . " 'companies.board.sort.locations', "
            . " 'Locations'),"
            . "(176, "
            . " 'companies.board.sort.last.update', "
            . " 'Last Update'),"
            . "(177, "
            . " 'companies.under.board.text', "
            . " 'Iron Board offers you a list machine builders and and other major international players in the industry.'),"

               
            // Profile menu
            . "(178, "
            . " 'profile.menu.title', "
            . " 'Profile'),"
            . "(179, "
            . " 'profile.menu.general', "
            . " 'General'),"
            . "(180, "
            . " 'profile.menu.postings', "
            . " 'Postings'),"
            . "(181, "
            . " 'profile.menu.uploads', "
            . " 'Uploads'),"
            . "(182, "
            . " 'profile.menu.credentials', "
            . " 'Email/Password'),"
            . "(183, "
            . " 'profile.menu.post.machine', "
            . " 'Post machine'),"

               
            // Register
            . "(184, "
            . " 'register.title', "
            . " 'Registration'),"
            . "(185, "
            . " 'register.breadcrumbs.register', "
            . " 'Register and post your machine absolutley free!'),"
            . "(186, "
            . " 'register.general.info.title', "
            . " 'General info'),"
            . "(187, "
            . " 'register.general.info', "
            . " '
                    Create an account in a few steps and post your machine absolutely free of charge.
                    <br/><br/>
                    <b>Required:</b>
                    <br/>
                    <ul>
                        <li>
                            Kindly fill in the blanck filds with the relvant information
                        </li>
                        <li>
                            Check your email for the confirmation link
                        </li>
                        <li>
                            Click on the activation link in order to activate your account
                        </li>
                        <li>
                            Feel free to post your machine
                        </li>
                    </ul>
                    <br/>
                    <b>Optional:</b>
                    <ul>
                        <li>
                            Complete your account information for a better visibility on the machine board
                        </li>
                    </ul>
                    <br/>
                    <i>
                        For any questions, comments or observations plase contact one of our representatives directly at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"
            . "(188, "
            . " 'register.form.title', "
            . " 'General'),"
            . "(189, "
            . " 'register.form.company.name', "
            . " 'Company name'),"
            . "(190, "
            . " 'register.form.email', "
            . " 'Email'),"
            . "(191, "
            . " 'register.form.password', "
            . " 'Password'),"
            . "(191, "
            . " 'register.form.repeat.password', "
            . " 'Repeat password'),"
            . "(192, "
            . " 'register.form.button.register', "
            . " 'Register'),"
            . "(193, "
            . " 'register.successfuly.registered.title', "
            . " 'Thank you for submitting the required fields!'),"
            . "(194, "
            . " 'register.successfuly.registered.info', "
            . " '
                    <b>Account activation:</b> 
                    <br/><br/>
                    <ul>
                        <li>
                            Check your email for the confirmation link
                        </li>
                        <li>
                            Click on the activation link in order to activate your account
                        </li>
                        <li>
                            Feel free to post your machine
                        </li>
                    </ul>
                    <br/>
                    <i>
                        For other questions please contact us at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"

                
               
            // Authentication
            . "(195, "
            . " 'authentication.title', "
            . " 'Authentication'),"
            . "(196, "
            . " 'authentication.form.title', "
            . " 'Sign In'),"
            . "(197, "
            . " 'authentication.form.login', "
            . " 'Login/Email'),"
            . "(198, "
            . " 'authentication.form.password', "
            . " 'Password'),"
            . "(199, "
            . " 'authentication.form.link.register', "
            . " 'Register with us!'),"
            . "(200, "
            . " 'authentication.form.link.forgot.password', "
            . " 'Forgot password?'),"
            . "(201, "
            . " 'authentication.form.button.sign.in', "
            . " 'Sign In'),"
            . "(202, "
            . " 'authentication.confirmation.message', "
            . " '
                    Your account was successfully created and confirmed!<br/>
                    Please Sign In to your account to get immediate access to the machine board.
                '),"
            . "(203, "
            . " 'authentication.message.authentication.required', "
            . " 'Only an authenticated user can perform this action!'),"


                
               
            // Recover password
            . "(204, "
            . " 'recover.title', "
            . " 'Password recovery'),"
            . "(205, "
            . " 'recover.note.title', "
            . " 'Note'),"
            . "(206, "
            . " 'recover.note.info', "
            . " '
                    After completing and submitting the required information please check your inbox/spam 
                    folder for the current password.
                    <br/><br/>
                    <i>
                        We kindly suggest that you remove the email containing your personal login details right 
                        after you receive them or to simply change them in your profile menu because this will 
                        increase your account protection.
                        <br/><br/>
                        If you can’t remember your Login details or Password kindly proceed with the Password 
                        recovery process on the right hand side or contact one of our representatives directly at
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"
            . "(207, "
            . " 'recover.form.title', "
            . " 'Recover password'),"
            . "(208, "
            . " 'recover.form.email', "
            . " 'Email/Login'),"
            . "(208, "
            . " 'recover.form.button.recover', "
            . " 'Recover'),"
            . "(209, "
            . " 'recover.check.email.title', "
            . " 'Check your email'),"
            . "(210, "
            . " 'recover.check.email.info', "
            . " '
                    Your password was send to your email.
                    <br/><br/>
                    <i>
                        We sugest you remove the email with your password after you get it, 
                        or visit the profile menu and change it, this will increase 
                        your account protection!
                        <br/><br/>
                        If you can’t remember your email to recover your password please contact us at:
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"


                
               
            // Profile General
            . "(211, "
            . " 'profile.general.info.title', "
            . " 'General account info'),"
            . "(212, "
            . " 'profile.general.info', "
            . " 'Here you can display your company description and contact details to gain better visibility and be contacted by other byers.'),"
            . "(213, "
            . " 'profile.general.public.page.title', "
            . " 'Public page'),"
            . "(214, "
            . " 'profile.general.form.title', "
            . " 'Details'),"
            . "(215, "
            . " 'profile.general.form.name', "
            . " 'Name'),"
            . "(216, "
            . " 'profile.general.form.contact.person', "
            . " 'Contact person'),"
            . "(217, "
            . " 'profile.general.form.phone', "
            . " 'Phone'),"
            . "(218, "
            . " 'profile.general.form.countries', "
            . " 'Countries'),"
            . "(219, "
            . " 'profile.general.form.address', "
            . " 'Address'),"
            . "(220, "
            . " 'profile.general.form.public.email', "
            . " 'Public email'),"
            . "(221, "
            . " 'profile.general.form.website', "
            . " 'Website'),"
            . "(222, "
            . " 'profile.general.form.button.update', "
            . " 'Update'),"
            . "(223, "
            . " 'profile.general.form.description', "
            . " 'Description'),"


                
               
            // Profile Machines
            . "(224, "
            . " 'profile.machine.info.title', "
            . " 'My posts'),"
            . "(225, "
            . " 'profile.machine.info', "
            . " 'Here you will be able to post machines as well as manage your products: activate or deactivate, edit or delete a post.'),"
            . "(226, "
            . " 'profile.machine.button.post.machine', "
            . " 'Post machine'),"
            . "(227, "
            . " 'profile.machine.board.title', "
            . " 'Macchine'),"


                
               
            // Profile Uploads
            . "(228, "
            . " 'profile.uploads.logo.form.title', "
            . " 'Company logo'),"
            . "(229, "
            . " 'profile.uploads.logo.form.button.add', "
            . " '<b class=\"font-28\">Click </b> to add new Logo!'),"
            . "(230, "
            . " 'profile.uploads.logo.form.button.update', "
            . " 'Upldate Logo!'),"
            . "(230, "
            . " 'profile.uploads.logo.info.title', "
            . " 'Note'),"
            . "(231, "
            . " 'profile.uploads.logo.info', "
            . " '
                    Upload your company Logo so that it appears on the board along with 
                    your company details in order to be easily identified by the customers.
                    <br><br>
                    <b>Uploading suggestions:</b>
                    <br>
                    <ul>
                        <li>
                            Accepted file extensions are: JPG, PNG, GIF.
                        </li>
                        <li>
                            Images will be automatically resized to 200x200
                        </li>
                        <li>
                            Maximum file size accepted is 20MB
                        </li>
                        <li>
                            Attempt to upload uncensored content can lead to permanent banning from our website
                        </li>
                    </ul>
                    <br>
                    <i>
                        For other questions please contact us at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"


                
               
            // Profile Credentials
            . "(232, "
            . " 'profile.credentials.info.title', "
            . " 'Authentication settings'),"
            . "(233, "
            . " 'profile.credentials.info', "
            . " 'Here you can manage your account settings: charge your current login and password.'),"
            . "(234, "
            . " 'profile.credentials.change.email.title', "
            . " 'Change email'),"
            . "(235, "
            . " 'profile.credentials.change.password.title', "
            . " 'Change password'),"
            . "(236, "
            . " 'profile.credentials.change.email.current.password', "
            . " 'Current password'),"
            . "(237, "
            . " 'profile.credentials.change.email.new.email', "
            . " 'New email'),"
            . "(238, "
            . " 'profile.credentials.change.password.new.password', "
            . " 'New password'),"
            . "(239, "
            . " 'profile.credentials.change.password.current.password', "
            . " 'Current password'),"
            . "(240, "
            . " 'profile.credentials.button.save', "
            . " 'Save'),"


                
               
            // About Us
            . "(241, "
            . " 'about.us.title', "
            . " 'About Us'),"
            . "(242, "
            . " 'about.us.info', "
            . " '
                    Want to find a machine? Iron BORD is a large platform that offers a wide 
                    specter of new and used machines covering all international manufacturers 
                    and offers direct access to the list of major companies in the field. Here 
                    you will be able to quickly find the needed machine tools and equipment, 
                    either new or used.
                    <br/><br/>
                    It also opens great opportunities for machines that come from auctions, 
                    liquidations and bankruptcies.
                    <br/><br/>
                    Iron Board was created to ease up the search process of the right machine 
                    tool for you as well as assist with the quick selling of your own machines.
                '),"


                
               
            // Services
            . "(243, "
            . " 'services.title', "
            . " 'Services'),"
            . "(244, "
            . " 'services.info', "
            . " '
                    Iron Board platform offers: a broad spectrum of machines, easy access 
                    to the machine tool of your interest, direct access to each registered 
                    company information, visibility on the largest machine tool platform, 
                    opportunity of regular broadcasts, individual potential customer research
                    by our product managers etc.
                '),"
            . "(245, "
            . " 'services.table.public.register', "
            . " 'Register as a company and become visible to all major machine tool buyers'),"
            . "(246, "
            . " 'services.table.public.post', "
            . " 'Find or post machines, manage you posts'),"
            . "(247, "
            . " 'services.table.partner.highlighting', "
            . " 'Your company will appear highlighting and on of any machine search'),"
            . "(248, "
            . " 'services.table.partner.marketing', "
            . " 'Manage and post your machines with the help of our marketing department'),"
            . "(249, "
            . " 'services.table.marketing.highlighted', "
            . " 'Your company and machines will always appear highlighted and on top of any search ans well as all the newsletters'),"
            . "(250, "
            . " 'services.table.marketing.broadcast', "
            . " 'Broadcast your machines and company services to all the companies'),"
            . "(251, "
            . " 'services.table.marketing.shioric', "
            . " 'Finding more buyers for you via external resources www.shioric.com'),"
                
                


                
               
            // Machine View
            . "(252, "
            . " 'view.machine.breadcrumbs.machine', "
            . " 'Machine'),"
            . "(253, "
            . " 'view.machine.title', "
            . " 'Machine'),"
            
            . "(254, "
            . " 'view.machine.name', "
            . " 'Name'),"
            . "(255, "
            . " 'view.machine.status', "
            . " 'Status'),"
            . "(256, "
            . " 'view.machine.type', "
            . " 'Type'),"
            . "(257, "
            . " 'view.machine.url', "
            . " 'Url'),"
            . "(258, "
            . " 'view.machine.price', "
            . " 'Price'),"
            . "(259, "
            . " 'view.machine.description', "
            . " 'Description'),"
            
            . "(160, "
            . " 'view.machine.condition.new', "
            . " 'New'),"
            . "(161, "
            . " 'view.machine.condition.used', "
            . " 'Used'),"
            . "(162, "
            . " 'view.machine.last.update', "
            . " 'Last Update'),"
                
            . "(163, "
            . " 'view.machine.image.total', "
            . " 'Total'),"
            . "(164, "
            . " 'view.machine.image.images', "
            . " 'images'),"
                
            . "(165, "
            . " 'view.machine.company.title', "
            . " 'Company'),"
            . "(166, "
            . " 'view.machine.company.name', "
            . " 'Name'),"
            . "(167, "
            . " 'view.machine.company.person', "
            . " 'Person'),"
            . "(168, "
            . " 'view.machine.company.email', "
            . " 'Email'),"
            . "(169, "
            . " 'view.machine.company.phone', "
            . " 'Phone'),"
            . "(170, "
            . " 'view.machine.company.website', "
            . " 'Website'),"
            . "(171, "
            . " 'view.machine.company.button.open', "
            . " 'Open'),"
                
                


                
               
            // Company View
            . "(272, "
            . " 'view.company.breadcrumbs.companies', "
            . " 'Companies'),"
            . "(273, "
            . " 'view.company.title', "
            . " 'Company'),"
            . "(274, "
            . " 'view.company.name', "
            . " 'Name'),"
            . "(275, "
            . " 'view.company.person', "
            . " 'Person'),"
            . "(276, "
            . " 'view.company.email', "
            . " 'Email'),"
            . "(277, "
            . " 'view.company.phone', "
            . " 'Phone'),"
            . "(278, "
            . " 'view.company.website', "
            . " 'Website'),"
            . "(279, "
            . " 'view.company.description', "
            . " 'Description'),"
            . "(280, "
            . " 'view.company.machine.title', "
            . " 'Machine'),"
            . "(281, "
            . " 'view.company.last.update', "
            . " 'Last Update'),"
                
                


                
               
            // Contact Us
            . "(282, "
            . " 'contacts.title', "
            . " 'Contact Us'),"
            . "(283, "
            . " 'contacts.form.title', "
            . " 'Leave a message'),"
            . "(284, "
            . " 'contacts.form.name', "
            . " 'Name'),"
            . "(285, "
            . " 'contacts.form.email', "
            . " 'Email'),"
            . "(286, "
            . " 'contacts.form.subject', "
            . " 'Subject'),"
            . "(287, "
            . " 'contacts.form.message', "
            . " 'Message'),"
            . "(288, "
            . " 'contacts.form.button.send', "
            . " 'Send'),"
            . "(289, "
            . " 'contacts.info.title', "
            . " 'Contacts'),"







            // Alerts
            . "(290, "
            . " 'alert.confirm.action', "
            . " 'Please reconfirm your action!'),"
            . "(291, "
            . " 'alert.confirm.activate.machine', "
            . " 'Are you sure you want to activate the machine?'),"
            . "(292, "
            . " 'alert.confirm.deactivate.machine', "
            . " 'Are you sure you want to deactivate the machine?'),"
            . "(293, "
            . " 'alert.confirm.button.deactivate', "
            . " 'Deactivate'),"
            . "(294, "
            . " 'alert.confirm.button.activate', "
            . " 'Activate'),"
            . "(294, "
            . " 'alert.confirm.button.remove', "
            . " 'Remove'),"
            . "(295, "
            . " 'alert.confirm.remove.machine', "
            . " 'Are you sure you want to remove the machine?'),"
            . "(296, "
            . " 'alert.confirm.button.cancel', "
            . " 'Cancel'),"
            . "(297, "
            . " 'alert.notification.no.posted.machines', "
            . " 'You don’t have a posted machines'),"







            // Machine Edit
            . "(298, "
            . " 'edit.machine.title', "
            . " 'Update Machine'),"
            . "(299, "
            . " 'edit.machine.breadcrumbs.machine', "
            . " 'Machines'),"
            . "(300, "
            . " 'edit.machine.form.title', "
            . " 'Update machine'),"
            . "(301, "
            . " 'edit.machine.form.machine.name', "
            . " 'Machine name'),"
            . "(302, "
            . " 'edit.machine.form.condition', "
            . " 'Condition'),"
            . "(303, "
            . " 'edit.machine.form.type', "
            . " 'Type'),"
            . "(304, "
            . " 'edit.machine.form.description', "
            . " 'Description'),"
            . "(305, "
            . " 'edit.machine.form.upload.image', "
            . " 'Upload image'),"
            . "(306, "
            . " 'edit.machine.form.click.to.add.new.image', "
            . " '<b class=\"font-28\">Click </b> to add new image!'),"
            . "(307, "
            . " 'edit.machine.form.image.uploaded', "
            . " 'Image succesful uploaded!'),"
            . "(308, "
            . " 'edit.machine.form.more.details', "
            . " '<b>Click</b> to add more details! (optional)'),"
            . "(309, "
            . " 'edit.machine.form.currency', "
            . " 'Currency'),"
            . "(310, "
            . " 'edit.machine.form.url', "
            . " 'Website'),"
            . "(311, "
            . " 'edit.machine.form.price', "
            . " 'Price'),"
            . "(312, "
            . " 'edit.machine.form.button.post.machine', "
            . " 'Update'),"
            . "(313, "
            . " 'edit.machine.condition.new', "
            . " 'New'),"
            . "(314, "
            . " 'edit.machine.condition.used', "
            . " 'Used');");
        
        
        $this->execute("INSERT INTO cms_fr "
            . " (cms_id, cms_name, cms_content) "
            . " VALUES "

            // Home
            . "(1, "
            . " 'home.title', "
            . " 'Find and post machines absolutely free!'),"
            . "(2, "
            . " 'home.title.short.info', "
            . " 'Iron BOARD offers a wide spectrum of new and used machines covering almost all manufacturers.<br/>It also opens great opportunities for machines that come from auctions, liquidations and bankruptcies.'),"
            . "(6, "
            . " 'home.button.post.machine', "
            . " 'Post Machine'),"
            . "(10, "
            . " 'home.button.find.machines', "
            . " 'Find machine'),"
            . "(12, "
            . " 'home.button.find.companies', "
            . " 'Find companies'),"
            . "(13, "
            . " 'home.button.contact.us', "
            . " 'Contact us'),"
            . "(14, "
            . " 'home.under.search.text', "
            . " 'In just a few clicks you will be able to quickly find the needed machine tools and equipment, new or used.<br/>Simply open the ad and contact the machine owner directly'),"
            . "(3, "
            . " 'form.field.search', "
            . " 'Search'),"

               
            // Header
            . "(4, "
            . " 'header.menu.button.home', "
            . " 'Home'),"
            . "(5, "
            . " 'header.menu.button.machines', "
            . " 'Machines'),"
            . "(7, "
            . " 'header.menu.button.companies', "
            . " 'Companies'),"
            . "(8, "
            . " 'header.menu.button.register', "
            . " 'Register'),"
            . "(9, "
            . " 'header.menu.button.sign.in', "
            . " 'Sign In'),"
            . "(122, "
            . " 'header.menu.button.post.machine', "
            . " 'Post machine'),"
            . "(123, "
            . " 'header.menu.button.profile', "
            . " 'Profile'),"
            . "(124, "
            . " 'header.menu.dropdown.general', "
            . " 'General'),"
            . "(125, "
            . " 'header.menu.dropdown.postings', "
            . " 'Postings'),"
            . "(126, "
            . " 'header.menu.dropdown.uploads', "
            . " 'Uploads'),"
            . "(127, "
            . " 'header.menu.dropdown.sign.out', "
            . " 'Sign Out'),"

               
            // Footer
            . "(128, "
            . " 'footer.block.suport.title', "
            . " 'Support'),"
            . "(129, "
            . " 'footer.block.suport.contact.us', "
            . " 'Contact Us'),"
            . "(130, "
            . " 'footer.block.suport.faq', "
            . " 'F.A.Q.'),"
            . "(132, "
            . " 'footer.block.aboutus.title', "
            . " 'About Us'),"
            . "(133, "
            . " 'footer.block.aboutus.aboutus', "
            . " 'About Us'),"
            . "(134, "
            . " 'footer.block.aboutus.services', "
            . " 'Services and Pakages'),"
            . "(135, "
            . " 'footer.block.aboutus.terms', "
            . " 'Terms & Conditions'),"
            . "(136, "
            . " 'footer.block.aboutus.privacy', "
            . " 'Privacy policy'),"
            . "(137, "
            . " 'footer.block.promo.title', "
            . " 'Promo'),"
            . "(138, "
            . " 'footer.block.promo.banner', "
            . " 'Banner advertising'),"
            . "(139, "
            . " 'footer.block.promo.adds', "
            . " 'Adds sdvertising'),"
            . "(140, "
            . " 'footer.block.promo.social', "
            . " 'Social Promotion'),"

               
            // Machine
            . "(141, "
            . " 'machine.search.form.title', "
            . " 'Find machine'),"
            . "(142, "
            . " 'machine.search.form.keywords', "
            . " 'Keywords'),"
            . "(143, "
            . " 'machine.search.form.type', "
            . " 'Type'),"
            . "(144, "
            . " 'machine.search.form.condition', "
            . " 'Condition'),"
            . "(145, "
            . " 'machine.search.form.button.search', "
            . " 'Search'),"
            . "(146, "
            . " 'machine.board.sort.name', "
            . " 'Name'),"
            . "(147, "
            . " 'machine.board.sort.condition', "
            . " 'Condition'),"
            . "(148, "
            . " 'machine.board.sort.type', "
            . " 'Type'),"
            . "(149, "
            . " 'machine.board.sort.last.update', "
            . " 'Last Update'),"
            . "(150, "
            . " 'machine.under.board.text', "
            . " 'Iron BOARD is an international platform for new and used machine tools trading. Easily post your own machine or quickly find a desired one.'),"
            . "(151, "
            . " 'machine.condition.new', "
            . " 'New'),"
            . "(152, "
            . " 'machine.condition.used', "
            . " 'Used'),"
            . "(153, "
            . " 'machine.title', "
            . " 'Machine'),"

               
            // Post Machine
            . "(154, "
            . " 'post.machine.title', "
            . " 'Post Machine'),"
            . "(155, "
            . " 'post.machine.breadcrumbs.machine', "
            . " 'Machines'),"
            . "(156, "
            . " 'post.machine.form.title', "
            . " 'Post machine'),"
            . "(157, "
            . " 'post.machine.form.machine.name', "
            . " 'Machine name'),"
            . "(158, "
            . " 'post.machine.form.condition', "
            . " 'Condition'),"
            . "(159, "
            . " 'post.machine.form.type', "
            . " 'Type'),"
            . "(160, "
            . " 'post.machine.form.description', "
            . " 'Description'),"
            . "(161, "
            . " 'post.machine.form.upload.image', "
            . " 'Upload image'),"
            . "(162, "
            . " 'post.machine.form.click.to.add.new.image', "
            . " '<b class=\"font-28\">Click </b> to add new image!'),"
            . "(163, "
            . " 'post.machine.form.image.uploaded', "
            . " 'Image succesful uploaded!'),"
            . "(164, "
            . " 'post.machine.form.more.details', "
            . " '<b>Click</b> to add more details! (optional)'),"
            . "(165, "
            . " 'post.machine.form.currency', "
            . " 'Currency'),"
            . "(166, "
            . " 'post.machine.form.url', "
            . " 'Website'),"
            . "(167, "
            . " 'post.machine.form.price', "
            . " 'Price'),"
            . "(168, "
            . " 'post.machine.form.button.post.machine', "
            . " 'Post machine'),"
            . "(169, "
            . " 'post.machine.condition.new', "
            . " 'New'),"
            . "(170, "
            . " 'post.machine.condition.used', "
            . " 'Used'),"

               
            // Companies
            . "(171, "
            . " 'companies.title', "
            . " 'Companies'),"
            . "(172, "
            . " 'companies.search.form.title', "
            . " 'Companies'),"
            . "(173, "
            . " 'companies.breadcrumbs.search', "
            . " 'Search'),"
            . "(174, "
            . " 'companies.board.sort.name', "
            . " 'Name'),"
            . "(175, "
            . " 'companies.board.sort.locations', "
            . " 'Locations'),"
            . "(176, "
            . " 'companies.board.sort.last.update', "
            . " 'Last Update'),"
            . "(177, "
            . " 'companies.under.board.text', "
            . " 'Iron Board offers you a list machine builders and and other major international players in the industry.'),"

               
            // Profile menu
            . "(178, "
            . " 'profile.menu.title', "
            . " 'Profile'),"
            . "(179, "
            . " 'profile.menu.general', "
            . " 'General'),"
            . "(180, "
            . " 'profile.menu.postings', "
            . " 'Postings'),"
            . "(181, "
            . " 'profile.menu.uploads', "
            . " 'Uploads'),"
            . "(182, "
            . " 'profile.menu.credentials', "
            . " 'Email/Password'),"
            . "(183, "
            . " 'profile.menu.post.machine', "
            . " 'Post machine'),"

               
            // Register
            . "(184, "
            . " 'register.title', "
            . " 'Registration'),"
            . "(185, "
            . " 'register.breadcrumbs.register', "
            . " 'Register and post your machine absolutley free!'),"
            . "(186, "
            . " 'register.general.info.title', "
            . " 'General info'),"
            . "(187, "
            . " 'register.general.info', "
            . " '
                    Create an account in a few steps and post your machine absolutely free of charge.
                    <br/><br/>
                    <b>Required:</b>
                    <br/>
                    <ul>
                        <li>
                            Kindly fill in the blanck filds with the relvant information
                        </li>
                        <li>
                            Check your email for the confirmation link
                        </li>
                        <li>
                            Click on the activation link in order to activate your account
                        </li>
                        <li>
                            Feel free to post your machine
                        </li>
                    </ul>
                    <br/>
                    <b>Optional:</b>
                    <ul>
                        <li>
                            Complete your account information for a better visibility on the machine board
                        </li>
                    </ul>
                    <br/>
                    <i>
                        For any questions, comments or observations plase contact one of our representatives directly at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"
            . "(188, "
            . " 'register.form.title', "
            . " 'General'),"
            . "(189, "
            . " 'register.form.company.name', "
            . " 'Company name'),"
            . "(190, "
            . " 'register.form.email', "
            . " 'Email'),"
            . "(191, "
            . " 'register.form.password', "
            . " 'Password'),"
            . "(191, "
            . " 'register.form.repeat.password', "
            . " 'Repeat password'),"
            . "(192, "
            . " 'register.form.button.register', "
            . " 'Register'),"
            . "(193, "
            . " 'register.successfuly.registered.title', "
            . " 'Thank you for submitting the required fields!'),"
            . "(194, "
            . " 'register.successfuly.registered.info', "
            . " '
                    <b>Account activation:</b> 
                    <br/><br/>
                    <ul>
                        <li>
                            Check your email for the confirmation link
                        </li>
                        <li>
                            Click on the activation link in order to activate your account
                        </li>
                        <li>
                            Feel free to post your machine
                        </li>
                    </ul>
                    <br/>
                    <i>
                        For other questions please contact us at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"

                
               
            // Authentication
            . "(195, "
            . " 'authentication.title', "
            . " 'Authentication'),"
            . "(196, "
            . " 'authentication.form.title', "
            . " 'Sign In'),"
            . "(197, "
            . " 'authentication.form.login', "
            . " 'Login/Email'),"
            . "(198, "
            . " 'authentication.form.password', "
            . " 'Password'),"
            . "(199, "
            . " 'authentication.form.link.register', "
            . " 'Register with us!'),"
            . "(200, "
            . " 'authentication.form.link.forgot.password', "
            . " 'Forgot password?'),"
            . "(201, "
            . " 'authentication.form.button.sign.in', "
            . " 'Sign In'),"
            . "(202, "
            . " 'authentication.confirmation.message', "
            . " '
                    Your account was successfully created and confirmed!<br/>
                    Please Sign In to your account to get immediate access to the machine board.
                '),"
            . "(203, "
            . " 'authentication.message.authentication.required', "
            . " 'Only an authenticated user can perform this action!'),"


                
               
            // Recover password
            . "(204, "
            . " 'recover.title', "
            . " 'Password recovery'),"
            . "(205, "
            . " 'recover.note.title', "
            . " 'Note'),"
            . "(206, "
            . " 'recover.note.info', "
            . " '
                    After completing and submitting the required information please check your inbox/spam 
                    folder for the current password.
                    <br/><br/>
                    <i>
                        We kindly suggest that you remove the email containing your personal login details right 
                        after you receive them or to simply change them in your profile menu because this will 
                        increase your account protection.
                        <br/><br/>
                        If you can’t remember your Login details or Password kindly proceed with the Password 
                        recovery process on the right hand side or contact one of our representatives directly at
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"
            . "(207, "
            . " 'recover.form.title', "
            . " 'Recover password'),"
            . "(208, "
            . " 'recover.form.email', "
            . " 'Email/Login'),"
            . "(208, "
            . " 'recover.form.button.recover', "
            . " 'Recover'),"
            . "(209, "
            . " 'recover.check.email.title', "
            . " 'Check your email'),"
            . "(210, "
            . " 'recover.check.email.info', "
            . " '
                    Your password was send to your email.
                    <br/><br/>
                    <i>
                        We sugest you remove the email with your password after you get it, 
                        or visit the profile menu and change it, this will increase 
                        your account protection!
                        <br/><br/>
                        If you can’t remember your email to recover your password please contact us at:
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"


                
               
            // Profile General
            . "(211, "
            . " 'profile.general.info.title', "
            . " 'General account info'),"
            . "(212, "
            . " 'profile.general.info', "
            . " 'Here you can display your company description and contact details to gain better visibility and be contacted by other byers.'),"
            . "(213, "
            . " 'profile.general.public.page.title', "
            . " 'Public page'),"
            . "(214, "
            . " 'profile.general.form.title', "
            . " 'Details'),"
            . "(215, "
            . " 'profile.general.form.name', "
            . " 'Name'),"
            . "(216, "
            . " 'profile.general.form.contact.person', "
            . " 'Contact person'),"
            . "(217, "
            . " 'profile.general.form.phone', "
            . " 'Phone'),"
            . "(218, "
            . " 'profile.general.form.countries', "
            . " 'Countries'),"
            . "(219, "
            . " 'profile.general.form.address', "
            . " 'Address'),"
            . "(220, "
            . " 'profile.general.form.public.email', "
            . " 'Public email'),"
            . "(221, "
            . " 'profile.general.form.website', "
            . " 'Website'),"
            . "(222, "
            . " 'profile.general.form.button.update', "
            . " 'Update'),"
            . "(223, "
            . " 'profile.general.form.description', "
            . " 'Description'),"


                
               
            // Profile Machines
            . "(224, "
            . " 'profile.machine.info.title', "
            . " 'My posts'),"
            . "(225, "
            . " 'profile.machine.info', "
            . " 'Here you will be able to post machines as well as manage your products: activate or deactivate, edit or delete a post.'),"
            . "(226, "
            . " 'profile.machine.button.post.machine', "
            . " 'Post machine'),"
            . "(227, "
            . " 'profile.machine.board.title', "
            . " 'Macchine'),"


                
               
            // Profile Uploads
            . "(228, "
            . " 'profile.uploads.logo.form.title', "
            . " 'Company logo'),"
            . "(229, "
            . " 'profile.uploads.logo.form.button.add', "
            . " '<b class=\"font-28\">Click </b> to add new Logo!'),"
            . "(230, "
            . " 'profile.uploads.logo.form.button.update', "
            . " 'Upldate Logo!'),"
            . "(230, "
            . " 'profile.uploads.logo.info.title', "
            . " 'Note'),"
            . "(231, "
            . " 'profile.uploads.logo.info', "
            . " '
                    Upload your company Logo so that it appears on the board along with 
                    your company details in order to be easily identified by the customers.
                    <br><br>
                    <b>Uploading suggestions:</b>
                    <br>
                    <ul>
                        <li>
                            Accepted file extensions are: JPG, PNG, GIF.
                        </li>
                        <li>
                            Images will be automatically resized to 200x200
                        </li>
                        <li>
                            Maximum file size accepted is 20MB
                        </li>
                        <li>
                            Attempt to upload uncensored content can lead to permanent banning from our website
                        </li>
                    </ul>
                    <br>
                    <i>
                        For other questions please contact us at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"


                
               
            // Profile Credentials
            . "(232, "
            . " 'profile.credentials.info.title', "
            . " 'Authentication settings'),"
            . "(233, "
            . " 'profile.credentials.info', "
            . " 'Here you can manage your account settings: charge your current login and password.'),"
            . "(234, "
            . " 'profile.credentials.change.email.title', "
            . " 'Change email'),"
            . "(235, "
            . " 'profile.credentials.change.password.title', "
            . " 'Change password'),"
            . "(236, "
            . " 'profile.credentials.change.email.current.password', "
            . " 'Current password'),"
            . "(237, "
            . " 'profile.credentials.change.email.new.email', "
            . " 'New email'),"
            . "(238, "
            . " 'profile.credentials.change.password.new.password', "
            . " 'New password'),"
            . "(239, "
            . " 'profile.credentials.change.password.current.password', "
            . " 'Current password'),"
            . "(240, "
            . " 'profile.credentials.button.save', "
            . " 'Save'),"


                
               
            // About Us
            . "(241, "
            . " 'about.us.title', "
            . " 'About Us'),"
            . "(242, "
            . " 'about.us.info', "
            . " '
                    Want to find a machine? Iron BORD is a large platform that offers a wide 
                    specter of new and used machines covering all international manufacturers 
                    and offers direct access to the list of major companies in the field. Here 
                    you will be able to quickly find the needed machine tools and equipment, 
                    either new or used.
                    <br/><br/>
                    It also opens great opportunities for machines that come from auctions, 
                    liquidations and bankruptcies.
                    <br/><br/>
                    Iron Board was created to ease up the search process of the right machine 
                    tool for you as well as assist with the quick selling of your own machines.
                '),"


                
               
            // Services
            . "(243, "
            . " 'services.title', "
            . " 'Services'),"
            . "(244, "
            . " 'services.info', "
            . " '
                    Iron Board platform offers: a broad spectrum of machines, easy access 
                    to the machine tool of your interest, direct access to each registered 
                    company information, visibility on the largest machine tool platform, 
                    opportunity of regular broadcasts, individual potential customer research
                    by our product managers etc.
                '),"
            . "(245, "
            . " 'services.table.public.register', "
            . " 'Register as a company and become visible to all major machine tool buyers'),"
            . "(246, "
            . " 'services.table.public.post', "
            . " 'Find or post machines, manage you posts'),"
            . "(247, "
            . " 'services.table.partner.highlighting', "
            . " 'Your company will appear highlighting and on of any machine search'),"
            . "(248, "
            . " 'services.table.partner.marketing', "
            . " 'Manage and post your machines with the help of our marketing department'),"
            . "(249, "
            . " 'services.table.marketing.highlighted', "
            . " 'Your company and machines will always appear highlighted and on top of any search ans well as all the newsletters'),"
            . "(250, "
            . " 'services.table.marketing.broadcast', "
            . " 'Broadcast your machines and company services to all the companies'),"
            . "(251, "
            . " 'services.table.marketing.shioric', "
            . " 'Finding more buyers for you via external resources www.shioric.com'),"
                
                


                
               
            // Machine View
            . "(252, "
            . " 'view.machine.breadcrumbs.machine', "
            . " 'Machine'),"
            . "(253, "
            . " 'view.machine.title', "
            . " 'Machine'),"
            
            . "(254, "
            . " 'view.machine.name', "
            . " 'Name'),"
            . "(255, "
            . " 'view.machine.status', "
            . " 'Status'),"
            . "(256, "
            . " 'view.machine.type', "
            . " 'Type'),"
            . "(257, "
            . " 'view.machine.url', "
            . " 'Url'),"
            . "(258, "
            . " 'view.machine.price', "
            . " 'Price'),"
            . "(259, "
            . " 'view.machine.description', "
            . " 'Description'),"
            
            . "(160, "
            . " 'view.machine.condition.new', "
            . " 'New'),"
            . "(161, "
            . " 'view.machine.condition.used', "
            . " 'Used'),"
            . "(162, "
            . " 'view.machine.last.update', "
            . " 'Last Update'),"
                
            . "(163, "
            . " 'view.machine.image.total', "
            . " 'Total'),"
            . "(164, "
            . " 'view.machine.image.images', "
            . " 'images'),"
                
            . "(165, "
            . " 'view.machine.company.title', "
            . " 'Company'),"
            . "(166, "
            . " 'view.machine.company.name', "
            . " 'Name'),"
            . "(167, "
            . " 'view.machine.company.person', "
            . " 'Person'),"
            . "(168, "
            . " 'view.machine.company.email', "
            . " 'Email'),"
            . "(169, "
            . " 'view.machine.company.phone', "
            . " 'Phone'),"
            . "(170, "
            . " 'view.machine.company.website', "
            . " 'Website'),"
            . "(171, "
            . " 'view.machine.company.button.open', "
            . " 'Open'),"
                
                


                
               
            // Company View
            . "(272, "
            . " 'view.company.breadcrumbs.companies', "
            . " 'Companies'),"
            . "(273, "
            . " 'view.company.title', "
            . " 'Company'),"
            . "(274, "
            . " 'view.company.name', "
            . " 'Name'),"
            . "(275, "
            . " 'view.company.person', "
            . " 'Person'),"
            . "(276, "
            . " 'view.company.email', "
            . " 'Email'),"
            . "(277, "
            . " 'view.company.phone', "
            . " 'Phone'),"
            . "(278, "
            . " 'view.company.website', "
            . " 'Website'),"
            . "(279, "
            . " 'view.company.description', "
            . " 'Description'),"
            . "(280, "
            . " 'view.company.machine.title', "
            . " 'Machine'),"
            . "(281, "
            . " 'view.company.last.update', "
            . " 'Last Update'),"
                
                


                
               
            // Contact Us
            . "(282, "
            . " 'contacts.title', "
            . " 'Contact Us'),"
            . "(283, "
            . " 'contacts.form.title', "
            . " 'Leave a message'),"
            . "(284, "
            . " 'contacts.form.name', "
            . " 'Name'),"
            . "(285, "
            . " 'contacts.form.email', "
            . " 'Email'),"
            . "(286, "
            . " 'contacts.form.subject', "
            . " 'Subject'),"
            . "(287, "
            . " 'contacts.form.message', "
            . " 'Message'),"
            . "(288, "
            . " 'contacts.form.button.send', "
            . " 'Send'),"
            . "(289, "
            . " 'contacts.info.title', "
            . " 'Contacts'),"







            // Alerts
            . "(290, "
            . " 'alert.confirm.action', "
            . " 'Please reconfirm your action!'),"
            . "(291, "
            . " 'alert.confirm.activate.machine', "
            . " 'Are you sure you want to activate the machine?'),"
            . "(292, "
            . " 'alert.confirm.deactivate.machine', "
            . " 'Are you sure you want to deactivate the machine?'),"
            . "(293, "
            . " 'alert.confirm.button.deactivate', "
            . " 'Deactivate'),"
            . "(294, "
            . " 'alert.confirm.button.activate', "
            . " 'Activate'),"
            . "(294, "
            . " 'alert.confirm.button.remove', "
            . " 'Remove'),"
            . "(295, "
            . " 'alert.confirm.remove.machine', "
            . " 'Are you sure you want to remove the machine?'),"
            . "(296, "
            . " 'alert.confirm.button.cancel', "
            . " 'Cancel'),"
            . "(297, "
            . " 'alert.notification.no.posted.machines', "
            . " 'You don’t have a posted machines'),"







            // Machine Edit
            . "(298, "
            . " 'edit.machine.title', "
            . " 'Update Machine'),"
            . "(299, "
            . " 'edit.machine.breadcrumbs.machine', "
            . " 'Machines'),"
            . "(300, "
            . " 'edit.machine.form.title', "
            . " 'Update machine'),"
            . "(301, "
            . " 'edit.machine.form.machine.name', "
            . " 'Machine name'),"
            . "(302, "
            . " 'edit.machine.form.condition', "
            . " 'Condition'),"
            . "(303, "
            . " 'edit.machine.form.type', "
            . " 'Type'),"
            . "(304, "
            . " 'edit.machine.form.description', "
            . " 'Description'),"
            . "(305, "
            . " 'edit.machine.form.upload.image', "
            . " 'Upload image'),"
            . "(306, "
            . " 'edit.machine.form.click.to.add.new.image', "
            . " '<b class=\"font-28\">Click </b> to add new image!'),"
            . "(307, "
            . " 'edit.machine.form.image.uploaded', "
            . " 'Image succesful uploaded!'),"
            . "(308, "
            . " 'edit.machine.form.more.details', "
            . " '<b>Click</b> to add more details! (optional)'),"
            . "(309, "
            . " 'edit.machine.form.currency', "
            . " 'Currency'),"
            . "(310, "
            . " 'edit.machine.form.url', "
            . " 'Website'),"
            . "(311, "
            . " 'edit.machine.form.price', "
            . " 'Price'),"
            . "(312, "
            . " 'edit.machine.form.button.post.machine', "
            . " 'Update'),"
            . "(313, "
            . " 'edit.machine.condition.new', "
            . " 'New'),"
            . "(314, "
            . " 'edit.machine.condition.used', "
            . " 'Used');");
        
        
        
        
        
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE cms_de;");
        $this->execute("DROP TABLE cms_fr;");
    }
}
