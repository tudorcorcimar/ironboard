<?php

use Phinx\Migration\AbstractMigration;

class CreateBroadcastsTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE broadcasts ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " status VARCHAR(50) DEFAULT 'created', "
                       . " company_id INT(11) NOT NULL , "
                       . " description TEXT NOT NULL DEFAULT '', "
                       . " created_at DATE, "
                       . " updated_at DATE, "
                       . " deadline DATE, "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE broadcasts;");
    }
}
