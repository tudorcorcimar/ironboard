<?php

use Phinx\Migration\AbstractMigration;

class AddSubscribedColumnToUsers extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        
        $this->execute("ALTER TABLE users "
            . " ADD subscribed BOOLEAN NOT NULL DEFAULT TRUE ;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
