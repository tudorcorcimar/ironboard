<?php

use Phinx\Migration\AbstractMigration;

class CreateCmsRuTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        
        $this->execute("CREATE TABLE cms_ru ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " cms_id INT(11), "
                       . " cms_name TEXT DEFAULT '', "
                       . " cms_content TEXT DEFAULT '', "
                       . " PRIMARY KEY (id) );");
        
        $this->execute("INSERT INTO cms_ru "
            . " (cms_id, cms_name, cms_content) "
            . " VALUES "

            // Home
            . "(1, "
            . " 'home.title', "
            . " 'Find and post machines absolutely free!'),"
            . "(2, "
            . " 'home.title.short.info', "
            . " 'Iron BOARD offers a wide spectrum of new and used machines covering almost all manufacturers.<br/>It also opens great opportunities for machines that come from auctions, liquidations and bankruptcies.'),"
            . "(6, "
            . " 'home.button.post.machine', "
            . " 'Post Machine'),"
            . "(10, "
            . " 'home.button.find.machines', "
            . " 'Find machine'),"
            . "(12, "
            . " 'home.button.find.companies', "
            . " 'Find companies'),"
            . "(13, "
            . " 'home.button.contact.us', "
            . " 'Contact us'),"
            . "(14, "
            . " 'home.under.search.text', "
            . " 'In just a few clicks you will be able to quickly find the needed machine tools and equipment, new or used.<br/>Simply open the ad and contact the machine owner directly'),"
            . "(3, "
            . " 'form.field.search', "
            . " 'Search'),"

               
            // Header
            . "(4, "
            . " 'header.menu.button.home', "
            . " 'Home'),"
            . "(5, "
            . " 'header.menu.button.machines', "
            . " 'Machines'),"
            . "(7, "
            . " 'header.menu.button.companies', "
            . " 'Companies'),"
            . "(8, "
            . " 'header.menu.button.register', "
            . " 'Register'),"
            . "(9, "
            . " 'header.menu.button.sign.in', "
            . " 'Sign In'),"
            . "(122, "
            . " 'header.menu.button.post.machine', "
            . " 'Post machine'),"
            . "(123, "
            . " 'header.menu.button.profile', "
            . " 'Profile'),"
            . "(124, "
            . " 'header.menu.dropdown.general', "
            . " 'General'),"
            . "(125, "
            . " 'header.menu.dropdown.postings', "
            . " 'Postings'),"
            . "(126, "
            . " 'header.menu.dropdown.uploads', "
            . " 'Uploads'),"
            . "(127, "
            . " 'header.menu.dropdown.sign.out', "
            . " 'Sign Out'),"

               
            // Footer
            . "(128, "
            . " 'footer.block.suport.title', "
            . " 'Support'),"
            . "(129, "
            . " 'footer.block.suport.contact.us', "
            . " 'Contact Us'),"
            . "(130, "
            . " 'footer.block.suport.faq', "
            . " 'F.A.Q.'),"
            . "(132, "
            . " 'footer.block.aboutus.title', "
            . " 'About Us'),"
            . "(133, "
            . " 'footer.block.aboutus.aboutus', "
            . " 'About Us'),"
            . "(134, "
            . " 'footer.block.aboutus.services', "
            . " 'Services and Pakages'),"
            . "(135, "
            . " 'footer.block.aboutus.terms', "
            . " 'Terms & Conditions'),"
            . "(136, "
            . " 'footer.block.aboutus.privacy', "
            . " 'Privacy policy'),"
            . "(137, "
            . " 'footer.block.promo.title', "
            . " 'Promo'),"
            . "(138, "
            . " 'footer.block.promo.banner', "
            . " 'Banner advertising'),"
            . "(139, "
            . " 'footer.block.promo.adds', "
            . " 'Adds sdvertising'),"
            . "(140, "
            . " 'footer.block.promo.social', "
            . " 'Social Promotion'),"

               
            // Machine
            . "(141, "
            . " 'machine.search.form.title', "
            . " 'Find machine'),"
            . "(142, "
            . " 'machine.search.form.keywords', "
            . " 'Keywords'),"
            . "(143, "
            . " 'machine.search.form.type', "
            . " 'Type'),"
            . "(144, "
            . " 'machine.search.form.condition', "
            . " 'Condition'),"
            . "(145, "
            . " 'machine.search.form.button.search', "
            . " 'Search'),"
            . "(146, "
            . " 'machine.board.sort.name', "
            . " 'Name'),"
            . "(147, "
            . " 'machine.board.sort.condition', "
            . " 'Condition'),"
            . "(148, "
            . " 'machine.board.sort.type', "
            . " 'Type'),"
            . "(149, "
            . " 'machine.board.sort.last.update', "
            . " 'Last Update'),"
            . "(150, "
            . " 'machine.under.board.text', "
            . " 'Iron BOARD is an international platform for new and used machine tools trading. Easily post your own machine or quickly find a desired one.'),"
            . "(151, "
            . " 'machine.condition.new', "
            . " 'New'),"
            . "(152, "
            . " 'machine.condition.used', "
            . " 'Used'),"
            . "(153, "
            . " 'machine.title', "
            . " 'Machine'),"

               
            // Post Machine
            . "(154, "
            . " 'post.machine.title', "
            . " 'Post Machine'),"
            . "(155, "
            . " 'post.machine.breadcrumbs.machine', "
            . " 'Machines'),"
            . "(156, "
            . " 'post.machine.form.title', "
            . " 'Post machine'),"
            . "(157, "
            . " 'post.machine.form.machine.name', "
            . " 'Machine name'),"
            . "(158, "
            . " 'post.machine.form.condition', "
            . " 'Condition'),"
            . "(159, "
            . " 'post.machine.form.type', "
            . " 'Type'),"
            . "(160, "
            . " 'post.machine.form.description', "
            . " 'Description'),"
            . "(161, "
            . " 'post.machine.form.upload.image', "
            . " 'Upload image'),"
            . "(162, "
            . " 'post.machine.form.click.to.add.new.image', "
            . " '<b class=\"font-28\">Click </b> to add new image!'),"
            . "(163, "
            . " 'post.machine.form.image.uploaded', "
            . " 'Image succesful uploaded!'),"
            . "(164, "
            . " 'post.machine.form.more.details', "
            . " '<b>Click</b> to add more details! (optional)'),"
            . "(165, "
            . " 'post.machine.form.currency', "
            . " 'Currency'),"
            . "(166, "
            . " 'post.machine.form.url', "
            . " 'Website'),"
            . "(167, "
            . " 'post.machine.form.price', "
            . " 'Price'),"
            . "(168, "
            . " 'post.machine.form.button.post.machine', "
            . " 'Post machine'),"
            . "(169, "
            . " 'post.machine.condition.new', "
            . " 'New'),"
            . "(170, "
            . " 'post.machine.condition.used', "
            . " 'Used'),"

               
            // Companies
            . "(171, "
            . " 'companies.title', "
            . " 'Companies'),"
            . "(172, "
            . " 'companies.search.form.title', "
            . " 'Companies'),"
            . "(173, "
            . " 'companies.breadcrumbs.search', "
            . " 'Search'),"
            . "(174, "
            . " 'companies.board.sort.name', "
            . " 'Name'),"
            . "(175, "
            . " 'companies.board.sort.locations', "
            . " 'Locations'),"
            . "(176, "
            . " 'companies.board.sort.last.update', "
            . " 'Last Update'),"
            . "(177, "
            . " 'companies.under.board.text', "
            . " 'Iron Board offers you a list machine builders and and other major international players in the industry.'),"

               
            // Profile menu
            . "(178, "
            . " 'profile.menu.title', "
            . " 'Profile'),"
            . "(179, "
            . " 'profile.menu.general', "
            . " 'General'),"
            . "(180, "
            . " 'profile.menu.postings', "
            . " 'Postings'),"
            . "(181, "
            . " 'profile.menu.uploads', "
            . " 'Uploads'),"
            . "(182, "
            . " 'profile.menu.credentials', "
            . " 'Email/Password'),"
            . "(183, "
            . " 'profile.menu.post.machine', "
            . " 'Post machine'),"

               
            // Register
            . "(184, "
            . " 'register.title', "
            . " 'Registration'),"
            . "(185, "
            . " 'register.breadcrumbs.register', "
            . " 'Register and post your machine absolutley free!'),"
            . "(186, "
            . " 'register.general.info.title', "
            . " 'General info'),"
            . "(187, "
            . " 'register.general.info', "
            . " '
                    Create an account in a few steps and post your machine absolutely free of charge.
                    <br/><br/>
                    <b>Required:</b>
                    <br/>
                    <ul>
                        <li>
                            Kindly fill in the blanck filds with the relvant information
                        </li>
                        <li>
                            Check your email for the confirmation link
                        </li>
                        <li>
                            Click on the activation link in order to activate your account
                        </li>
                        <li>
                            Feel free to post your machine
                        </li>
                    </ul>
                    <br/>
                    <b>Optional:</b>
                    <ul>
                        <li>
                            Complete your account information for a better visibility on the machine board
                        </li>
                    </ul>
                    <br/>
                    <i>
                        For any questions, comments or observations plase contact one of our representatives directly at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"
            . "(188, "
            . " 'register.form.title', "
            . " 'General'),"
            . "(189, "
            . " 'register.form.company.name', "
            . " 'Company name'),"
            . "(190, "
            . " 'register.form.email', "
            . " 'Email'),"
            . "(191, "
            . " 'register.form.password', "
            . " 'Password'),"
            . "(191, "
            . " 'register.form.repeat.password', "
            . " 'Repeat password'),"
            . "(192, "
            . " 'register.form.button.register', "
            . " 'Register'),"
            . "(193, "
            . " 'register.successfuly.registered.title', "
            . " 'Thank you for submitting the required fields!'),"
            . "(194, "
            . " 'register.successfuly.registered.info', "
            . " '
                    <b>Account activation:</b> 
                    <br/><br/>
                    <ul>
                        <li>
                            Check your email for the confirmation link
                        </li>
                        <li>
                            Click on the activation link in order to activate your account
                        </li>
                        <li>
                            Feel free to post your machine
                        </li>
                    </ul>
                    <br/>
                    <i>
                        For other questions please contact us at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"

                
               
            // Authentication
            . "(195, "
            . " 'authentication.title', "
            . " 'Authentication'),"
            . "(196, "
            . " 'authentication.form.title', "
            . " 'Sign In'),"
            . "(197, "
            . " 'authentication.form.login', "
            . " 'Login/Email'),"
            . "(198, "
            . " 'authentication.form.password', "
            . " 'Password'),"
            . "(199, "
            . " 'authentication.form.link.register', "
            . " 'Register with us!'),"
            . "(200, "
            . " 'authentication.form.link.forgot.password', "
            . " 'Forgot password?'),"
            . "(201, "
            . " 'authentication.form.button.sign.in', "
            . " 'Sign In'),"
            . "(202, "
            . " 'authentication.confirmation.message', "
            . " '
                    Your account was successfully created and confirmed!<br/>
                    Please Sign In to your account to get immediate access to the machine board.
                '),"
            . "(203, "
            . " 'authentication.message.authentication.required', "
            . " 'Only an authenticated user can perform this action!'),"


                
               
            // Recover password
            . "(204, "
            . " 'recover.title', "
            . " 'Password recovery'),"
            . "(205, "
            . " 'recover.note.title', "
            . " 'Note'),"
            . "(206, "
            . " 'recover.note.info', "
            . " '
                    After completing and submitting the required information please check your inbox/spam 
                    folder for the current password.
                    <br/><br/>
                    <i>
                        We kindly suggest that you remove the email containing your personal login details right 
                        after you receive them or to simply change them in your profile menu because this will 
                        increase your account protection.
                        <br/><br/>
                        If you can’t remember your Login details or Password kindly proceed with the Password 
                        recovery process on the right hand side or contact one of our representatives directly at
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"
            . "(207, "
            . " 'recover.form.title', "
            . " 'Recover password'),"
            . "(208, "
            . " 'recover.form.email', "
            . " 'Email/Login'),"
            . "(208, "
            . " 'recover.form.button.recover', "
            . " 'Recover'),"
            . "(209, "
            . " 'recover.check.email.title', "
            . " 'Check your email'),"
            . "(210, "
            . " 'recover.check.email.info', "
            . " '
                    Your password was send to your email.
                    <br/><br/>
                    <i>
                        We sugest you remove the email with your password after you get it, 
                        or visit the profile menu and change it, this will increase 
                        your account protection!
                        <br/><br/>
                        If you can’t remember your email to recover your password please contact us at:
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"


                
               
            // Profile General
            . "(211, "
            . " 'profile.general.info.title', "
            . " 'General account info'),"
            . "(212, "
            . " 'profile.general.info', "
            . " 'Here you can display your company description and contact details to gain better visibility and be contacted by other byers.'),"
            . "(213, "
            . " 'profile.general.public.page.title', "
            . " 'Public page'),"
            . "(214, "
            . " 'profile.general.form.title', "
            . " 'Details'),"
            . "(215, "
            . " 'profile.general.form.name', "
            . " 'Name'),"
            . "(216, "
            . " 'profile.general.form.contact.person', "
            . " 'Contact person'),"
            . "(217, "
            . " 'profile.general.form.phone', "
            . " 'Phone'),"
            . "(218, "
            . " 'profile.general.form.countries', "
            . " 'Countries'),"
            . "(219, "
            . " 'profile.general.form.address', "
            . " 'Address'),"
            . "(220, "
            . " 'profile.general.form.public.email', "
            . " 'Public email'),"
            . "(221, "
            . " 'profile.general.form.website', "
            . " 'Website'),"
            . "(222, "
            . " 'profile.general.form.button.update', "
            . " 'Update'),"
            . "(223, "
            . " 'profile.general.form.description', "
            . " 'Description'),"


                
               
            // Profile Machines
            . "(224, "
            . " 'profile.machine.info.title', "
            . " 'My posts'),"
            . "(225, "
            . " 'profile.machine.info', "
            . " 'Here you will be able to post machines as well as manage your products: activate or deactivate, edit or delete a post.'),"
            . "(226, "
            . " 'profile.machine.button.post.machine', "
            . " 'Post machine'),"
            . "(227, "
            . " 'profile.machine.board.title', "
            . " 'Macchine'),"


                
               
            // Profile Uploads
            . "(228, "
            . " 'profile.uploads.logo.form.title', "
            . " 'Company logo'),"
            . "(229, "
            . " 'profile.uploads.logo.form.button.add', "
            . " '<b class=\"font-28\">Click </b> to add new Logo!'),"
            . "(230, "
            . " 'profile.uploads.logo.form.button.update', "
            . " 'Upldate Logo!'),"
            . "(230, "
            . " 'profile.uploads.logo.info.title', "
            . " 'Note'),"
            . "(231, "
            . " 'profile.uploads.logo.info', "
            . " '
                    Upload your company Logo so that it appears on the board along with 
                    your company details in order to be easily identified by the customers.
                    <br><br>
                    <b>Uploading suggestions:</b>
                    <br>
                    <ul>
                        <li>
                            Accepted file extensions are: JPG, PNG, GIF.
                        </li>
                        <li>
                            Images will be automatically resized to 200x200
                        </li>
                        <li>
                            Maximum file size accepted is 20MB
                        </li>
                        <li>
                            Attempt to upload uncensored content can lead to permanent banning from our website
                        </li>
                    </ul>
                    <br>
                    <i>
                        For other questions please contact us at 
                        <a href=\"mailto:info@ibmachine.com\">
                            info@ibmachine.com
                        </a>
                    </i>
                '),"


                
               
            // Profile Credentials
            . "(232, "
            . " 'profile.credentials.info.title', "
            . " 'Authentication settings'),"
            . "(233, "
            . " 'profile.credentials.info', "
            . " 'Here you can manage your account settings: charge your current login and password.'),"
            . "(234, "
            . " 'profile.credentials.change.email.title', "
            . " 'Change email'),"
            . "(235, "
            . " 'profile.credentials.change.password.title', "
            . " 'Change password'),"
            . "(236, "
            . " 'profile.credentials.change.email.current.password', "
            . " 'Current password'),"
            . "(237, "
            . " 'profile.credentials.change.email.new.email', "
            . " 'New email'),"
            . "(238, "
            . " 'profile.credentials.change.password.new.password', "
            . " 'New password'),"
            . "(239, "
            . " 'profile.credentials.change.password.current.password', "
            . " 'Current password'),"
            . "(240, "
            . " 'profile.credentials.button.save', "
            . " 'Save'),"


                
               
            // About Us
            . "(241, "
            . " 'about.us.title', "
            . " 'About Us'),"
            . "(242, "
            . " 'about.us.info', "
            . " '
                    Want to find a machine? Iron BORD is a large platform that offers a wide 
                    specter of new and used machines covering all international manufacturers 
                    and offers direct access to the list of major companies in the field. Here 
                    you will be able to quickly find the needed machine tools and equipment, 
                    either new or used.
                    <br/><br/>
                    It also opens great opportunities for machines that come from auctions, 
                    liquidations and bankruptcies.
                    <br/><br/>
                    Iron Board was created to ease up the search process of the right machine 
                    tool for you as well as assist with the quick selling of your own machines.
                '),"


                
               
            // Services
            . "(243, "
            . " 'services.title', "
            . " 'Services'),"
            . "(244, "
            . " 'services.info', "
            . " '
                    Iron Board platform offers: a broad spectrum of machines, easy access 
                    to the machine tool of your interest, direct access to each registered 
                    company information, visibility on the largest machine tool platform, 
                    opportunity of regular broadcasts, individual potential customer research
                    by our product managers etc.
                '),"
            . "(245, "
            . " 'services.table.public.register', "
            . " 'Register as a company and become visible to all major machine tool buyers'),"
            . "(246, "
            . " 'services.table.public.post', "
            . " 'Find or post machines, manage you posts'),"
            . "(247, "
            . " 'services.table.partner.highlighting', "
            . " 'Your company will appear highlighting and on of any machine search'),"
            . "(248, "
            . " 'services.table.partner.marketing', "
            . " 'Manage and post your machines with the help of our marketing department'),"
            . "(249, "
            . " 'services.table.marketing.highlighted', "
            . " 'Your company and machines will always appear highlighted and on top of any search ans well as all the newsletters'),"
            . "(250, "
            . " 'services.table.marketing.broadcast', "
            . " 'Broadcast your machines and company services to all the companies'),"
            . "(251, "
            . " 'services.table.marketing.shioric', "
            . " 'Finding more buyers for you via external resources www.shioric.com'),"
                
                


                
               
            // Machine View
            . "(252, "
            . " 'view.machine.breadcrumbs.machine', "
            . " 'Machine'),"
            . "(253, "
            . " 'view.machine.title', "
            . " 'Machine'),"
            
            . "(254, "
            . " 'view.machine.name', "
            . " 'Name'),"
            . "(255, "
            . " 'view.machine.status', "
            . " 'Status'),"
            . "(256, "
            . " 'view.machine.type', "
            . " 'Type'),"
            . "(257, "
            . " 'view.machine.url', "
            . " 'Url'),"
            . "(258, "
            . " 'view.machine.price', "
            . " 'Price'),"
            . "(259, "
            . " 'view.machine.description', "
            . " 'Description'),"
            
            . "(160, "
            . " 'view.machine.condition.new', "
            . " 'New'),"
            . "(161, "
            . " 'view.machine.condition.used', "
            . " 'Used'),"
            . "(162, "
            . " 'view.machine.last.update', "
            . " 'Last Update'),"
                
            . "(163, "
            . " 'view.machine.image.total', "
            . " 'Total'),"
            . "(164, "
            . " 'view.machine.image.images', "
            . " 'images'),"
                
            . "(165, "
            . " 'view.machine.company.title', "
            . " 'Company'),"
            . "(166, "
            . " 'view.machine.company.name', "
            . " 'Name'),"
            . "(167, "
            . " 'view.machine.company.person', "
            . " 'Person'),"
            . "(168, "
            . " 'view.machine.company.email', "
            . " 'Email'),"
            . "(169, "
            . " 'view.machine.company.phone', "
            . " 'Phone'),"
            . "(170, "
            . " 'view.machine.company.website', "
            . " 'Website'),"
            . "(171, "
            . " 'view.machine.company.button.open', "
            . " 'Open'),"
                
                


                
               
            // Company View
            . "(272, "
            . " 'view.company.breadcrumbs.companies', "
            . " 'Companies'),"
            . "(273, "
            . " 'view.company.title', "
            . " 'Company'),"
            . "(274, "
            . " 'view.company.name', "
            . " 'Name'),"
            . "(275, "
            . " 'view.company.person', "
            . " 'Person'),"
            . "(276, "
            . " 'view.company.email', "
            . " 'Email'),"
            . "(277, "
            . " 'view.company.phone', "
            . " 'Phone'),"
            . "(278, "
            . " 'view.company.website', "
            . " 'Website'),"
            . "(279, "
            . " 'view.company.description', "
            . " 'Description'),"
            . "(280, "
            . " 'view.company.machine.title', "
            . " 'Machine'),"
            . "(281, "
            . " 'view.company.last.update', "
            . " 'Last Update'),"
                
                


                
               
            // Contact Us
            . "(282, "
            . " 'contacts.title', "
            . " 'Contact Us'),"
            . "(283, "
            . " 'contacts.form.title', "
            . " 'Leave a message'),"
            . "(284, "
            . " 'contacts.form.name', "
            . " 'Name'),"
            . "(285, "
            . " 'contacts.form.email', "
            . " 'Email'),"
            . "(286, "
            . " 'contacts.form.subject', "
            . " 'Subject'),"
            . "(287, "
            . " 'contacts.form.message', "
            . " 'Message'),"
            . "(288, "
            . " 'contacts.form.button.send', "
            . " 'Send'),"
            . "(289, "
            . " 'contacts.info.title', "
            . " 'Contacts'),"







            // Alerts
            . "(290, "
            . " 'alert.confirm.action', "
            . " 'Please reconfirm your action!'),"
            . "(291, "
            . " 'alert.confirm.activate.machine', "
            . " 'Are you sure you want to activate the machine?'),"
            . "(292, "
            . " 'alert.confirm.deactivate.machine', "
            . " 'Are you sure you want to deactivate the machine?'),"
            . "(293, "
            . " 'alert.confirm.button.deactivate', "
            . " 'Deactivate'),"
            . "(294, "
            . " 'alert.confirm.button.activate', "
            . " 'Activate'),"
            . "(294, "
            . " 'alert.confirm.button.remove', "
            . " 'Remove'),"
            . "(295, "
            . " 'alert.confirm.remove.machine', "
            . " 'Are you sure you want to remove the machine?'),"
            . "(296, "
            . " 'alert.confirm.button.cancel', "
            . " 'Cancel'),"
            . "(297, "
            . " 'alert.notification.no.posted.machines', "
            . " 'You don’t have a posted machines'),"







            // Machine Edit
            . "(298, "
            . " 'edit.machine.title', "
            . " 'Update Machine'),"
            . "(299, "
            . " 'edit.machine.breadcrumbs.machine', "
            . " 'Machines'),"
            . "(300, "
            . " 'edit.machine.form.title', "
            . " 'Update machine'),"
            . "(301, "
            . " 'edit.machine.form.machine.name', "
            . " 'Machine name'),"
            . "(302, "
            . " 'edit.machine.form.condition', "
            . " 'Condition'),"
            . "(303, "
            . " 'edit.machine.form.type', "
            . " 'Type'),"
            . "(304, "
            . " 'edit.machine.form.description', "
            . " 'Description'),"
            . "(305, "
            . " 'edit.machine.form.upload.image', "
            . " 'Upload image'),"
            . "(306, "
            . " 'edit.machine.form.click.to.add.new.image', "
            . " '<b class=\"font-28\">Click </b> to add new image!'),"
            . "(307, "
            . " 'edit.machine.form.image.uploaded', "
            . " 'Image succesful uploaded!'),"
            . "(308, "
            . " 'edit.machine.form.more.details', "
            . " '<b>Click</b> to add more details! (optional)'),"
            . "(309, "
            . " 'edit.machine.form.currency', "
            . " 'Currency'),"
            . "(310, "
            . " 'edit.machine.form.url', "
            . " 'Website'),"
            . "(311, "
            . " 'edit.machine.form.price', "
            . " 'Price'),"
            . "(312, "
            . " 'edit.machine.form.button.post.machine', "
            . " 'Update'),"
            . "(313, "
            . " 'edit.machine.condition.new', "
            . " 'New'),"
            . "(314, "
            . " 'edit.machine.condition.used', "
            . " 'Used');");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE cms_ru;");
    }
}
