<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersLocationsTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE users_locations ("
                       . " id INT(15) NOT NULL AUTO_INCREMENT, "
                       . " user_id INT, "
                       . " location_id INT(4), "
                       . " region_id INT(3), "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE users_locations;");
    }
}
