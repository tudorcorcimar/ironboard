<?php

use Phinx\Migration\AbstractMigration;

class PopulateUsersPackagesTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("INSERT INTO users_packages "
                       . " (id, name) "
                       . " VALUES "
                       . " (1, 'non-member'), "
                       . " (2, 'member') ;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
