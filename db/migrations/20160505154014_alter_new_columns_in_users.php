<?php

use Phinx\Migration\AbstractMigration;

class AlterNewColumnsInUsers extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("ALTER TABLE users "
                       . " ADD ("
                       . " address TEXT, "
                       . " website VARCHAR(255), "
                       . " public_email VARCHAR(255) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
