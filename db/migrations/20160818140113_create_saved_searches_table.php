<?php

use Phinx\Migration\AbstractMigration;

class CreateSavedSearchesTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE saved_search ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " name VARCHAR(255), "
                       . " json_filters text, "
                       . " created_at DATE, "
                       . " updated_at DATE, "
                       . " PRIMARY KEY (id) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE saved_search;");
    }
}
