<?php

use Phinx\Migration\AbstractMigration;

class CreateUserPakagesTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE users_packages ("
                       . " id INT(2), "
                       . " name VARCHAR(50) );");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE users_packages;");
    }
}
