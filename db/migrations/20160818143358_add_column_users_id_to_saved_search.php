<?php

use Phinx\Migration\AbstractMigration;

class AddColumnUsersIdToSavedSearch extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        
        $this->execute("ALTER TABLE saved_search "
            . " ADD users_id INT(11) ;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
