<?php

use Phinx\Migration\AbstractMigration;

class UpdateAdminPassword extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("UPDATE admin SET password = 'freshdewonfreewind' WHERE permissions = 'admin';");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}