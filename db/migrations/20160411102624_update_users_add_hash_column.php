<?php

use Phinx\Migration\AbstractMigration;

class UpdateUsersAddHashColumn extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("ALTER TABLE users "
                       . " ADD hash VARCHAR(155);");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
