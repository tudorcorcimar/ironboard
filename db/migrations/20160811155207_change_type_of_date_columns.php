<?php

use Phinx\Migration\AbstractMigration;

class ChangeTypeOfDateColumns extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("ALTER TABLE machine MODIFY created_at DATETIME;");
        $this->execute("ALTER TABLE machine MODIFY updated_at DATETIME;");
        
        $this->execute("ALTER TABLE users MODIFY created_at DATETIME;");
        $this->execute("ALTER TABLE users MODIFY updated_at DATETIME;");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }
}
