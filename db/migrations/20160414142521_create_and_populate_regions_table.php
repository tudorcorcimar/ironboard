<?php

use Phinx\Migration\AbstractMigration;

class CreateAndPopulateRegionsTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE regions ("
                       . " id INT(4) NOT NULL, "
                       . " name VARCHAR(255) NOT NULL);");
        
        $this->execute("
            INSERT INTO `regions` (`id`,`name`) VALUES (1,'Africa');
            INSERT INTO `regions` (`id`,`name`) VALUES (2,'Latin America and the Caribbean');
            INSERT INTO `regions` (`id`,`name`) VALUES (3,'Asia');
            INSERT INTO `regions` (`id`,`name`) VALUES (4,'EU 28');
            INSERT INTO `regions` (`id`,`name`) VALUES (5,'Oceania');
            INSERT INTO `regions` (`id`,`name`) VALUES (6,'Europe Non EU 28');
            INSERT INTO `regions` (`id`,`name`) VALUES (7,'Northern America');
        ");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE regions;");
    }
}
